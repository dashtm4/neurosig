<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/privacy', function () {
    return view('privacy');
});

// Auth::routes();

Route::auth();

Route::get('/welcome', 'HomeController@index');

Route::get('/home', 'HomeController@home');

Route::get('tos', function () {
    return view('tos');
});

Route::resource('support', 'SupportController');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('uploads', 'UploadsController');
});

Route::get(
    'downloads/download/{id}',
    [
        'as' => 'downloads.download',
        'uses' => 'DownloadsController@download',
    ]
)->middleware('restricted');

Route::group(['middleware' => ['restricted']], function () {
    Route::resource('downloads', 'DownloadsController');
});

// Route::group(['middleware' => ['trainer']], function () {
//     Route::resource('trainers', 'TrainersController');
// });

//Route::resource('contact', 'ContactController');

Route::get(
    'contact',
    [
    'as' => 'contact.create',
    'uses' => 'ContactController@create',
    ]
);

Route::post(
    'contact',
    [
    'as' => 'contact.store',
    'uses' => 'ContactController@store',
    ]
);

Route::resource('excel', 'ExcelController');

Route::resource('files', 'FilesController');

Route::resource('projectfiles', 'ProjectFilesController');

Route::group(
    [
        'namespace' => 'Admin',
    ],
    function () {
        Route::put(
            'users/{id}/privacy',
            [
            'as' => 'users.privacy',
            'uses' => 'UsersController@updatePrivacy',
            ]
        );
    }
);

Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'adminexpertsystem',
    ],
    function () {
        Route::resource('snippets', 'SnippetsController');
    }
);

Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'admin',
    ],
    function () {
        Route::resource('privacies', 'PrivaciesController');

        Route::resource('users', 'UsersController');

        Route::resource('clients', 'ClientsController');

        Route::resource('files', 'FilesController');

        Route::resource('projects', 'ProjectsController');

        Route::resource('sites', 'SitesController');

        Route::get('token-settings', 'TokenSettingsController@index')->name('token-settings');
        Route::get('closed-projects', 'ProjectsController@closed')->name('closed-projects');
        Route::get('closed-sites', 'SitesController@closed')->name('closed-sites');

        Route::delete(
            'files/destroy-for-project/{projectId}',
            [
            'as' => 'files.destroy-for-project',
            'uses' => 'FilesController@destroyForProject',
            ]
        );

        Route::put(
            'files/restore-for-project/{projectId}',
            [
            'as' => 'files.restore-for-project',
            'uses' => 'FilesController@restoreForProject',
            ]
        );

        Route::put(
            'projects/restore/{projectId}',
            [
              'as' => 'projects.restore',
              'uses' => 'ProjectsController@restore',
            ]
        );
    }
);

Route::get('uploader', 'UploadsController@uploader')->name('uploader');
Route::post('uploader-store', 'UploadsController@uploaderStore')->name('uploader-store');

Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'adminexpertsystem',
    ],
    function () {
        Route::post(
            'files/{file}/reviews/parent',
            [
            'as' => 'files.reviews.parent',
            'uses' => 'ReviewsController@parent',
            ]
        );

        Route::resource('files.reviews', 'ReviewsController');
    }
);

Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'adminexpertsystem',
    ],
    function () {
        Route::get(
            'projects/{id}/getsites',
            [
            'as' => 'users.getsites',
            'uses' => 'ProjectsController@getsites',
            ]
        );
    }
);

Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'adminexpertsystem',
    ],
    function () {
        Route::post(
            'files/{file}/reviews/namefields',
            [
            'as' => 'files.reviews.namefields',
            'uses' => 'ReviewsController@namefields',
            ]
        );

        Route::resource('files.reviews', 'ReviewsController');
    }
);

Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'adminexpertsystem',
    ],
    function () {
        Route::resource('files.reviews', 'ReviewsController');
    }
);
