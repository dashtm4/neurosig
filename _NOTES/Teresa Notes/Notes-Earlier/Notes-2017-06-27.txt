July 6, 2017 Notes

##Reference

    login into Cloud 9 Workspace:
        https://ide.c9.io/tmlight/file-upload-app
    
    phpMyAdmin:
        https://file-upload-app-tmlight.c9users.io/phpmyadmin/
        
    preview:
        https://file-upload-app-tmlight.c9users.io/

## Notes from Previous Sessions
    Reading:
        See https://laravel.com/docs/5.4/eloquent-relationships#many-to-many for more information about assigning sites and users to the site_user table.

    Application Design Related:
        If we know a non-admin user can only be assigned to one and only one site, then the users table should have a site_id field. 
        Then, after creating a new site, the admin can create a new user, and assign that user to the desired site.
        The caveat here is the user can only belong to one site.  -- We considered this but decided against it.

    Actions:
        In order to restrict the http://file-upload-app-tmlight.c9users.io/admin/sites/1/edit screen to only include users belonging to the project associated
        with the site, you'll want to 
                add a project_id field to users. 
            Then 
                add a belongsTo() relationship in the User model
            and
                add a hasMany relationship in the Project model.
                
        Then in the sites#edit view, 
        
            modify the multi-select field selection logic to restrict results only to the users who belong to the site's project.

        http://www.easylaravelbook.com/blog/2015/08/25/populating-a-laravel-form-multiple-select-box-with-hasmany-relation-values/

        You can delete the SitesUsersController.php file because site/user assignments will be managed in sites#edit.

    File Upload Feature Design:

        For the file upload feature, I would just make a file upload form available to users who log in. When they upload file, if they are
        only assigned to one site, then they just upload the file and they're done. If they belong to multiple sites, include a select field in
        which they first choose the site.

        file extension checks can be handled in the form request using the file type validator.

## Future

    Contact Support Form Strategy and Reading:
        https://laravel.com/docs/5.1/mail
        
        
NOTES

Documentation:
File uploads are stored in storage/app/storage/files

Contact Form (Future):
Use SparkPost or Mailgun for mail delivery.  User SparkPost, create the form and submit and create mail object new to 5.4 and takes teh form contents and injects into the email and using whatever 
delivery method.  Probaly use Mailgun becuase one less account,  it is still the lsame code.  Just use mailgun api.  The code is going to look the exactly.  Would look in the Laravel docs. 

Thursday (7/6) - next meeting at 1pm Pacific

Teresa - Next Steps
    Fix the seeder - site 1 user id should be 9
    
    Update the seeder to have user with more than 1 site
    
    Create some files to upload.
    
    Edit the Uploads Controller -- and then we are done.  
    Want to populate the data base next
    Grab the file name, save that along with the user id in
    
    For Files index, grab all the stuff out of the file table and order by name.  

 
##Today's Notes
