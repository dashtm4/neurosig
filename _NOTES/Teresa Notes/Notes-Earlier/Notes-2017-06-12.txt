June 12, 2017 Notes

##Reference

    login into Cloud 9 Workspace:
        https://ide.c9.io/tmlight/file-upload-app
    
    phpMyAdmin:
        https://file-upload-app-tmlight.c9users.io/phpmyadmin/
        
    preview:
        https://file-upload-app-tmlight.c9users.io/
    
##Teresa Update    
    

Review file upload/download app with focus on the site, user and file upload.  The project and client may be added in later.

I created a "cheat sheet" of the file names.  

I created Migrations, Seeders, Models, Controllers, some Routes and Views.  Should check the SitesUsersController and the routes. 

The SitesUsersController has nothing in it.  And the SitesUsersSeeder gives the error below so I manually put one record in it.
  SQLSTATE[42S02]: Base table or view not found: 1146 Table 'c9.site_users' doesn't exist
  
##Teresa Update 

## NOTES

* Delete password form field from /users/edit - DONE




See https://laravel.com/docs/5.4/eloquent-relationships#many-to-many for more information about assigning sites and users to the site_user table.


If we know a non-admin user can only be assigned to one and only one site, then the users table should have a site_id field. Then, after creating a new site, 
the admin can create a new user, and assign that user to the desired site. The caveat here is the user can only belong to one site.

In order to restrict the http://file-upload-app-tmlight.c9users.io/admin/sites/1/edit screen to only include users belonging to the project associated
with the site, you'll want to add a project_id field to users. Then add a belongsTo() relationship in the User model, and a hasMany relationship in the 
Project model. Then in the sites#edit view, modify the multi-select field selection logic to restrict results only to the users who belong to the
site's project.

http://www.easylaravelbook.com/blog/2015/08/25/populating-a-laravel-form-multiple-select-box-with-hasmany-relation-values/

You can delete the SitesUsersController.php file because site/user assignments will be managed in sites#edit.



For the file upload feature, I would just make a file upload form available to users who log in. When they upload file, if they are
only assigned to one site, then they just upload the file and they're done. If they belong to multiple sites, include a select field in
which they first choose the site.

file extension checks can be handled in the form request using the file type validator.


