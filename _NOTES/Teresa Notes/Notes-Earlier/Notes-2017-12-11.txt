If a user who had previously uploaded files is deleted, the deletion process
will need to include a step in which the admin is prompted to reassign the 
user's uploaded files to another user associated with the site.

