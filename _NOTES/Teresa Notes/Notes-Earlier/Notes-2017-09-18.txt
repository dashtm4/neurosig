September 18, 2017 Notes

\App\Site::->select(\DB::raw('concat(name, " "  , id'))->pluck('name', 'id');


\App\Site::select('id',DB::raw('CONCAT("(ID: ", identifier, ") ", name) AS name'))->pluck('name', 'id');




https://stackoverflow.com/questions/22008487/how-to-concatenate-columns-with-laravel-4-eloquent



function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');   

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}


round(pow(1024, log($file->size, 1024) - floor(log($file->size, 1024))), 2) .'M';


## Validate by File Types

'file' => 'mimetypes:application/zip,
application/x-rar-compressed,
application/x-brainvision-data,
text/x-brainvision-marker,
text/x-brainvision-header,
text/plain,application/pdf'



zip - application/zip
rar - application/x-rar-compressed
eeg - application/x-brainvision-data
vmrk - text/x-brainvision-marker
vhdr - text/x-brainvision-header
txt - text/plain
pdf - application/pdf