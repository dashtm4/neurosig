February 12, 2018

Discussion List:

    Project User Type - to view files.  Is it time to create User Type and User Access Tables?.  We were going to write middleware for this.
    
    NOTES: Provided there is no foreseeable need to add further user types, creating an is_project field in the users
    table is recommended. Alternatively, refactoring the system to use for instance Laravel's authorization solution
    is going to be time consuming.
    
    Deployment Automation Update
    
    NOTES: Moving to next week.
    
    Parsing File Name - Update Project and File Tables Now for Future Upgrade
        Separate Fields for Visit Number and Subject Number.
        Based on project setup. Update Project and Files Table Migrations.  
        Triggered when a review is done.
        
        Example file name: PERX-OTHER_99999_11Jan2018_PJ_6981CL0004_10001_B-1.zip
        https://app.smartsheet.com/b/home?lx=xuICKTvZeFoeC1UxTIA1FA
        
    Pagination Removal
    
To Do List:

    Grouping Files
        Solve issue where there are multiple uploads associated with one EEG

    Save Review Attachment in S3 with Ability to View Within the App
    
    Email Update - send/receive reviews to data@neurosig.com
    

            
Future:    
    
    Synchronizing downloads: 
    
        Need more research
    

            # Synchronizing newly uploaded files to a third-party drive
            
            1. Create a new endpoint such as /uploads/sync. This controller action will contain the logic responsible for synchronizing
               S3 files with Google Drive.
            2. Create a job which will look at S3 and determine whether any files have been uploaded in the past 60 minutes.
            3. Loop over identified files and upload them to a third-party service such as Google Drive.
            4. Integrate this job with the endpoint
            5. Run the endpoint manually to confirm working
            6. Configure CRON to run at the top of every hour, using WGET to execute that endpoint (and therefore, the underlying job).
            
            WGET http://neurosig.com/uploads/sync
            
            
                public function sync()
                {
                    dump("SYNCHRONIZING FILES");
                }
                
                Route::get('/uploads/sync', 'UploadsController@sync');
            
            Alternatively, consider looking for a Windows program that runs in the background and automatically synchronizes S3 files with a local directory.
            Need to make sure this runs as a Windows Service so automatically starts anytime the computer is rebooted.
            
            When this is resolved we need to modify the Review update logic to automatically set the "downloaded on" field to NOW() when a review is submitted if the file had not been downloaded prior to that.
            
NOTES

{{--            @if($users instanceof \Illuminate\Pagination\LengthAwarePaginator ) --}}
{{--            <?php echo $users->render(); ?> --}}
{{--            @endif --}}

            <div class="form-group hidden-print">
        {!! Form::label('paginate','Paginate?') !!}
        
        {!! Form::select('paginate',
            [
                'yes' => 'Yes',
                'no'   => 'No'
            ],
            'yes',
            [
                'id'    => 'paginate',
                'class' =>'form-control',
                'placeholder' => 'Paginate?'
            ]
            
            )!!}
    </div>
    
            //if ($request->has('paginate') and $request->get('paginate') == 'no') {  
            $users = $query->get();
        //} else {
        //    $users = $query->paginate(15);
        //}