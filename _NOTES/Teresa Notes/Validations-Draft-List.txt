
Validations List:
    Validate that a duplicate site cannot be entered
    
    Validate - Limit File Types
        # New ones - EDF, .NS2 
        
        The NS2 file extension is related to Lotus Notes and used for older format of databases used in this program. It might still be possible to open these files in latest versions of Lotus Notes.
        MIME: application/vnd.lotus-notes
        and
            application/x-edf
            
        ##
        
            'file' => 'mimetypes:application/zip,
            application/x-rar-compressed,
            application/x-brainvision-data,
            text/x-brainvision-marker,
            text/x-brainvision-header,
            text/plain,application/pdf'
        
            
            
            zip - application/zip
            rar - application/x-rar-compressed
            eeg - application/x-brainvision-data
            vmrk - text/x-brainvision-marker
            vhdr - text/x-brainvision-header
            txt - text/plain
            pdf - application/pdf
