
<fieldset>
    
    <!--Quality Rating Section     -->
    <div class="form-group row">
        <legend class="col-form-label col-sm-2 pt-0">Quality Rating</legend>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-2">
                    {{ Form::radio('rating', '0', ['class'=>'form-check-input', 'id'=>'zero-check']) }}
                    <label class="form-check-label" for="zero-check">
                      Not Rated
                    </label>
                </div>
                <div class="col-sm-2">
                    {{ Form::radio('rating', '1', ['class'=>'form-check-input', 'id'=>'one-check']) }}
                    <label class="form-check-label" for="one-check">
                      Not Usable
                    </label>
                </div>
                <div class="form-check col-md-2">
                    {{ Form::radio('rating', '2', ['class'=>'form-check-input', 'id'=>'two-check']) }}
                    <label class="form-check-label" for="two-check">
                      Some Usable
                    </label>
                </div>
                <div class="form-check col-sm-2">
                    {{ Form::radio('rating', '3', ['class'=>'form-check-input', 'id'=>'three-check']) }}
                    <label class="form-check-label" for="three-check">
                      All With Tips
                    </label>
                </div>
                
                <div class="form-check col-sm-2">
                    {{ Form::radio('rating', '4', ['class'=>'form-radio-input', 'id'=>'four-check']) }}
                    <label class="form-radio-label" for="four-check">
                      All Good
                    </label>
                </div>
            </div>
        </div>
        
    </div>
    
    <!--Reviewer Feedback Section  with Snippets   -->
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {!! Form::label('reviewer_notes','Reviewer Feedback') !!}
                {!! Form::textarea('reviewer_notes', null, ['id' => 'snippitField', 'class' => 'form-control', 'rows' => 8, 'cols' => 50]) !!}
            </div>
        </div>
        <div class="col-sm-6">
            <legend class="col-form-label col-sm-2 pt-0">Snippits:</legend>
            <div class="container">
                
                @foreach ($snippets as $snippet)
                    <span class="badge badge-pill badge-primary" data-text="{{$snippet['snippet']}}" onClick="document.getElementById('snippitField').value+=this.getAttribute('data-text')+'\n';">{{$snippet['name']}}</span>
                @endforeach
               
            </div>
        </div>
    </div>
    
    <!-- Quality Type Section   -->   
    <div class="form-group row">
        <legend class="col-form-label col-sm-2 pt-0">Type of Quality Issue</legend>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-2">
                    {{ Form::radio('qualityCategory', 'None', true, ['class'=>'form-check-input', 'id'=>'good-check', 'checked'=>'checked']) }}
                    <label class="form-check-label" for="good-check">
                      Good
                    </label>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-2">
                    {{ Form::radio('qualityCategory', 'Electrical Noise', false, ['class'=>'form-check-input', 'id'=>'electrical-check']) }}
                    <label class="form-check-label" for="electrical-check">
                      Electrical Noise
                    </label>
                </div>
                <div class="col-sm-2">
                    {{ Form::radio('qualityCategory', 'Eyes', false, ['class'=>'form-check-input', 'id'=>'eyes-check']) }}
                    <label class="form-check-label" for="eyes-check">
                      Eyes
                    </label>
                </div>
                <div class="col-md-2">
                    {{ Form::radio('qualityCategory', 'Gel Bridging', false, ['class'=>'form-check-input', 'id'=>'bridging-check']) }}
                    <label class="form-check-label" for="bridging-check">
                      Gel Bridging
                    </label>
                </div>
                <div class="col-sm-2">
                    {{ Form::radio('qualityCategory', 'Bad Electrods', false, ['class'=>'form-check-input', 'id'=>'electrods-check']) }}
                    <label class="form-check-label" for="electrods-check">
                      Bad Electrods
                    </label>
                </div>
                
                <div class="col-sm-2">
                    {{ Form::radio('qualityCategory', 'Other Artifacts', false, ['class'=>'form-check-input', 'id'=>'artifacts-check']) }}
                    <label class="form-check-label" for="artifacts-check">
                      Other Artifacts
                    </label>
                </div>
                
                
            </div class="row">
            <div class="row clearfix">
                <div class="col-sm-2">
                    {{ Form::radio('qualityCategory', 'Response Button', false, ['class'=>'form-check-input', 'id'=>'response-check']) }}
                    <label class="form-check-label" for="response-check">
                      Response Button
                    </label>
                </div>
                <div class="col-sm-2">
                    {{ Form::radio('qualityCategory', 'Missing Session(s)', false, ['class'=>'form-check-input', 'id'=>'missing-check']) }}
                    <label class="form-check-label" for="missing-check">
                      Missing Session(s)
                    </label>
                </div>
                <div class="col-sm-2">
                    {{ Form::radio('qualityCategory', 'Other', false, ['class'=>'form-check-input', 'id'=>'other-check']) }}
                    <label class="form-check-label" for="other-check">
                      Other
                    </label>
                </div>
                 
                
            </div>
            
        </div>
    </div>
<br>    
    
    <!-- File Upload Section   --> 
    
    <div class="form-group">
        {!! Form::label('Upload file to include with email') !!}
        {!! Form::file('email_attachment', null) !!}
    </div>
<br>

    <!-- Status Section --> 
    
    <div class="form-group">       
        {!! Form::label('status','Follow Up')!!}
        {{ Form::checkbox('status','Follow Up', null) }}
    </div>

    <!-- Comment Section   -->     
    <div class="form-group">
        {!! Form::label('comment','Internal Comment') !!}
        {!! Form::text('comment', null, ['class' => 'form-control', 'rows' => 1, 'cols' => 50]) !!}
    </div>

    <div class="form-group">

        {!!Form::label('is_draft','Save as draft?')!!}

        {!! Form::select('is_draft',
            [
                'no' => 'No (E-mails will be sent if selected below)',
                'yes'   => 'Yes (E-mails will not be sent)'
            ],
            NULL, 
            [
                'id'    => 'is_draft',
                'class' =>'form-control'
            ]
            
            )!!}

    </div>
<br>
    <div class="form-group">       
        {!! Form::label('send_email','Send Email')!!}
        <input type="checkbox" name="send_email" id="send_email" value="1" checked="checked"/>
        <p>
            By checking this checkbox, you will send an email containing the rating and associated notes
            to {{ $file->user->email }}.
        </p>
    </div>

    <div class="form-group">

        {!!Form::label('send_email_all_users','Send Email to all site members')!!}
        <input type="checkbox" name="send_email_all_users" id="send_email_all_users" value="1", checked="checked" />
        <p>
            By checking this checkbox, you will send an email containing the rating and associated notes
            to all members associated with this file's site.
        </p>
    </div>
</fieldset>

