<!--Project Partial-->

<fieldset>
    
    <div class="form-group">
        {!!Form::label('identifier','Project Identifier')!!}
        {!!Form::text('identifier',null, ['id' =>'identifIer','class' =>'form-control','placeholder' =>'Enter Id']) !!}
    </div>
    
     <div class="form-group">
        {!!Form::label('client_id','Client Id')!!}
        {!! Form::select('client_id', App\Client::orderBy('identifier', 'asc')->pluck('identifier', 'id'),null, 
        ['placeholder' => 'Select Client', 'class' => 'form-control'])!!}
    </div>
    
    <div class="form-group">
        {!! Form::label('clientName','Client Name')!!}
        {!! Form::text('clientName', null, ['id' =>'clientName','class' =>'form-control','placeholder' =>'Client Name']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('clientType','Client Type') !!}
        
        {!! Form::select('clientType',
            [
                'New'      => 'New',
                'Repeat'    => 'Repeat',
                'Other'     => 'Other'
            ],
            isset($project) ? $project->clientType : null, 
            [
                'id' =>'status',
                'class' =>'form-control',
                'placeholder' =>'Select Client Type'
            ]
            ) !!}
    </div>
    
    <div class="form-group">
        {!!Form::label('name','Project Name')!!}
        {!!Form::text('name', null, ['id' =>'name','class' =>'form-control','placeholder' => 'Enter Name']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('category','Project Category') !!}
        
        {!! Form::select('category',
            [
                'Client'   => 'Client',
                'Internal' => 'Internal',
                'Other'    => 'Other'
            ],
            isset($project) ? $project->category : null, 
            [
                'id'    => 'category',
                'class' =>'form-control',
                'placeholder' => 'Select Project Category'
            ]
            
            )!!}
    </div>
    
     <div class="form-group">
        {!! Form::label('visitsPerSubject','Vists Per Subject')!!}
        {!! Form::number('visitsPerSubject', null, ['id' =>'visitsPerSubject','class' =>'form-control','placeholder' =>'Enter Number of Visits for each Subject']) !!}
    </div>
    
        @include('admin.projects._positionsInFilename_form')
        
        @include('admin.projects._keyDates_form')
    
     <div class="form-group">
        {!! Form::label('retentionNotes','Retention Notes')!!}
        {!! Form::text('notes', null, ['id' =>'nretentionNotes','class' =>'form-control','placeholder' =>'Enter Rentention Notes']) !!}
    </div>   

    <!--Repository Paths and Descriptions-->
    <div class="form-group">
        {!! Form::label('repoPath1','Repository Path 1')!!}
        {!! Form::text('repoPath1', null, ['id' =>'repoPath1','class' =>'form-control','placeholder' =>'Repository Path 1']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('repoPath1Description','Repository Path 1 Description')!!}
        {!! Form::text('repoPath1Description', null, ['id' =>'repoPath1Description','class' =>'form-control','placeholder' =>'Repository Path 1 Description']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('repoPath2','Repository Path 2')!!}
        {!! Form::text('repoPath2', null, ['id' =>'repoPath2','class' =>'form-control','placeholder' =>'Repository Path 2']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('repoPath2Description','Repository Path 2 Description')!!}
        {!! Form::text('repoPath2Description', null, ['id' =>'repoPath2Description','class' =>'form-control','placeholder' =>'Repository Path 2 Description']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('repoPath3','Repository Path 3')!!}
        {!! Form::text('repoPath3', null, ['id' =>'repoPath3','class' =>'form-control','placeholder' =>'Repository Path 3']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('repoPath3Description','Repository Path 3 Description')!!}
        {!! Form::text('repoPath3Description', null, ['id' =>'repoPath3Description','class' =>'form-control','placeholder' =>'Repository Path 3 Description']) !!}
    </div>
    
     <div class="form-group">
        {!! Form::label('status','Project Status') !!}
        
        {!! Form::select('status',
            [
                'Open'      => 'Open',
                'Closed'    => 'Closed',
                'Other'     => 'Other'
            ],
            isset($project) ? $project->status : null, 
            [
                'id' =>'status',
                'class' =>'form-control',
                'placeholder' =>'Select Project Status'
            ]
            ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('notes','Project Notes')!!}
        {!! Form::text('notes', null, ['id' =>'notes','class' =>'form-control','placeholder' =>'Enter Notes']) !!}
    </div>   

</fieldset>
<br clear="all" />