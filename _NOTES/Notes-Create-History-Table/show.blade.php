<!--Projects Show-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Project {{ $project->projectId }}</h1>

    </div>
</div>

<div class="container clearfix mb-4">

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Description</th>
                <th>Value</th>
            </tr>
        </thead>
    
        <tbody>
            
            <tr>
                <td>Project Identifier</td>
                <td>{{ $project->identifier }}</td>
            </tr>
            
            <tr>
                <td>Project Name</td>
                <td>{{ $project->name }}</td>
            </tr>
            
            <tr>
                <td>Category</td>
                <td>{{ $project->category }}</td>
            </tr>
            
            <tr>
                <td>Status</td>
                <td>{{$project->status}}</td>
            </tr>
            
            <tr>
                <td>Notes</td>
                <td>{{$project->notes }}</td>
            </tr>
            
    
            <tr>
                <td>Client Name</td>
                <td>{{ $project->clientName }}</td>
            </tr>
            
            <tr>
                <td>Client Type</td>
                <td>
                    @if (! is_null($project->clientType))
                        {{ $project->clientType }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr> 
            
            <tr>
                <td>Client Notes</td>
                <td>{{ $project->clientNotes}}</td>
            </tr>
            
    
            <tr>
                <td>File Name Position - Visit</td> 
                <td>{{ $project->fileNamePosition_VisitID }}</td> 
            </tr> 
            
            <tr>
                <td>File Name Position - Subject</td> 
                <td>{{ $project->fileNamePosition_SubjectID }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Date</td> 
                <td>{{ $project->fileNamePosition_Date }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Tester</td> 
                <td>{{ $project->fileNamePosition_TesterID }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Project</td> 
                <td>{{ $project->fileNamePosition_ProjectID }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Site</td> 
                <td>{{ $project->fileNamePosition_SiteID }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Station</td> 
                <td>{{ $project->fileNamePosition_StationID }}</td> 
            </tr>
    
             <tr>
                <td>Retention End Date</td>
                <td>
                    @if (! is_null($project->retentionEndDate))
                        {{ date("m/d/y", strtotime($project->retentionEndDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
    
            <tr>
                <td>Retention Notes</td>
                <td>{{$project->retentionNotes }}</td>
            </tr>
            
            <tr>
                <td>Repository Path 1</td>
                <td>{{$project->repoPath1 }}</td>
            </tr>
            
            <tr>
                <td>Repository Path 1 Description</td>
                <td>{{$project->repoPath1Description }}</td>
            </tr>
            
            <tr>
                <td>Repository Path 2</td>
                <td>{{$project->repoPath2 }}</td>
            </tr>
            
            <tr>
                <td>Repository Path 2 Description</td>
                <td>{{$project->repoPath2Description }}</td>
            </tr>
            
            <tr>
                <td>Repository Path 3</td>
                <td>{{$project->repoPath3 }}</td>
            </tr>
            
            <tr>
                <td>Repository Path 3 Description</td>
                <td>{{$project->repoPath3Description }}</td>
            </tr>
            
            <tr>
                <td>Qualification Date</td>
                <td>
                    @if (! is_null($project->qualificationDate))
                        {{ date("m/d/y", strtotime($project->qualificationDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>Contract Date</td>
                <td>
                    @if (! is_null($project->contractDate))
                        {{ date("m/d/y", strtotime($project->contractDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>Project Start Date</td>
                <td>
                    @if (! is_null($project->projectStartDate))
                        {{ date("m/d/y", strtotime($project->projectStartDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>Training Start Date</td>
                <td>
                    @if (! is_null($project->trainingStartDate))
                        {{ date("m/d/y", strtotime($project->trainingStartDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>First Subject Date</td>
                <td>
                    @if (! is_null($project->firstSubjectDate))
                        {{ date("m/d/y", strtotime($project->firstSubjectDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>Last Subject Date</td>
                <td>
                    @if (! is_null($project->lastSubjectDate))
                        {{ date("m/d/y", strtotime($project->lastSubjectDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>Last Data Release Date</td>
                <td>
                    @if (! is_null($project->lastReleaseDate))
                        {{ date("m/d/y", strtotime($project->lastReleaseDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
             <tr>
                <td>Visits Per Subject</td> 
                <td>{{ $project->visitsPerSubject }}</td> 
             </tr>
                   
            <tr>
                <td>Client ID</td>
                <td>{{$project->client_id }}</td>
            </tr>
            
            <tr>
                <td> Created On</td>
                <td>{{ date("m/d/y", strtotime($project->created_at)) }}</td>
            </tr>
            
            <tr>
                <td>Updated On</td>
                <td>
                    @if (! is_null($project->updated_at))
                        {{ date("m/d/y", strtotime($project->updated_at)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>Deleted On</td>
                <td>
                    @if (! is_null($project->deleted_at))
                        {{ date("m/d/y", strtotime($project->deleted_at)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <div class="btn-group float-right">
        {!! link_to_route('projects.index', "Projects Home", null, ['class' => 'btn btn-default'] ) !!}
    
        {!! link_to_route('projects.edit', "Edit Project", [$project->id], ['class' => 'btn btn-primary']) !!}
        
        @if($project->trashed())
        
        <button type='button' data-toggle="modal" data-target="#restoreConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Restore Project</button>
        
        @else
        
        <button type='button' data-toggle="modal" data-target="#deleteConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete Project</button>
        
        @endif
    
        @if($project->files()->count() > 0)
        <button type='button' data-toggle="modal" data-target="#deleteFilesConfirm" class='btn btn-warn'><i class='glyphicon glyphicon-trash'></i> Delete Project Files</button>
        @endif
        
        @if($project->trashedFiles()->count() > 0)
         <button type='button' data-toggle="modal" data-target="#restoreFilesConfirm" class='btn btn-warn'><i class='glyphicon glyphicon-trash'></i> Restore Project Files</button>
        @endif
    </div>
    
</div>
    



@include('admin.projects._restore_confirm')

@include('admin.projects._restore_files_confirm')

@include('admin.projects._delete_confirm')

@include('admin.projects._delete_files_confirm')
    
@endsection