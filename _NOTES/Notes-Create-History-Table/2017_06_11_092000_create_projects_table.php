<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
  
            //Project Information          
            $table->string('identifier', 50);
            $table->string('name', 150);
            $table->string('category', 50)->nullable();
            $table->string('status', 50)->nullable();
            $table->text('notes')->nullable();

            //Client Information
            $table->string('clientName', 150)->nullable()->default(null);
            $table->string('clientType', 150)->nullable()->default(null);
            $table->text('clientNotes')->nullable();
            
            //File Name Postions
            $table->integer('fileNamePosition_VisitID')->unsigned()->nullable()->default(0);
            $table->integer('fileNamePosition_SubjectID')->unsigned()->nullable()->default(1);
            $table->integer('fileNamePosition_Date')->unsigned()->nullable()->default(2);
            $table->integer('fileNamePosition_TesterID')->unsigned()->nullable()->default(3);
            $table->integer('fileNamePosition_ProjectID')->unsigned()->nullable()->default(4);
            $table->integer('fileNamePosition_SiteID')->unsigned()->nullable()->default(5);
            $table->integer('fileNamePosition_StationID')->unsigned()->nullable()->default(6);

            //Repository and Rentention Information
            $table->datetime('retentionEndDate')->nullable()->default(null); 
            $table->text('retentionNotes')->nullable();
            $table->string('repoPath1', 191)->nullable()->default(null);
            $table->text('repoPath1Description')->nullable();
            $table->string('repoPath2', 191)->nullable()->default(null);
            $table->text('repoPath2Description')->nullable();
            $table->string('repoPath3', 191)->nullable()->default(null);
            $table->text('repoPath3Description')->nullable();
            
            //Cycle Time Historical Information
            $table->datetime('qualificationDate')->nullable()->default(null);
            $table->datetime('contractDate')->nullable()->default(null);  
            $table->datetime('projectStartDate')->nullable()->default(null);
            $table->datetime('trainingStartDate')->nullable()->default(null);
            $table->datetime('firstSubjectDate')->nullable()->default(null);
            $table->datetime('lastSubjectDate')->nullable()->default(null);
            $table->datetime('releaseDate')->nullable()->default(null);

             //To Calculate Where You Are in a Study
            $table->integer('visitsPerSubject')->unsigned()->nullable();
          
            //Relationships
            $table->integer('client_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
