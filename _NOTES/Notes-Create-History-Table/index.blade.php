<!--Projects Index -->

@extends ('layouts.app')

@section ('content')

    <div class="page-header">
        <div class = "container clearfix">
            
            <h1>Projects</h1>
            <div class="btn-group float-right">
    
            <a href="{{ route('closed-projects') }}" class="btn btn-danger hidden-print">Closed Projects</a>
            <a href="{{ route('projects.create') }}" class="btn btn-primary hidden-print">New Project</a>
            </div>
        </div>
        
    </div>
    
    @if ($projects->count() > 0)
    
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                    <th>Key</th>
                    <th>Identifier</th>
                    <th>Project Name</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Client Name</th>
                    <th>Client Type</th>
                    <th>Notes</th>
                    <th>FNP-Visit</th>
                    <th>FNP-Subject</th>
                    <th>FNP-Date</th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($projects as $project)
               
                <tr style="list-style: none;">
                    
                                    
                    <td>
                        {{ $project->id}}
                    </td>
                    
                    <td>
                        {!! link_to_route('projects.edit', $project->identifier, [ $project->id ]) !!}
                    </td>
                    
                    <td>
                        {{ $project->name}}
                    </td> 

                    <td>
                        {{ $project->category}}
                    </td>
        
                    <td>
                        {{ $project->status}}
                    </td>
                    
                    <td>
                        {{ $project->clientName}}
                    </td>
                    
                    <td>
                        {{ $project->clientType}}
                    </td>
                    
                    <td>
                        {{ $project->notes}}
                    </td>
                    
                    <td>
                        {{ $project->fileNamePosition_VisitID}}
                    </td>
                    
                    <td>
                        {{ $project->fileNamePosition_SubjectID}}
                    </td>
                    
                    <td>
                        {{ $project->fileNamePosition_Date}}
                    </td>
                    
                    <td>
                        <div class="btn-group float-right">
                             {!! link_to_route('projects.edit', "Edit", [$project->id], ['class' => 'btn btn-primary btn-sm hidden-print ']) !!}
                    
                            {!! link_to_route('projects.show', "Show", [$project->id],['class' => 'btn btn-secondary btn-sm hidden-print ']) !!}
                        </div>
                       
                    </td>
            @endforeach

        </tbody>
      </table> 

    @else
    
        <p>
            No projects found!
        </p>
        
    @endif

@endsection