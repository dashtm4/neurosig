    <div class="form-group">
        {!!Form::label('qualificationDate','Qualification Date')!!}
        {!!Form::date('qualificationDate',null, ['id' =>'qualificationDate','class' =>'form-control','placeholder' =>'Enter Qualification Date']) !!}
    </div>

    <div class="form-group">
        {!!Form::label('contractDate','Contract Date')!!}
        {!!Form::date('contractDate',null, ['id' =>'contractDate','class' =>'form-control','placeholder' =>'Enter Contract Date']) !!}
    </div>
 
     <div class="form-group">
        {!!Form::label('projectStartDate','Project Start Date')!!}
        {!!Form::date('projectStartDate',null, ['id' =>'projectStartDate','class' =>'form-control','placeholder' =>'Enter Project Start Date']) !!}
    </div>

     <div class="form-group">
        {!!Form::label('trainingStartDate','Training Start Date')!!}
        {!!Form::date('trainingStartDate',null, ['id' =>'trainingStartDate','class' =>'form-control','placeholder' =>'Enter Training Start Date']) !!}
    </div>
    
     <div class="form-group">
        {!!Form::label('firstSubjectDate','First Subject Date')!!}
        {!!Form::date('firstSubjectDate',null, ['id' =>'firstSubjectDate','class' =>'form-control','placeholder' =>'Enter First Subject Date']) !!}
    </div>
    
     <div class="form-group">
        {!!Form::label('lastSubjectDate','Last Subject Date')!!}
        {!!Form::date('lastSubjectDate',null, ['id' =>'lastSubjectDate','class' =>'form-control','placeholder' =>'Enter Last Subject Date']) !!}
    </div>
    
     <div class="form-group">
        {!!Form::label('releaseDate','Last Release Date')!!}
        {!!Form::date('releaseDate',null, ['id' =>'releaseDate','class' =>'form-control','placeholder' =>'Enter Last Release Date']) !!}
    </div>
    
     <div class="form-group">
        {!!Form::label('retentionEndDate','Retention End Date')!!}
        {!!Form::date('retentionEndDate',null, ['id' =>'lastSubjectDate','class' =>'form-control','placeholder' =>'Enter Retention End Date']) !!}
    </div>