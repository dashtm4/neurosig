<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->string('identifier', 50);
            
            $table->string('fullName');
            $table->string('email');
            $table->string('telephone', 150)->nullable()->default(null);
            $table->string('siteIdentifier', 50);
            
            $table->string('category', 50);
            $table->string('subCategory', 50);
            $table->string('subject', 150);
            $table->text('message');
            
            $table->string('status')->nullable();
            $table->string('priority', 50);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
