<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('telephone', 150)->nullable()->after('email');
            $table->string('privacy', 150)->nullable()->after('role');
            $table->text('notes')->nullable()->after('role');
            $table->string('status', 150)->nullable()->after('role');

        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('telephone');
            $table->dropColumn('privacy');
            $table->dropColumn('notes');
            $table->dropColumn('status');

        });
    }
}