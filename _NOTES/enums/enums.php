return [
    'type' => [
        'None' => "None",
        'Electrical Noise' => "Electrical Noise",
        'Eyes' => "Eyes",
        'Gel Bridging' => "Gel Bridging",
        'Bad Electrode(s)' => "Bad Electrode(s)",
        'Other Artifacts' => "Other Artifacts",
        'Response Button' => "Response Button",
        'Missing Session(s)' => "Missing Session(s)",
        'Other' => "Other"
    ]
];

return [
    'category' => [
        'Quality' => "Quality",
        'Support' => "Support",
        'Niceness' => "Niceness",
        'Other' => "Other",
    ]
];