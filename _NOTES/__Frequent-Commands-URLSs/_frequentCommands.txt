php artisan make:seeder PropertiesTable

composer dump-autoload
 composer clearcache

composer update

php artisan make:model Suite --migration

php artisan make:controller PhotoController

php artisan make:controller admin/LeasesController

php artisan clear-compiled

php artisan make:migration create_users_table

php artisan migrate:refresh --seed

To run one migration put into its own folder and say this:

    php artisan migrate --path=/database/migrations/separate

migrate:refresh 
command will roll back all of your migrations and then execute the  migrate command. 
This command effectively re-creates your entire database:


To rollback the last migration:
php artisan migrate:rollback --path=/database/migrations/separate

$ php artisan db:seed --class=App\\ReportSeeder

<?php die("HELLO") ?>



To upgrade from a minor version of Laraval:
    sudo composer update
    
File location for the Downloads
        /web/storage/app/storage/file

To dump the database:
mysqldump c9 > bk-2019-03-19.sql

$ npm run dev


Git Related:
    git status
    
    git add .
    git commit -a -m "updated to latest laravel version" 
    git push
    
    GIT Reference:
    http://gitref.org/basic/