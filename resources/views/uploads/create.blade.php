<!--Create A Client-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Upload File <span id="counter"></span></h1>

    </div>
</div>

<div class="container clearfix">

    {!! Form::open(['route' => 'uploads.store', 
        'id' => 'tip-form', 
        'class' => 'form', 
        'novalidate' => 
        'novalidate', 
        'files' => true
        ])
    !!}

    @if (Auth::User()->projects != null AND Auth::User()->projects()->count() > 1)

    <div class="form-group">
        {!! Form::label('project_id','Project') !!}
        {!! Form::select('project_id',
            Auth::User()->projects()->pluck('projects.name', 'projects.id'),
            null, 
            [
                'id'    => 'project_id',
                'class' =>'form-control',
                'placeholder' => 'Select Project'
                ]
            ) !!}
    </div>
    
    @else 
    
    {!! Form::hidden('project_id', Auth::User()->projects()->first() == null ? '' : Auth::User()->projects()->first()->id) !!}
    
    @endif

    @if(Auth::User()->sites()->count() > 1)

    <div class="form-group">
        {!! Form::label('site_id','Site') !!}
        {!! Form::select('site_id',
            Auth::User()->sites()->pluck('sites.name', 'sites.id'),
            null, 
            [
                'id'    => 'category',
                'class' =>'form-control',
                'placeholder' => 'Select Site']
            ) !!}
    </div>
    
    @else 
    
        {!! Form::hidden(
            'site_id', 
            Auth::User()->sites()->first()->id) 
        !!}
    
    @endif
    
    <div class="form-group">
        {!! Form::label('EEG') !!}
        {!! Form::file('eeg', null) !!}
    </div>
    
    {!! Form::submit('Upload File', [
        'class'=>'btn btn-primary hidden-print float-left'
        ]) 
    !!}
      
    {!! Form::close() !!}


</div>
</div>
  

@endsection