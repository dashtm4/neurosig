@extends('layouts.app')

    @section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
            
            @if (Auth::User()->projects() != null AND Auth::User()->projects()->count() > 1)
                <div class="form-group">
                    {!! Form::label('project_id','Project') !!}
                    {!! Form::select('project_id',
                        Auth::User()->projects()->pluck('projects.name', 'projects.id'),
                        null, 
                        [
                            'id'    => 'project_id',
                            'class' =>'form-control',
                            'placeholder' => 'Select Project'
                            ]
                        ) !!}
                </div>
                
                @else 
                
                {!! Form::hidden('project_id', Auth::User()->projects()->first()->id, ['id' => 'project_id']) !!}
                
                @endif
            
                @if(Auth::User()->sites()->count() > 1)
            
                <div class="form-group">
                    {!! Form::label('site_id','Site') !!}
                    {!! Form::select('site_id',
                        Auth::User()->sites()->first->pluck('sites.name', 'sites.id'),
                        null, 
                        [
                            'id'    => 'category',
                            'class' =>'form-control',
                            'placeholder' => 'Select Site']
                        ) !!}
                </div>
                
                @else 
                
                    {!! Form::hidden(
                        'site_id', 
                        Auth::User()->sites()->first()->id,
                        ['id' => 'site_id']) 
                    !!}
                
                @endif
                <uploader></uploader>
            </div>
        </div>
    </div>
    @endsection