<!--Downloads Index - The Reviewer's Index View -->


@extends ('layouts.app')

@section ('content')
    
    <div class="page-header">
        <div class="container">
            <h1>Your Review Files</h1>
        </div>
    </div>
    
    <div class = "container clearfix">
        {!! Form::open(['route' => 'downloads.index', 'method' => 'get', 'id' => 'tip-form', 'class' => 'form', 'novalidate' => 'novalidate', 'files' => false]) !!}
    
    <!--Project Filter-->
    <div class="form-group hidden-print">
        {!!Form::label('project_id','Filter by Project')!!}
        {!! Form::select('project_id', App\Project::select('id', DB::raw('CONCAT("(ID: ", identifier, ") ", name) AS name'))->pluck('name', 'id'),
        null, 
        ['placeholder' => 'Select Project', 'class' => 'form-control'])!!}
    </div>

    <!--Site-->
    <div class="form-group hidden-print">
        {!!Form::label('site_id','Filter by Site')!!}
        {!! Form::select('site_id', App\Site::select('id', DB::raw('CONCAT("(ID: ", identifier, ") ", name) AS name'))->pluck('name', 'id'),
        null, 
        ['placeholder' => 'Select Site', 'class' => 'form-control'])!!}
    </div>

    <!--File Type-->  
    <div class="form-group hidden-print">
        {!! Form::label('file_type','File Type') !!}

        {!! Form::select('file_type',
            [
                null=>'Select File Type',
                'Study' => 'Study',
                'Practice'   => 'Practice',
                'Training'   => 'Training',
                'Other'      => 'Other',
                'Test'       => 'Testing'
            ],
            'Select File Type',
            [
                'id'    => 'file_type',
                'class' =>'form-control'
            ]
            
            )!!}
    </div>


    <!-- Parents Only -->
    <div class="form-group hidden-print">
        {!! Form::label('parent','Parents ') !!}
        <input type="checkbox" value="0" id="parent" name="parent" {{$hasParentId ? "checked=checked" : ""}}/> 
        
    <!-- No Rating, Parents Only and Study Data (See Downloads Controller) --> 
        {!!Form::label('rating','No Rating - Study')!!}
        {!!Form::checkbox('rating', 1,  $hasRating) !!}

    <!-- Remove Status--> 
        {!!Form::label('no_status','No Status')!!}
        {!!Form::checkbox('no_status', 1,  $hasNoStatus) !!}
    
    <!-- Show Deleted -->
        {!!Form::label('show_deleted','Show Deleted')!!}
        {!!Form::checkbox('show_deleted', 1,  $hasShowDeleted) !!}
    
    
    </div>

    <div class="btn-group pull-left">
        {!! Form::submit('Filter', array('class'=>'btn btn-info hidden-print')) !!}
    
        <a href="/downloads?clear=true" class="btn btn-success hidden-print">Clear Filter</a>
    </div>
    
    
    {!! Form::close() !!}
        
    </div>
    
    
    
    @if ($files->count() > 0)
    
        <table class="table table-responsive">
            <thead>
              <tr>
                    <th>Key</th>
                    <th>Parent</th>
                    <th>File Type</th>
                    <th>Project Key</th>
                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'name'])}}" class="{{ $sortBy == 'name' ? 'text-info' : '' }}">Name</a>
                    </th>
                    <th>EEG Date</th>
                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'subject_id'])}}" class="{{ $sortBy == 'subject_id' ? 'text-info' : '' }}">Subject Id</a>
                    </th>
                    <th>Visit Id</th>
                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'site_id'])}}" class="{{ $sortBy == 'site_id' ? 'text-info' : '' }}">Site</a>
                    </th>
                    <th>Rating</th>
                    <th>Quality Category</th>
                    <th>Internal Status</th>
                    <!--<th>Internal Comment</th>-->
                    <th>Size (MB)</th>
                    <th>Uploaded By</th>
                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'created_at'])}}" class="{{ $sortBy == 'created_at' ? 'text-info' : '' }}">Uploaded On</a>
                    </th>
                   
                    <th></th>
              </tr>
            </thead>
                    
            <tbody>
        
                @foreach ($files as $file)
               
                    <tr style="list-style: none;{{$file->trashed() ? "background-color:#ffe0b2 !important;":''}}" class="{{$file->trashed() ? "bg-warning" : ""}}">
                        
                        <!-- Key -->
                        <td>
                            {!! link_to_route('files.edit', $file->id, [ $file->id ]) !!}
                        </td>
                        
                        <!-- Parent -->      
                        <td>
                            {{ $file->parent_id }}
                        </td>
                        
                        <!-- File Type -->                         
                        <td>
                            @if (! is_null($file->file_type))
                                {{ $file->file_type }}
                            @else
                                &nbsp;
                            @endif
                        </td>
                        
                        <!-- Project Key-->                         
                        <td>
                            @if (! is_null($file->project_id))
                                {{ $file->project_id }}
                            @else
                                &nbsp;
                            @endif
                        </td>
            
                         <!-- Name -->                   
                        <td>
                           {!! link_to_route('files.reviews.edit', $file->name, [$file->id, $file->id]) !!}
            
                        </td>
                        
                        <!-- EEG Date -->                     
                        <td class="text-nowrap">
                            @if (! is_null($file->eeg_date))
                                {{ $file->eeg_date }}
                            @else
                                &nbsp;
                            @endif
                        </td>     
                        
                        <!-- Subject ID -->     
                        <td>
                            @if (! is_null($file->subject_id))
                                {{ $file->subject_id }}
                            @else
                                &nbsp;
                            @endif
                        </td>
                        
                        <!-- Visit ID -->                     
                        <td>
                            @if (! is_null($file->subject_visit))
                                {{ $file->subject_visit }}
                            @else
                                &nbsp;
                            @endif
                        </td>
                        
                        <!-- Site Number -->                   
                        <td>
                            {{ $file->site == null ? "missing" : $file->site->identifier }}
                        </td>
                        
                        <!-- Quality Rating -->                    
                        <td>
                            {{ $file->rating }}
                        </td>
                        
                         <!--Quality Category -->
                        <td>
                            @if (! is_null($file->qualityCategory))
                                {{ $file->qualityCategory }}
                            @else
                                &nbsp;
                            @endif
                        </td>
                        
                         <!--Internal Status-->     
                        <td>
                            @if (! is_null($file->status))
                                {{ $file->status }}
                            @else
                                &nbsp;
                            @endif
                        </td>
                        
                         <!--Internal Comment-->     
                        <!--<td>-->
                        <!--    @if (! is_null($file->comment))-->
                        <!--        {{ $file->comment }}-->
                        <!--    @else-->
                        <!--        &nbsp;-->
                        <!--    @endif-->
                        <!--</td> -->
                        
                        <!-- File Size -->   
                        <td>
                            @if (! is_null($file->size)) 
                               @if ($file->size < 1000) 
                                 < 1MB
                               @else
                                 {{ round($file->size / 1024 / 1024, 3) }}
                               @endif
                            @else 
                               N/A 
                            @endif
                        </td>
                        
                        <!-- User Uploaded-->  
                        <td>
                            {{ $file->user->fullName}} ({{ $file->user->email }})
                             @if ($file->user->trashed())
                            &nbsp;(DELETED)
                            @endif
                        </td>
                        
                        <!-- Date Created-->  
                        <td>
                            {{ $file->created_at }}
                        </td>
                        
                        <td>
                            <div class="btn-group float-right">
                                {!! link_to_route('files.reviews.edit', "Review", [$file->id, $file->id],['class' => 'btn btn-success btn-sm hidden-print ']) !!}
                                {!! link_to_route('downloads.download', "Download", [$file->id], ['class' => 'hidden-print btn btn-info btn-sm hidden-print ']) !!}
                            </div>
                            
                        </td>
                @endforeach
            </tbody>
        </table>

    @else
    
    <div class="container pt-3 clearfix">
    
        <div class="alert alert-dismissible alert-primary">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4 class="alert-heading">Congratulations!</h4>
          <p class="mb-0">All caught up! No more reviews to create right now.</p>
        </div>
    </div>
    

        
    @endif

@endsection