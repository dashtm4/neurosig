@extends ('layouts.app')

@section ('content')

    <h1>Contact Us</h1>
  
<div class="row">   

  <div class="col-md-6">
    
    {!! Form::open(['route' => 'contact.store', 'class' => 'form', 'novalidate' => 'novalidate']) !!}

<fieldset>
    
    <div class="form-group">
        {!!Form::label('name','Your Name')!!}
        {!!Form::text('name', null, ['id' =>'identifIer','class' =>'form-control','placeholder' =>'Enter your name',]) !!}
    </div>
    
    <div class="form-group">
        {!!Form::label('email','Your E-mail Address')!!}
        {!!Form::text('email', null, ['id' =>'email','class' =>'form-control','placeholder' =>'Enter e-mail address',]) !!}
    </div>

    <div class="form-group">
        {!!Form::label('msg','Your Message')!!}
        {!!Form::textarea('msg',null, ['class' =>'form-control','placeholder' =>'Enter message',]) !!}
    </div>

</fieldset>
    
    {!! Form::submit('Contact Us', array('class'=>'btn btn-primary hidden-print')) !!}
      
    {!! Form::close() !!}

  </div>
</div>
<br/>
@endsection



