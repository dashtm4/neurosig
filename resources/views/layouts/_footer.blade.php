<footer class="footer">
  <div class="container-fluid" style="background-color: #363839; padding-top:10px;">
    <ul style="list-style-type:none">
        <li>
            <p class="text-muted">
                Copyright 2018 NeuroSig, Inc. | <a href="https://neurosig.com/privacy/" target="_blank">Privacy Policy</a>
            </p>

        </li>
    </ul>
  </div>
</footer>