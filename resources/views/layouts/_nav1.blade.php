<!--NeuroSig Main Menu September 9, 2018-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" ari-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
          
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                
               <!-- Admin Ability for Admin Users and System Developers -->  
               @if (
                        Auth::user() 
                        and (Auth::user()->is_admin == 1 or Auth::user()->is_system == 1)
                    )
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Admin
                        </a>
                        
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            
                            {{ link_to_route('users.index', 'Users', null, ['class' => 'dropdown-item']) }}
                            {{ link_to_route('clients.index', 'Clients', null, ['class' => 'dropdown-item']) }}
                            {{ link_to_route('projects.index', 'Projects', null, ['class' => 'dropdown-item']) }}
                            {{ link_to_route('sites.index', 'Sites', null, ['class' => 'dropdown-item']) }}
                            {{ link_to_route('files.index', 'Files', null , ['class' => 'dropdown-item']) }}
                            {{ link_to_route('privacies.index', 'Privacies', null , ['class' => 'dropdown-item']) }}
                            <div class="dropdown-divider"></div>
                            @if (Auth::user()->is_system)
                                <a href="" class="dropdown-item">Secret feature</a>
                            @endif
                            
                            
                        </div>
                    </li>
                @endif
                        
                <!--Add Upload Ability for Site, Admin & System Useres -->
                @if (
                        Auth::user() 
                        and (Auth::user()->is_site == 1 
                        or Auth::user()->is_system == 1 
                        or Auth::user()->is_admin == 1 
                    ) 
                    
                    and Auth::user()->sites()->count() > 0)
    
                    <li class="nav-item">
                        {{ link_to_route('uploads.create', 'Upload File', null, ['class' => 'nav-link']) }}
                    </li>
                    
                    <li class="nav-item">
                        {{ link_to_route('uploader', 'Upload Files (NEW)', null, ['class' => 'nav-link']) }}
                    </li>
    
                @endif

                <!--Add Download & Review Ability for Expert, Admin & System Useres -->                       
                @if (
                        Auth::User() 
                        and (Auth::user()->is_expert == 1 
                        or Auth::user()->is_system == 1 
                        or Auth::user()->is_admin == 1)
                    )
                        
                    <li class="nav-item">
                        {{ link_to_route('downloads.index', 'Review', null, ['class' => 'nav-link']) }}
                    </li>
                    
                @endif
                
                <!--Add Snippets -->                       
                @if (
                        Auth::User() 
                        and (Auth::user()->is_expert == 1 
                        or Auth::user()->is_system == 1 
                        or Auth::user()->is_admin == 1)
                    )
                        
                    <li class="nav-item">
                        {{ link_to_route('snippets.index', 'Snippets', null, ['class' => 'nav-link']) }}
                    </li>
                    
                @endif
                
                        
                <!--View Your Files - Site and Monitor Users -->                 
                @if (Auth::user()
                    and (Auth::user()->is_site == 1 or Auth::user()->is_monitor == 1)
    
                    )
                    <li class="nav-item">
                        <a href="/files" class="nav-link">View Your Site Files</a>
                    </li>
                @endif
                        
                <!--View Your Files - Project User -->                    
                @if (Auth::user() and Auth::user()->is_project == 1)
                    <li class="nav-item">
                        <a href="/projectfiles" class="nav-link">View Your Project Files</a>
                    </li>
                @endif
                
                @if (Auth::User())
                <li class="nav-item">
                    <a href="https://support.neurosig.com/" class="nav-link" target="_blank">Support</a>
                </li>

                <!--Support and Return Shipping-->
                <!--<li class="nav-item dropdown">-->
                <!--    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
                <!--      Shipping-->
                <!--    </a>-->
                <!--    <div class="dropdown-menu" aria-labelledby="navbarDropdown">-->
                <!--        <a class="dropdown-item" href="https://support.neurosig.com/wp-content/uploads/2018/06/NeuroSig-Return-Shipping-Instructions-Europe-2018-06-11-R1.pdf">-->
                <!--          NeuroSig Return Shipping Instructons - Europe-->
                <!--        </a>-->
                <!--        <a class="dropdown-item" href="https://support.neurosig.com/wp-content/uploads/2018/06/NeuroSig-Study-System-Checklist-2018-06-13-R0.pdf">-->
                <!--          NeuroSig Study System Checklist-->
                <!--        </a>-->
                <!--        <div class="dropdown-divider"></div>-->
                <!--        <a class="dropdown-item" href="https://support.neurosig.com/wp-content/uploads/2018/06/NeuroSig-Shipping-Labels-2018-06-13-R0.pdf">-->
                <!--          NeuroSig Shipping Labels-->
                <!--        </a>-->
                <!--    </div>-->
                <!--</li>-->
                    
                <li class="nav-item">
                    <a href="https://support.neurosig.com/contact/" class="nav-link" target="_blank">Contact</a>
                </li>
                @endif
    
            </ul>
            
            <ul class="navbar-nav ml-auto">
                @if (Auth::guest())
                    <li class="nav-item">
                        <a href="{{ url('/login') }}" class="nav-link">Login</a>
                    </li>
                @else
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {{Auth::user()->name}}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @if (Auth::user()->is_system == 1)
                            <a class="dropdown-item" href="{{ route('token-settings') }}">Token Settings</a>
                            <a class="dropdown-item" href="/admin/users">System</a>
    
                        @elseif (Auth::user()->is_admin == 1)
                            <a class="dropdown-item" href="#">Admin</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
