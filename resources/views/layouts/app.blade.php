<!DOCTYPE html>
<html lang="en"
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('/') }}" />


    <title>NeuroSig</title>


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="{{ url('/css/dropzone.css') }}">
    
    @if(Route::currentRouteName() == 'home')
        <link rel="stylesheet" href="/css/home.css">
    @endif

</head>

<body id="app-layout">
    
    <div id="app">
    
    <header>
        <div class="container">
            <!-- Branding Image -->
            
            <a class="navbar-brand" href="{{ url('/home') }}">
                <img src="/img/NeuroSig-Star-In-Brain-Logo-320x80.png" alt="NeuroSig">
            </a>
        </div>
    </header>

    @include('layouts._nav1')

    @include('layouts._alerts')
      
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @yield('content')
            </div>
        </div>
    </div>
    
  <!--Footer    -->
  @include('layouts._footer')
  
</div>

  <!--JavaScript    -->
 
    
    <script src="/js/app.js"></script>
    
    @yield('javascript')


</body>
</html>