<section class="user-alerts">
    @if(Session::has('message'))

        <div class="alert alert-info">
            @if(Session::has('message'))
                {{ Session::get('message') }}
            @endif
        </div>
    @endif
    
    @if (count($errors) > 0)
        <div class="alert alert-warning">
            Errors detected on page:
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
        </div>
    @endif
    
    @if (
        Auth::user() 
        and Auth::needPrivacy()
    )
        <div id="privacy-update" class="alert alert-dismissible alert-primary">
            <div class ="container">
                <div class="row">
                    <div class = "col-md-8 mb-2">
                        <strong> NeuroSig uses cookies to give you the best, most relevant experience on our websites.  For users with accounts, we store your contact information.  Select Accept to approve.</strong>
                    </div>
                    
                    <div class="col-md-1 mb-2">
                        <button type="button" data-user-id="{{Auth::user()->id}}" class="btn btn-secondary" v-on:click="updatePrivacy">Accept</button>
                    </div>
                    <div class="col-md-2">
                        <a href="https://neurosig.com/privacy/" class="btn btn-secondary" role="button" target="_blank">View Privacy Policy</a>
                    </div>
                    
                </div>
            </div>
            
        </div>
    
    @endif
</section>
