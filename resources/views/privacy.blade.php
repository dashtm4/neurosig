@extends ('layouts.app')

@section ('content')

<header class="masthead prepare">
    <div class="container">
        <div class="intro-text">
            <h1>NeuroSig Privacy Policy</h1>
        </div>
    </div>
</header>
<section>
    <div class="container">
        <p> NeuroSig.com’s “NeuroSig.com will be hereinafter referred to as [SITE].COM” <strong>PRIVACY POLICY </strong> applies
            to you<strong> (</strong>the<strong> “User,” “Your,” </strong>and<strong> “You”) </strong>as You use<strong>
                [</strong>SITE].COM<strong> (“[SITE].COM,” “We,” Us,” </strong>and <strong> “Our”). </strong>We respect
            privacy and work hard to bring You an
            informative, user-friendly, and privacy-protecting Web site. This Privacy
            Policy is in place to explain how we collect information and use to provide
            You with the best service and experience possible. </p>
        <p> By navigating to and using [SITE].com’s Web site services (the “ <strong>Services</strong>”), You agree to
            this Privacy Policy. <u> <strong> We ask You do not use our Services if You do not agree to this
                Privacy Policy. </strong> </u></p>
        <ol type="I">
            <li>
                <h2>What Information is Collected By Our Website</h2>
            </li>
        </ol>
        <ol type="A">
            <li>
                <p><strong>GENERAL.</strong> [SITE].com collects two basic types of information through Our
                    Services. We gather personally identifiable information, which will
                    identify an individual (“<strong>Personal Information</strong>”)
                    and non-personally identifiable information, which includes
                    aggregated information, demographic information, IP addresses and
                    any other information that does not reveal a person’s specific
                    identity (“<strong>Non-Personal Information</strong>”) </p>
                <ol type="a">
                    <li>
                        <p><a name="_GoBack"></a> <strong>Personal Information.</strong> It is not necessary to provide
                            Personal Information to use
                            the site. Although, You will need to provide certain
                            Personal Information to use various Services on Our Web
                            site. For example, when you sign up with [SITE].com, You
                            create a personal account, tied directly to Your personal
                            email address and Your name, used for uploading, storing,
                            and identifying Your account. You may wish to input your
                            billing information. In order to do so, We will need
                            verification and payment information, such as Your billing
                            address, your billing name, a credit card number with its
                            expiration date, paypal account information, or other
                            identifying information. Email invitations and/or
                            correspondence may be made from Our Web site as well. As
                            such, individuals You invite and/or correspond with may be
                            added to Your account for easy access at a later date. </p>
                    </li>
                    <li>
                        <p><strong>Non-Personal Information.</strong> Upon visiting and interacting with [SITE].com and
                            Our
                            Services, We may collect and record Non-Personal
                            Information, including: </p>
                        <ol type="i">
                            <li>
                                <p> Internet Protocol (“<strong>IP</strong>”) address -
                                    IP addresses may be collected so We may better
                                    serve You by understanding from where our visitors
                                    are originating. This is a numeric code and address
                                    which enables identification of a given computer
                                    connected to the Internet. This code can often
                                    identify the country, state and city from which
                                    Your computer is connecting to the Internet. Your
                                    IP address transmits whenever You communicate
                                    online or surf the Web so that the content at which
                                    You are looking and the people You are
                                    communicating with can find Your computer on the
                                    network. </p>
                            </li>
                            <li>
                                <p> Browser type, version, and operating system -
                                    Knowing the browsers (e.g Firefox 3.5.3 or Internet
                                    Explorer 7) and operating systems (e.g. Mac OS X,
                                    Opera, Safari, Windows Vista) used by Our visitors
                                    helps Us optimize the display and delivery of Our
                                    site for visitors accessing it from differing
                                    software platforms. </p>
                            </li>
                            <li>
                                <p> Screen Size - Knowledge of this allows Us to
                                    optimize the display and delivery of Our site for
                                    visitors. </p>
                            </li>
                            <li>
                                <p> Technologies supported by Your browser for software
                                    such as Java, Adobe Flash, Adobe Director, and
                                    Adobe PDF, and the audio formats supported by Your
                                    browser for RealPlayer, QuickTime, and Windows
                                    Media Player - Knowing the formats supported by
                                    visitors’ browsers helps Us optimize the content on
                                    Our site for visitors accessing it with different
                                    browser configurations. </p>
                            </li>
                            <li>
                                <p> URL of the page that directed you to Our site - If
                                    you arrive at Our Web site through a link on
                                    another Web site - a blog, newspaper article, or
                                    search engine, for example - our Web server will
                                    record the address of the Web page that referred
                                    You to our site. If You arrive at our Web site by
                                    clicking on a search result returned by a search
                                    engine, Our server will record the search terms
                                    that You used. </p>
                            </li>
                            <li>
                                <p> Whether You have bookmarked our Web site on Your Web browser. </p>
                            </li>
                            <li>
                                <p> The Web pages within Our site that you visit, the
                                    Web page You visit first on Our site (the entry
                                    page), and the Web page You visit last on Our site
                                    (the exit page). </p>
                            </li>
                            <li>
                                <p> Bandwidth used - The total number of bytes
                                    downloaded when You browse our site. </p>
                            </li>
                            <li>
                                <p> Amount of time You spend during a visit to our
                                    site. </p>
                            </li>
                            <li>
                                <p> Time and date of Your site visit. </p>
                            </li>
                        </ol>
                    </li>
                </ol>
                <p> [SITE].com may use first-party cookies, as well as pixel tags, web beacons,
                    clear GIFs and similar technologies in order to deliver a better service
                    and user-friendly experience. Cookies are small files that Web servers
                    place on a user’s hard drive. They can serve several functions such as
                    allowing the Web site to identify You as a previous visitor each time you
                    access a site; tracking what information You view at a site; and they can
                    be used to help a Web site tailor screens for each customer’s preference.
                    If You are concerned about the potential use of the information gathered
                    from Your computer by cookies, You can set your browser to prompt You
                    before it accepts a cookie. Most Internet browsers have settings that let
                    You identify and/or reject cookies, some allow for plug-ins that assist
                    with Your concerns. </p>
                <p> We use Google Analytics to analyze the use of this website. Google
                    Analytics generates statistical and other information about website use by
                    means of cookies, which are stored on users' computers. The information
                    generated relating to our website Google Analytics uses to create reports
                    about the use of the website. Google will store this information. Google's
                    privacy policy is available at: http://www.google.com/privacypolicy.html.
                    See also http://www.google.com/intl/en/analytics/privacyoverview.html. We
                    also may use additional third-party Web site analytics tools as We
                    determine a need to better serve You. </p>
            </li>
        </ol>
        <ol start="2" type="I">
            <li>
                <h2>How Do We Use The Information We Collect.</h2>
            </li>
        </ol>
        <ol type="A">
            <li>
                <p><strong>PERSONAL INFORMATION.</strong> The following are ways We may use Personal
                    Information: </p>
                <ol type="a">
                    <li><strong>Finalize Membership.</strong> We will use Personal Information to
                        finalize payments for the purpose of Your Membership to
                        [SITE].com.
                    </li>
                    <li><strong>Administrative Communications.</strong> We may need to send service
                        announcements to You using your email address. This use is
                        necessary to perform maintenance and inform You of
                        necessary changes to Our service or your account. You may
                        not unsubscribe from these.
                    </li>
                    <li><strong>Miscellaneous Communications.</strong> We may notify You of
                        additions or changes to Our service, or other
                        communications that enhance Your experience. We may also
                        send third party information to You after thoroughly
                        reviewing the information. You may unsubscribe from these
                        notifications by taking the appropriate steps outlined in
                        the enclosed Opt-Out section below.
                    </li>
                    <li><strong>Comments.</strong> We may offer You the option, and You may choose
                        to place comments, appreciation or otherwise suggestions.
                        Any information You share is public information.
                    </li>
                    <li><strong>Customer Service.</strong> By sending Us an electronic mail message
                        You may be sending us personally-identifying information.
                        In these cases, We may retain the information as long as
                        necessary to respond to Your request or otherwise resolve
                        the subject matter of Your e-mail. Please be aware that
                        email is not necessarily secure from interception or
                        misdirection. For Your own protection You may wish to
                        communicate sensitive information using a method other than
                        electronic mail, such as with the U.S. Postal Service.
                    </li>
                    <li><strong>Memberships.</strong> [SITE].com provides memberships which allows
                        individuals a private link to upload files in order to
                        share with [SITE].com. By sharing information, you will be
                        using [SITE].com’s FTP service.
                    </li>
                    <li><strong>Note.</strong> Subject to You becoming a member, We do not use email
                        addresses You use with your account for direct marketing
                        purposes.
                    </li>
                </ol>
            </li>
            <li>
                <p><strong>NON-PERSONAL INFORMATION.</strong> We may use Non-Personal Information for
                    any purpose and reserve the right to share the non-identifying,
                    Non-Personal Information with third parties. If combination of
                    Non-Personal Information and Personal Information occurs, we will
                    act as if it is Personal Information so long as it combined. </p>
            </li>
        </ol>
        <ol start="3" type="I">
            <li>
                <h2>Disclosure of Personal Information.</h2>
            </li>
        </ol>
        <ol type="A">
            <li>
                <p><strong>THIRD PARTY SERVICE PROVIDERS.</strong> We share Personal Information, as necessary, with
                    carefully
                    screened third-party service providers to provide You the services
                    requested. For example, credit card authorization companies.
                    Advertisers on Our Web site use their own software on Our site. The
                    advertiser’s software may use cookies or other tracking procedures
                    to offer the most relevant product to You – if applicable. </p>
            </li>
            <li>
                <p><strong>THIRD PARTY COMMUNICATIONS. </strong> [SITE].com also may share Personal Information with
                    third parties
                    for such third parties' direct marketing purposes. Such third
                    parties to whom we transfer Personal Information for direct
                    marketing purposes will handle, store, retain, use, share and
                    access Personal Information in accordance with their own privacy
                    policies. It is your responsibility to carefully review such terms
                    and conditions and privacy policies, and [SITE].com disclaims any
                    and all liability relating thereto. If You would prefer that we do
                    not share your Personal Information with such third parties for
                    their direct marketing purposes, you may submit an opt-out request
                    as described in the Opt-Out section below. </p>
            </li>
            <li>
                <p><strong>LAW AND HARM. </strong> We may disclose your information if We believe that it is
                    reasonably necessary to comply with a law, regulation or legal
                    request, as well as to protect other’s safety. We may also disclose
                    Your information to address fraud, security or technical issues, as
                    well as to protect [SITE].com’s rights or property. </p>
            </li>
            <li>
                <p><strong>BUSINESS TRANSFER. </strong> In the event that [SITE].com is involved in a bankruptcy,
                    merger,
                    acquisition, reorganization or sale of assets, your information may
                    be sold or transferred as part of that transaction. The promises in
                    this privacy policy will apply to your information as transferred
                    to the new entity. </p>
            </li>
            <li>
                <p><strong>ADDITIONAL INFORMATION.</strong> You may provide Us with additional information to make
                    public, such
                    as biographies, or your geographic location. From time-to-time You
                    may also provide other personal information, as Our new Services
                    may offer. </p>
            </li>
        </ol>
        <ol start="4" type="I">
            <li>
                <h2>Updating Your Personal Information.</h2>
                <p> Please update Your account information from time to time. In order to help
                    ensure privacy and prevent negative consequences, You may want to change
                    your password. If You lose or forget your password, either Contact Us or
                    use the online forms for retrieval. </p>
            </li>
        </ol>
        <ol start="5" type="I">
            <li>
                <h2>Security.</h2>
                <p> No data transmission over the Internet or data storage system can be 100%
                    protected and secure. We strive to have reasonable security measures in
                    place to help protect against misuse, alteration, loss or other negative
                    consequence of information under our control. We in no way warrant the
                    security of any information You transmit to Us through or in connection
                    with Our Services or stored by Us. You understand, acknowledge and agree
                    that any information You transmit through Our Services or upload for
                    storage in connection with Our Services, is done so at Your own risk.
                    Please immediately contact us if You believe any transmission with Us is
                    compromised. </p>
            </li>
        </ol>
        <ol start="6" type="I">
            <li>
                <h2>Protecting The Privacy of Children.</h2>
                <h3> Our Policy Towards Children: </h3>
                <p> Our Services are not directed to people under 13. If you become aware that
                    Your child has provided us with personal information without Your consent,
                    please contact us at info@NeuroSig.com. We do not knowingly collect
                    personal information from children under 13. If we become aware that a
                    child under 13 has provided us with personal information, we take steps to
                    remove such information and terminate the child's account. If You are under
                    13, do not register with [SITE].com. </p>
            </li>
        </ol>
        <ol start="7" type="I">
            <li>
                <h2>Third Party Sites.</h2>
                <p> The Services may contain links to third party websites that are not under
                    Our control. We are not responsible for the privacy practices or the
                    contents of any linked site, or any link contained in any linked site. We
                    provide other site’s links only as a convenience. The inclusion of a link
                    on Our site does not imply endorsement of the linked site by Us. If You
                    provide any Personal Information through any such third party website, Your
                    transaction will occur on such third party's website and not [SITE].com and
                    the Personal Information You provide will be collected by and controlled by
                    the privacy policy of that third party. You should familiarize Yourself
                    with the privacy policies and practices of any such third parties. <strong> PLEASE NOTE THAT THIS
                        PRIVACY POLICY DOES NOT ADDRESS THE PRIVACY OR
                        INFORMATION PRACTICES OF ANY THIRD PARTIES. </strong></p>
                <p><strong>EMAIL FROM WEB SITE:</strong> You may email articles from our site to friends and family.
                    Emailing these
                    articles to Your friends will not result in cookies being placed on your
                    computer. </p>
            </li>
        </ol>
        <ol start="8" type="I">
            <li>
                <h2>Changes/Notification.</h2>
                <p> We may revise this Privacy Policy from time to time. The most current
                    version of the policy will govern Our use of Your information and will
                    always be at <a href="http://myowneulogy.com/privacy">http://[site].com/privacy</a>. If
                    we make a substantial changes to this Privacy Policy, We will post a large
                    notification on Our homepage and include a notice to a mailing list about
                    Our Privacy Policy. </p>
            </li>
        </ol>
        <ol start="9" type="I">
            <li>
                <h2>Jurisdictional Issues.</h2>
                <p> Those who choose to access the Services do so on their own initiative and
                    at their own risk, and are responsible for complying with all local laws,
                    rules and regulations. We may limit the Services' availability, in whole or
                    in part, to any person, geographic area or jurisdiction We choose, at any
                    time and in Our sole discretion </p>
            </li>
        </ol>
        <ol start="10" type="I">
            <li>
                <h2>Contacting [site].com</h2>
                <p> If You have concerns, comments or want to know what personal information we
                    have about You, please contact Us via Our [feedbacks] for a disclose of the
                    information We have. You have the right to correct or delete information We
                    may have concerning you. Contact the Feedback Page above. Our email address
                    is [admin@neurosig.com] if that better suits You. </p>
                <p> By using this site, [SITE].com, each visitor constitutes consent to use
                    the information for purposes stated in this policy. </p>
                <p><em>This Privacy Policy is effective as of __________. </em> <em> <br/>
                </em></p>
            </li>
        </ol>
    </div>

@endsection