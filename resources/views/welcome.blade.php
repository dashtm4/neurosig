<!--Welcome (Landing) Page September 8, 2018-->
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome!!!</div>

                <div class="card-body">
                    Welcome to NeuroSig's Application Portal.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection