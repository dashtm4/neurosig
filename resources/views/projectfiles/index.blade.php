<!--Project Files Index -->

@extends ('layouts.app')

@section ('content')

    <div class="page-header">
        <div class="container clearfix">
            <h1>Your Project Files</h1>
            {!! link_to_route('excel.index', 
                  'Export to Excel', 
                  ['project_id' => isset($project_id) ? $project_id : ''], 
                  ['class' => 'btn btn-primary pull-right']) 
            !!}
        </div>
        
    </div>

    @if(count($userProjects)>1)
    <div class="container clearfix">
        {!! Form::open(['route' => 'projectfiles.index', 'method' => 'get', 'id' => 'tip-form', 'class' => 'form', 'novalidate' => 'novalidate', 'files' => false]) !!}

    <div class="form-group hidden-print">
        {!!Form::label('project_id','Filter by Project')!!}
         {!! Form::select(
                   'project_id',
                    $userProjects->mapWithKeys(function ($project, $key) { return [$project->id => "(ID: " .$project->id. ") " . $project->name];})->all(),
                    null, 
            ['placeholder' => 'Select Site', 'class' => 'form-control'])!!}
    </div>
    
   
    
    <div class="btn-group float-left">
        {!! Form::submit('Filter', array('class'=>'btn btn-info hidden-print')) !!}
    
        <a href="/admin/files" class="btn btn-success hidden-print">Clear Filter</a>
    </div>

    
    {!! Form::close() !!}
    
    </div>
    @endif

    
        @if ($files->count() > 0)

        <table class="table table-striped table-hover">
            <thead>
              <tr>
                    <th>Key</th>
                    <th>File Type</th>
                    <th>Status</th>

                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'name'])}}" class="{{ $sortBy == 'name' ? 'text-info' : '' }}">File Name</a>
                    </th>                    
                    
                    <th>Site Name</th>
                    <th>Site ID</th>
                    <th>Subject</th>
                    <th>Visit</th>
                    <th>EEG Date</th>

                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'created_at'])}}" class="{{ $sortBy == 'created_at' ? 'text-info' : '' }}">Upload Date</a>
                    </th>
                    
                    <th>Rating</th>
                    <th></th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($files as $file)
               
                <tr style="list-style: none;">
                    
                    <td>
                        {!! link_to_route('files.edit', $file->id, [ $file->id ]) !!}
                    </td>
                    
                    <td>
                        @if (! is_null($file->file_type))
                            {{ $file->file_type }}
                        @else
                            &nbsp;
                        @endif
                    </td>
                    
                    <td>
                        @if (! is_null($file->status))
                            {{ $file->status }}
                        @else
                            &nbsp;
                        @endif
                    </td>
                    
                    <td>
                        {{ $file->fileExtensionStrip() }} 
                    </td>

                    <td>
                        {{ $file->site->name }}
                    </td>
                    
                    <td>
                        {{ $file->site->identifier }}
                    </td>
   
                    <td>
                        @if (! is_null($file->subject_id))
                            {{ $file->subject_id }}
                        @else
                            &nbsp;
                        @endif
                    </td>
                    
                    <td>
                        @if (! is_null($file->subject_visit))
                            {{ $file->subject_visit }}
                        @else
                            &nbsp;
                        @endif
                    </td>
 
                    <td>
                        {{ $file->eeg_date }}
                    </td>
                    
                    <td>
                        {{ $file->created_at }}
                    </td>
                    
                    <td>
                        {{ $file->rating }}
                    </td>
                
                    <td>
                        {!! link_to_route('projectfiles.show', "Show", [$file->id],['class' => 'btn btn-secondary btn-sm hidden-print']) !!}
                    </td>

            @endforeach
        </tbody>
      </table> 

    @else
    
        <p>
            No files found!
        </p>
        
    @endif


    
@endsection