<!--Project Files Show-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>File Information {{ $file->fileId }}</h1>
    </div>
</div>

<div class="container clearfix mb-4">
    <table class="table">
    
        <!-- File Name Less Extension -->        
        <tr>
            <th>Name:</th>
            <td>{{ $file->fileExtensionStrip() }} </td>
        </tr>
        
         <!-- Site Number -->                   
        <tr>
            <th>Site:</th>
            <td>{{ $file->site->identifier }}</td>
        </tr>
        
        <!-- File Type-->
        <tr>
            <th>Type:</th>
            <td>
                @if (! is_null($file->file_type))
                    {{ $file->file_type }}
                    @else
                        &nbsp;
                @endif
            </td>
        </tr>
        
        <!-- Uploaded By -->  
        <tr>
            <th>Uploaded&nbsp;By:</th>
            <td>{{ $file->user->fullName }} ({{ $file->user->email }})</td>
        </tr>
        
        <!-- File Internal Record Number -->    
        <tr>
            <th>Internal&nbsp;Id:</th>
            <td>{{ $file->id }}</td>
        </tr>   

        <tr>
            <th>Quality&nbsp;Rating:</th>
            <td>
                @if (! is_null($file->rating))
                    {{ $file->rating }}
                    @else
                        &nbsp;
                @endif
            </td>        
        </tr>

        <!-- Reviwer Comments -->
        <tr>
            <th>Reviewer&nbsp;Feedback:</th>
            <td>
                @if (! is_null($file->reviewer_notes))
                    {{ $file->reviewer_notes }}
                    @else
                        &nbsp;
                @endif
            </td>        
        </tr>
 
         <!-- Quality Category -->
        <tr>
            <th>Quality&nbsp;Category:</th>
            <td>
                @if (! is_null($file->qualityCategory))
                    {{ $file->qualityCategory }}
                    @else
                        &nbsp;
                @endif
            </td>        
        </tr>
        
         <!-- Status -->
        <tr>
            <th>Internal&nbsp;Status:</th>
            <td>
                @if (! is_null($file->status))
                    {{ $file->status }}
                    @else
                        &nbsp;
                @endif
            </td>        
        </tr>
        
         <!-- Comments -->
        <tr>
            <th>Internal&nbsp;Comment:</th>
            <td>
                @if (! is_null($file->comment))
                    {{ $file->comment }}
                    @else
                        &nbsp;
                @endif
            </td>        
        </tr>    
        


    </table>
    {!! link_to_route('projectfiles.index', "Project Files Home", null, ['class' => 'btn btn-primary float-right'] ) !!}
</div>


@endsection