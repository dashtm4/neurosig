<!--View You Site Files Index -->

@extends ('layouts.app')

@section ('content')

    <div class="page-header">
        <div class="container clearfix">
            <h1>Your Site Files</h1>
        </div>
    </div>

    @if(count($userSites) > 1)
    <div class="container clearfix">
        {!! Form::open(['/files', 'method' => 'get', 'id' => 'tip-form', 'class' => 'form', 'novalidate' => 'novalidate', 'files' => false]) !!}
    

        <!--Site-->
        <div class="form-group hidden-print">
            
            {!!Form::label('site_id','Filter by Site')!!}
            
            {!! Form::select(
                   'site_id',
                    $userSites->mapWithKeys(function ($site, $key) { return [$site->id => "(ID: " .$site->identifier. ") " . $site->name];})->all(),
                    null, 
            ['placeholder' => 'Select Site', 'class' => 'form-control'])!!}
        </div>

        <!--File Type-->  
        <div class="form-group hidden-print">
            {!! Form::label('file_type','File Type') !!}
    
            {!! Form::select('file_type',
                [
                    null=>'Select File Type',
                    'Study' => 'Study',
                    'Practice'   => 'Practice'
                ],
                'Study',
                [
                    'id'    => 'file_type',
                    'class' =>'form-control'
                ]
                
                )!!}
        </div>
        @endif

        <!-- Parents Only -->
        <div class="form-group hidden-print">
            {!! Form::label('parent','Parents ') !!}
            <input type="checkbox" value="0" id="parent" name="parent" {{$hasParentId ? "checked=checked" : ""}}/>
        
        
        </div>

        <div class="btn-group float-left">
            {!! Form::submit('Filter', array('class'=>'btn btn-info hidden-print')) !!}    
            <a href="/files?clear=true" class="btn btn-success hidden-print">Clear Filter</a>
        </div>
        
    
    {!! Form::close() !!}
    </div>
    
    
    
    @if ($files->count() > 0)
    
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                    <th>Key</th>
                    <th>Parent</th>
                    <th>File Type</th>
                    <th>Site Number</th>
                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'name'])}}" class="{{ $sortBy == 'name' ? 'text-info' : '' }}">File Name</a>
                    </th>
                    <th>Rating</th>
                    <th>Review</th>
                    <th>Uploaded By (Site User)</th>
                    <th>Uploaded On</th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($files as $file)
               
                <tr style="list-style: none;">
                    
<!-- Key -->
                    <td>
                        {!! link_to_route('files.edit', $file->id, [ $file->id ]) !!}
                    </td>
<!-- Parent -->      
                    <td>
                        {{ $file->parent_id }}
                    </td>                    

<!-- File Type -->                         
                    <td>
                        @if (! is_null($file->file_type))
                            {{ $file->file_type }}
                        @else
                            &nbsp;
                        @endif
                    </td>
                    
 <!-- Site Identifier -->                   
                    <td>
                        {{ $file->site->identifier }}
                    </td>

 <!-- File Name -->     
 
                    <td>
                        {{ $file->name}}
                    </td>
                    

                    <td>
                        {{ $file->rating }}
                    </td>
 
                     <td>
                        {{ $file->reviewer_notes }}
                    </td>                   
                    
                    <td>
                        {{ $file->user->fullName }}
                    </td>
                    
                    <td>
                        {{ date("m/d/y", strtotime($file->created_at)) }}
                    </td>

            @endforeach
        </tbody>
      </table> 

    @else
    
        <p>
            No files found!
        </p>
        
    @endif

@endsection