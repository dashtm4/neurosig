<!--User Partial-->

<!--First Name-->
<div class="form-group row"> 
    <legend class="col-form-label col-sm-2 pt-0">First Name</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('name',null, ['id' =>'name','class' =>'form-control','placeholder' =>'Enter First Name']) !!}
    </div>
</div>

<!--Last Name-->    
<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Last Name</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('lastName',null, ['id' =>'lastName','class' =>'form-control','placeholder' =>'Enter Last Name']) !!}
    </div>
</div>

<!--Full Name-->  
<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Full Name</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('fullName',null, ['id' =>'fullName','class' =>'form-control','placeholder' =>'Enter Full Name']) !!}
    </div>
</div>

<!--Telephone-->  
 <div class="form-group row border-bottom"> 
    <legend class="col-form-label col-sm-2 pt-0">Telephone</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('telephone',null, ['id' =>'telephone','class' =>'form-control','placeholder' =>'Enter Telephone Number']) !!}
    </div>
</div>

<!--Email-->      
<div class="form-group row border-bottom"> 
    <legend class="col-form-label col-sm-2 pt-0">Email</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('email',null, ['id' =>'email','class' =>'form-control','placeholder' =>'Enter Email']) !!}
    </div>
</div>

<!--Email-->   
<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Password</legend>
    <div class=" form-group col-sm-3 row">
    
        @if (strpos(Route::currentRouteAction(), "edit") !== false)
            {!!Form::password('password',['id' =>'password','class' =>'form-control','placeholder' =>'(unchanged)',]) !!}
        @else
            {!!Form::password('password',['id' =>'password','class' =>'form-control','placeholder' =>'Enter Password',]) !!}
        @endif
    </div>
</div>

<!--Password-->     
<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">User or Tester ID</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('userId',null, ['id' =>'userId','class' =>'form-control','placeholder' =>'Enter Tester or User Id']) !!}
    </div>
</div>

<!--Secure Code-->
<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Secure Code</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('userCode',null, ['id' =>'userCode','class' =>'form-control','placeholder' =>'Enter User Secure Code']) !!}
    </div>
</div>

<!--Notes-->
<div class="form-group row border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">User Notes</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::textarea('notes',
            null, 
            ['id' =>'notes',
            'class' =>'form-control',
            'rows' => 5, 
            'cols' => 50 ,
            'placeholder' =>'Enter Notes',]
        ) !!}
    </div>
</div>    

<!--Primary Role  (Radio)  -->
<div class="form-group row border-bottom"> 
    <legend class="col-form-label col-sm-2 pt-0">Primary Role</legend>
    
    <div class="col-sm-10 row pb-2">
        
        <div class="col-sm-2">
            <label class="form-check-label" for="tester-check">Tester</label>
            {{ Form::radio('role', 'Tester', true, ['class'=>'form-radio-input', 'id'=>'tester-check']) }}
        </div>
        
        <div class="col-sm-2">
            <label class="form-check-label" for="monitor-check">Monitor</label>
            {{ Form::radio('role', 'Monitor', false, ['class'=>'form-radio-input', 'id'=>'monitor-check']) }}
        </div>
        
        <div class="col-sm-2">
            <label class="form-check-label" for="project-check">Project</label>
            {{ Form::radio('role', 'Project', false, ['class'=>'form-radio-input', 'id'=>'project-check']) }}
        </div>
        
        <div class="col-sm-2">
            <label class="form-check-label" for="other-check">Other</label>
            {{ Form::radio('role', 'Information', false, ['class'=>'form-radio-input', 'id'=>'other-check']) }}
        </div>
        
        <div class="col-sm-2">
            <label class="form-check-label" for="expert-check">Expert</label>
            {{ Form::radio('role', 'Expert', false, ['class'=>'form-radio-input', 'id'=>'expert-check']) }}
        </div>
        
        <div class="col-sm-2">
            <label class="form-check-label" for="admin-check">Admin</label>
            {{ Form::radio('role', 'Admin', false, ['class'=>'form-radio-input', 'id'=>'admin-check']) }}
        </div>
        
        <div class="col-sm-2">
            <label class="form-check-label" for="system-check">System</label>
            {{ Form::radio('role', 'System', false, ['class'=>'form-radio-input', 'id'=>'system-check']) }}
        </div>
    </div>
</div>
    

<!--Abilities (Checkbox)    -->
<div class="form-group row border-bottom"> 
    <legend class="col-form-label col-sm-2 pt-0">Abilities</legend>
    <div class="col-sm-10 row pb-2">
        
        <!--is_Site - Users that are Uploading Files-->  
        <div class="col-sm-2">  
            <label class="form-check-label" for="site-check">Site</label>
            {{ Form::checkbox('is_site', true) }}
        </div>
        
        <!--is_monitor - Assigned to Multiple Sites -->
        <div class="col-sm-2">  
        <label class="form-check-label" for="monitor-check">Monitor</label>
            {{ Form::checkbox('is_monitor', true) }}
        </div>
        
        <!--Project -For Client Project Managers -->
        <div class="col-sm-2">   
            <label class="form-check-label" for="project-check">Project</label>
                {{ Form::checkbox('is_project', true) }}        
        </div>     
        
        <!--Other for Site Access, No Project Info -->
        <div class="col-sm-2">   
            <label class="form-check-label" for="other-check">Other</label>
                {{ Form::checkbox('is_other', true) }}        
        </div> 
        
        <!--is_Expert - to enable Downloading of Files-->         
        <div class="col-sm-2">  
            <label class="form-check-label" for="expert-check">Expert</label>
            {{ Form::checkbox('is_expert', true) }}
        </div>
        
        <!--Admin-->
        <div class="col-sm-2">  
            <label class="form-check-label" for="admin-check">Admin</label>
                {{ Form::checkbox('is_admin', true) }}
        </div> 
        
        <!--System - For Developers to Test New Features -->
        <div class="col-sm-2">   
            <label class="form-check-label" for="system-check">System</label>
                {{ Form::checkbox('is_system', true)}}
        </div> 
    </div>
</div>
    
<!--Select Projects-->
<div class="form-group row border-bottom">
<legend class="col-form-label col-sm-2 pt-0">Select Projects</legend>
<div class=" form-group col-sm-6 row">
  {!! Form::select(
  'projects[]', 
  \App\Project::pluck('name', 'id'), 
  null, 
  [
  'id'    => 'project_id',
  'class' => 
    'form-control',
    'multiple' => 'multiple',
    'size' => '10',
    'required'=> 'true',
    'v-on:change'=>'sitesForPojects'
  ]) !!}
</div>
</div>
          
<!--Select Sites-->   
<div class="form-group row border-bottom">
<legend class="col-form-label col-sm-2 pt-0">Select Sites</legend>
<div class=" form-group col-sm-6 row">
  {!! Form::select('sites[]', 
  $sitesSelection, 
  null, 
  ['id' => 'site_id',
    'class' => 'form-control',
    'multiple' => 'multiple', 
    'size' => '20'
  ]) !!}
</div>
</div>

<!--Status-->       
<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">User Status</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('status',null, ['id' =>'status','class' =>'form-control','placeholder' =>'Status']) !!}
    </div>
</div>
    
<!--Send Email-->      
<div class="form-group row border-bottom"> 
    <legend class="col-form-label col-sm-2 pt-0">Email User</legend>
    <div class="col-sm-10 row pb-2">
      
      <label class="form-check-label" for="email-check">Email User if New Password &nbsp</label>
      {{ Form::checkbox('send_email', true) }}  

    </div>    
</div>