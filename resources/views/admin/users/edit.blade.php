<!--Users - Edit-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Edit User </h1>
        <h4><span style="color:blue;">{{ $user->name }}</span> (ID: {{ $user->id }})</h4>
    </div>
</div>

<div class="container clearfix border-bottom pb-4 mb-6"> 
  <div class="row">   
  <div class="col-md-10">
    {!! Form::model($user, [
      'route' => ['users.update', $user->id], 
      'method' => 'put',
      'id' => 'tip-form', 
      'class' => 'form', 
      'novalidate' => 'novalidate']
      ) !!}
    
      @include('admin.users._user_form', ['sitesSelection' => $sitesSelection])

    <div class="form-group">
      {!! Form::submit('Update User', array('class'=>'btn btn-primary')) !!}  
      {!! Form::close() !!}
      {!! link_to_route('users.index', "Users Home", null, ['class' => 'btn btn-default btn-first'] ) !!}
        
      {!! link_to_route('users.create', "Create New User", null, ['class' => 'btn btn-default btn-first'] ) !!}
        </div>    
    </div>
  </div>
</div>

@endsection