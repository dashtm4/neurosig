<!--Users Show-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>User</h1>
        <h4><span style="color:blue;">{{ isset($user->fullName)?$user->fullName : '' }}</span> (ID: {{ $user->id }})</h4>
    </div>
</div>
<div class="container clearfix border-bottom pb-4 mb-6"> 
<table class="table table-striped">
    <thead>
        <tr>
            <th>Description</th>
            <th>Value</th>
        </tr>
    </thead>

    <tbody>
            <tr>
                <td>First Name (Username)</td>
                <td>{{ $user->name }}</td>
            </tr>
            
            <tr>
                <td>Last Name</td>
                <td>{{ $user->lastName }}</td>
            </tr>
            
            <tr>
                <td>Full Name</td>
                <td>{{ $user->fullName }}</td>
            </tr>
            
            <tr>
                <td>User or Tester ID</td>
                <td>{{ $user->userId }}</td>
            </tr>
            
            <tr>
                <td>Email</td>
                <td>{{ $user->email }}</td>
            </tr>
            
            <tr>
                <td>Telephone</td>
                <td>{{$user->telephone }}</td>
            </tr>
            
            <tr>
                <td>Full Name</td>
                <td>{{$user->fullName }}</td>
            </tr>
            
            <tr>
                <td>Tester or UserID</td>
                <td>{{ $user->userID }}</td>
            </tr>
            
            <tr>
                <td>Role</td>
                <td>{{ $user->role }}</td>
            </tr>
 
            <tr>
                <td>Status</td>
                <td>{{ $user->status }}</td>
            </tr>
 
             <tr>
                <td>Privacy</td>
                <td>{{$user->privacy }}</td>
            </tr>
            
            <tr>
                <td>Notes</td>
                <td>{{ $user->notes }}</td>
            </tr>
            
             <tr>
                <td>Expert</td>
                <td>{{ $user->is_expert == 1 ? "Yes" : "No" }}</td>
            </tr>
            
            
            <tr>
                <td>Site User</td>
                <td>{{ $user->is_site == 1 ? "Yes" : "No" }}</td>
            </tr>
        
            <tr>
                <td>Monitor User</td>
                <td>{{ $user->is_monitor == 1 ? "Yes" : "No" }}</td>
            </tr>
            
            <tr>
                <td>Project User</td>
                <td>{{ $user->is_project == 1 ? "Yes" : "No" }}</td>
            </tr>
            
            <tr>
                <td>Other User</td>
                <td>{{ $user->is_other == 1 ? "Yes" : "No" }}</td>
            </tr>
            
            <tr>
                <td>Admin User</td>
                <td>{{ $user->is_admin == 1 ? "Yes" : "No" }}</td>
            </tr>
                            
            <tr>
                <td>System User</td>
                <td>{{ $user->is_system == 1 ? "Yes" : "No" }}</td>
            </tr>
            
            <tr>
                <td> Created On</td>
                <td>{{ date("m/d/y", strtotime($user->created_at)) }}</td>
            </tr>
            
            <tr>
                <td> Deleted On</td>
                
                <td>
                    @if (! is_null($user->deleted_at))
                        {{ date("m/d/y", strtotime($user->deleted_at)) }}
                    @else
                     &nbsp;
                    @endif
                </td>
            </tr>
    </tbody>
</table>

<br/>

<fieldset >
    {!! link_to_route('users.index', "Users Home", null, ['class' => 'btn btn-default'] ) !!}

    {!! link_to_route('users.edit', "Edit User", [$user->id], ['class' => 'btn btn-primary']) !!}
</fieldset> 
<br/>

@if (is_null($user->deleted_at))
<p>
<button type='button' data-toggle="modal" data-target="#deleteConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</button>
</p>
@endif

@include('admin.users._delete_confirm')
  </div>  
@endsection