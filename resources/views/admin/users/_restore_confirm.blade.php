<!-- Users - Restore_Confirm Modal Partial -->

<div class="modal fade" id="restoreConfirm" tabindex="-1" role="dialog" aria-labelledby="restoreConfirmLabel"> <!-- id tells Bootstrap which modal to open -->
  
  <div class="modal-dialog" role="document">
    
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="restoreConfirmTitle">Restore User</h4>
      </div>
      
      <div class="modal-body">
        <p>Please confirm you would like to restore this User.</p> 
      </div>
      

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      
        {!! Form::open([
            'route' => ['users.restore', $user->id],
            'class' => 'form', 
            'method' => 'put',
            'id' => 'tip-form',
            'novalidate' => 'novalidate'
            ]
        ) !!}
         
         <button type="submit" class="btn btn-danger" id="restoreConfirmButton">Restore</button>
         
        {!! Form::close() !!}  
      </div>
    </div>
  </div>
</div>
      
