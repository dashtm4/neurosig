<!--Users Index -->

@extends ('layouts.app')

@section ('content')

    <div class="page-header">
        <div class="container">
            <h1>Users</h1>
        </div>
    </div>
    
    <div class="container clearfix">
         {!! Form::open(['route' => 'users.index', 'method' => 'get', 'id' => 'tip-form', 'class' => 'form', 'novalidate' => 'novalidate', 'files' => false]) !!}

        <div class="form-group hidden-print">
            {!!Form::label('project_id','Filter by Project')!!}
            {!! Form::select('project_id', App\Project::select('id', DB::raw('CONCAT("(ID: ", identifier, ") ", name) AS name'))->pluck('name', 'id'),
            null, 
            ['placeholder' => 'Select Project', 'class' => 'form-control'])!!}
        </div>
        
        <!--Site-->
        <div class="form-group hidden-print">
            {!! Form::label('site_id','Filter by Site')!!}
            {!! Form::select('site_id', App\Site::select('id', DB::raw('CONCAT("(ID: ", identifier, ") ", name) AS name'))->pluck('name', 'id'),
            null, 
            ['placeholder' => 'Select Site', 'class' => 'form-control'])!!}
        </div>
        
        <div class="form-group hidden-print">
        
            <!-- No Rating, Parents Only and Study Data (See Downloads Controller) --> 
            {!!Form::label('site_user','Site')!!}
            {!!Form::checkbox('site_user', 1,  !isset($isSite) || (isset($isSite) && $isSite)) !!}
    
            <!-- Remove Status--> 
            {!!Form::label('monitor_user','Monitor')!!}
            {!!Form::checkbox('monitor_user', 1,  !isset($isMonitor) || (isset($isMonitor) && $isMonitor)) !!}
        
            <!-- Show Deleted -->
            {!!Form::label('project_user','Project')!!}
            {!!Form::checkbox('project_user', 1,  !isset($isProject) || (isset($isProject) && $isProject)) !!}
            
            <!-- Show Deleted -->
            {!!Form::label('all_users','All Users')!!}
            {!!Form::checkbox('all_users', 1,  !isset($allUsers) || (isset($allUsers) && $allUsers)) !!}
        </div>
    

        <!-- Show Deleted -->
        <div class="form-group hidden-print">
            {!!Form::label('show_deleted','Show Deleted')!!}
            {!!Form::checkbox('show_deleted', 1,  $hasShowDeleted) !!}
        </div>

        <!-- Submit the Form -->
        <div class="btn-group pull-left">
            {!! Form::submit('Filter', array('class'=>'btn btn-info hidden-print')) !!}
            <a href="/admin/users" class="btn btn-success hidden-print pull-right">Clear Filter</a>
        </div>
        
       
        <a href="{{ route('users.create') }}" class="btn btn-primary pull-right hidden-print">New User</a>
        
        {!! Form::close() !!}

    </div>
    
    <br/>  <br/>
    @if ($users->count() > 0)
    
    
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Key</th>
                <th>Name</th>
                <th>ID</th>
                <th>Email</th>
                <th>Telephone</th>
                <th>Primary Role</th>
                <th>Site</th>
                <th>Monitor</th>
                <th>Project</th>
                <th>Other</th>
                <th>Notes</th>            
                <th>Date Created</th>
                <th>Date Deleted</th>
                <th>        
                    <button v-on:click="writeText('{{$emails}}')" class="btn btn-primary btn-sm">Copy Emails!</button>
                </th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($users as $user)
               
                    <tr style="list-style: none;
                            {{$user->trashed() ? "background-color:#ffe0b2 !important;":''}}" 
                            class="{{$user->trashed() ? "bg-warning" : ""}}">
                    <td>
                        {{ $user->id }}
                    </td>

                    <td>
                        {{ $user->fullName}}
                    </td>  
                    
                    <td>
                        {{ $user->userId}}
                    </td> 
                    
                    <td>
                        {{ $user->email}}
                    </td>
                    
                    <td>
                        @if (is_null($user->telephone))
                            &nbsp;
                        @elseif (($user->telephone == "NULL"))
                            &nbsp;
                        @else
                            {{ $user->telephone }}
                        @endif
                    </td>
    
                    <td>
                        {{ $user->role}}
                    </td>

                    <td>
                        {{ $user->is_site == 1 ? "Yes" : "No" }}
                    </td>

                    <td>
                        {{ $user->is_monitor == 1 ? "Yes" : "No" }}
                    </td>
                    
                    <td>
                        {{ $user->is_project == 1 ? "Yes" : "No" }}
                    </td>
                    
                    <td>
                        {{ $user->is_other == 1 ? "Yes" : "No" }}
                    </td>
 
                     <td>
                        {{ $user->notes}}
                    </td>
                                       
                    <td>
                        {{ date("m/d/y", strtotime($user->created_at)) }}
                    </td>

                    <td>
                        @if (! is_null($user->deleted_at))
                            {{ date("m/d/y", strtotime($user->deleted_at)) }}
                        @else
                            &nbsp;
                        @endif
                    </td>
 
                    <td>
                        <div class="btn-group pull-right">
                            {!! link_to_route('users.edit',"Edit", [$user->id], ['class' => 'btn btn-primary btn-sm']) !!}
                            {!! link_to_route('users.show',"Show", [$user->id], ['class' => 'btn btn-secondary btn-sm']) !!}
                        </div>
                       
                    </td>  

                    </tr>
            @endforeach
         
        </tbody>
      </table> 

    @else
    
        <p>
            No users found!
        </p>
        
    @endif
    
    


@endsection