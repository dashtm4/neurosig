<!--Sites Index -->

@extends ('layouts.app')

@section ('content')

    <div class="page-header clearfix">
        <div class="container clearfix">
            
            <h1>Sites</h1>
            <div class="btn-group float-right">
                
                @if(Route::currentRouteName() == 'closed-sites')
                    <a href="{{ route('sites.index') }}" class="btn btn-danger hidden-print">Open Sites</a>
                @else
                    <a href="{{ route('closed-sites') }}" class="btn btn-danger hidden-print">Closed Sites</a>
                @endif
                
                <a href="{{ route('sites.create') }}" class="btn btn-primary hidden-print">New Site</a>
            </div>
        </div>
    </div>
    
    @if ($sites->count() > 0)
    
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                    <th>Key</th>
                    <th>Identifier</th>
                    <th>Name</th>
                    <th>Country</th>
                    <th></th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($sites as $site)
               
                <tr style="list-style: none;">
                    <td>
                       {{ $site->id}}
                    </td>
                    
                    <td>
                        {!! link_to_route('sites.edit', $site->identifier, [ $site->id ]) !!}
                    </td>
                    
                    <td>
                        {{ $site->name}}
                    </td>
                    
                    <td>
                        {{ $site->country}}
                    </td>

                    <td>
                        <div class="btn-group float-right">
                            {!! link_to_route('sites.edit', "Edit", [$site->id], ['class' => 'hidden-print btn btn-primary btn-sm hidden-print ']) !!}
                        
                            {!! link_to_route('sites.show', "Show", [$site->id],['class' => 'btn btn-secondary btn-sm hidden-print ']) !!}
                        </div>
                    </td>
            @endforeach

        </tbody>
      </table> 

    @else
    
        <p>
            No sites found!
        </p>
        
    @endif
    
@endsection