<!--Site Partial-->

<div class="form-group row  border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">Project</legend>
    <div class=" form-group col-sm-3 row">
        {!! Form::select('project_id',
            App\Project::pluck('name', 'id'),
            isset($site) ? $site->project_id : '', 
            [
                'id'    => 'category',
                'class' =>'form-control',
                'placeholder' => 'Select Project']
        ) !!}
    </div>
</div>

<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Site Identifier</legend>
    <div class=" form-group col-sm-3 row">
        {!!Form::text('identifier',
            null, 
            ['id' =>'identifier',
            'class' =>'form-control',
            'placeholder' =>'Enter Site Identifier',]
        ) !!}
    </div>
</div>

<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Site Name</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('name',
            null, 
            ['id' =>'name',
            'class' =>'form-control',
            'placeholder' =>'Enter Site Name',]
        ) !!}
    </div>
</div>

<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Address Line 1</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('street1',
            null, 
            ['id' =>'street1',
            'class' =>'form-control',
            'placeholder' =>'Enter Addess Line 1',]
        ) !!}
    </div>
</div>

<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Address Line 2</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('street2',
            null, 
            ['id' =>'street2',
            'class' =>'form-control',
            'placeholder' =>'Enter Addess Line 2',]
        ) !!}
    </div>
</div>

<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">City</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('city',
            null, 
            ['id' =>'city',
            'class' =>'form-control',
            'placeholder' =>'Enter City',]
        ) !!}
    </div>
</div>

<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">State</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('state',
            null, 
            ['id' =>'state',
            'class' =>'form-control',
            'placeholder' =>'Enter State',]
        ) !!}
    </div>
</div>

<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Zip</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('zip',
            null, 
            ['id' =>'zip',
            'class' =>'form-control',
            'placeholder' =>'Enter Zip or Postal Code',]
        ) !!}
    </div>
</div>


<div class="form-group row">
    <legend class="col-form-label col-sm-2 pt-0">Country</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('country',
            null, 
            ['id' =>'country',
            'class' =>'form-control',
            'placeholder' =>'Enter Country',]
        ) !!}
    </div>
</div>

<div class="form-group row border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">Ship Attention</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('shipAttention',
            null, 
            ['id' =>'shipAttention',
            'class' =>'form-control',
            'placeholder' =>'Enter Ship Attention To',]
        ) !!}
    </div>
</div>

<div class="form-group row  border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">Site Telephone</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::text('siteTelephone',
            null, 
            ['id' =>'siteTelephone',
            'class' =>'form-control',
            'placeholder' =>'Enter Site Telephone',]
        ) !!}
    </div>
</div>

 <div class="form-group row border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">Site Status</legend>
    <div class=" form-group col-sm-9 row">    
        {!! Form::select('status',
            [
                'Open'      => 'Open',
                'Closed'    => 'Closed',
                'Other'     => 'Other'
            ],
            isset($site) ? $site->status : null, 
            [
                'id' =>'status',
                'class' =>'form-control',
                'placeholder' =>'Select Site Status'
            ]
            ) !!}
    </div>
</div>
    
<div class="form-group row ">
    <legend class="col-form-label col-sm-2 pt-0">Site Notes</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::textarea('notes',
            null, 
            ['id' =>'notes',
            'class' =>'form-control',
            'rows' => 5, 
            'cols' => 50 ,
            'placeholder' =>'Enter Site Notes (nclude VAT Number when applicable)',]
        ) !!}
    </div>
</div>

 <div class="form-group row border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">Eqiupment Status</legend>
    <div class=" form-group col-sm-9 row">
    
    {!! Form::select('equipmentStatus',
        [
            'At Site'      => 'At Site',
            'Schedule Return'      => 'Schedule Return',
            'Picked Up'    => 'Picked Up',
            'Accepted - No Issues'    => 'Accepted - No Issues',
            'Accepted - Issues'    => 'Accepted - Issues',
            'Other'     => 'Other'
        ],
        isset($site) ? $site->equipmentStatus : null, 
        [
            'id' =>'equipmentStatus',
            'class' =>'form-control',
            'placeholder' =>'Select Equipment Status'
        ]
            ) !!}
    </div>
</div>

<div class="form-group row border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">Shipping Notes</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::textarea('shippingNotes',
            null, 
            ['id' =>'shippingNotes',
            'class' =>'form-control',
            'rows' => 5, 
            'cols' => 50 ,
            'placeholder' =>'Enter Original Shipping Notes',]
        ) !!}
    </div>
</div>

<div class="form-group row border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">Import / Export Notes</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::textarea('importExportNotes',
            null, 
            ['id' =>'importExportNotes',
            'class' =>'form-control',
            'rows' => 5, 
            'cols' => 50 ,
            'placeholder' =>'Enter Import / Export Notes Notes and VAT Number (if applicable)',]
        ) !!}
    </div>
</div>

<div class="form-group row border-bottom">
    <legend class="col-form-label col-sm-2 pt-0">Return Shipping Notes</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::textarea('returnShippingNotes',
            null, 
            ['id' =>'returnShippingNotes',
            'class' =>'form-control',
            'rows' => 5, 
            'cols' => 50 ,
            'placeholder' =>'Enter Return Shipping Notes',]
        ) !!}
    </div>
</div>

<div class="form-group row ">
    <legend class="col-form-label col-sm-2 pt-0">Return Inspection Notes</legend>
    <div class=" form-group col-sm-9 row">
        {!!Form::textarea('returnInspectionNotes',
            null, 
            ['id' =>'equipmentInspectionNotes',
            'class' =>'form-control',
            'rows' => 5, 
            'cols' => 50 ,
            'placeholder' =>'Enter Return Inspection Notes',]
        ) !!}
    </div>
</div>