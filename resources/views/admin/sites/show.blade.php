<!--Sites Show-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Site {{ $site->siteId }}</h1>
    </div>
</div>

<div class="container clearfix mb-4">

<table class="table table-striped">
    <thead>
        <tr>
            <th>Description</th>
            <th>Value</th>
        </tr>
    </thead>

    <tbody>
        
        <tr>
            <td>Project Identifier</td>
            <td>{{ $site->project->identifier }}</td>
        </tr>
        
        <tr>
            <td>Site Identifier</td>
            <td>{{ $site->identifier }}</td>
        </tr>

        <tr>
            <td>Name</td>
            <td>{{ $site->name }}</td>
        </tr>

        <tr>
            <td>Addess Line 1</td>
            <td>{{ $site->street1 }}</td>
        </tr>

        <tr>
            <td>Addess Line 2</td>
            <td>{{ $site->street2 }}</td>
        </tr>
 
         <tr>
            <td>Zip</td>
            <td>{{ $site->zip }}</td>
        </tr>
        
        <tr>
            <td>Country</td>
            <td>{{ $site->country }}</td>
        </tr>

        <tr>
            <td>Ship Attention</td>
            <td>{{ $site->shipAttention }}</td>
        </tr>

        <tr>
            <td>Site Telephone</td>
            <td>{{ $site->siteTelephone }}</td>
        </tr>
        
        <tr>
            <td>Site Status</td>
            <td>{{ $site->status }}</td>
        </tr>
 
         <tr>
            <td>Site Notes</td>
            <td>{{ $site->notes }}</td>
        </tr>
 
         <tr>
            <td>Equipment Status</td>
            <td>{{ $site->equipmentStatus }}</td>
        </tr>
        
         <tr>
            <td>Equipment Status</td>
            <td>{{ $site->equipmentStatus }}</td>
        </tr>
 
         <tr>
            <td>Shipping Notes</td>
            <td>{{ $site->shippingNotes }}</td>
        </tr>
        
         <tr>
            <td>Import/Export Notes</td>
            <td>{{ $site->importExportNotes }}</td>
        </tr>        
        
         <tr>
            <td>Return Shipping Notes</td>
            <td>{{ $site->returnShippingNotes }}</td>
        </tr>

         <tr>
            <td>Return Inspection Notes</td>
            <td>{{ $site->returnInspectionNotes }}</td>
        </tr>
        
        <tr>
            <td> Created On</td>
            <td>{{ date("m/d/y", strtotime($site->created_at)) }}</td>
        </tr>

        <tr>
            <td>Updated On</td>
            <td>
                @if (! is_null($site->updated_at))
                    {{ date("m/d/y", strtotime($site->updated_at)) }}
                @else
                    &nbsp;
                @endif
            </td>
        </tr>
            
        <tr>
            <td>Deleted On</td>
            <td>
                @if (! is_null($site->deleted_at))
                    {{ date("m/d/y", strtotime($site->deleted_at)) }}
                @else
                    &nbsp;
                @endif
            </td>
        </tr>
    </tbody>
</table>
<br/>

    <div class="btn-group float-right">
        {!! link_to_route('sites.index', "Sites Home", null, ['class' => 'btn btn-default'] ) !!}
    
        {!! link_to_route('sites.edit', "Edit Site", [$site->id], ['class' => 'btn btn-primary']) !!}
        
        @if($site->trashed())
            <button type='button' data-toggle="modal" data-target="#restoreConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Restore Site</button>
        @else
            <button type='button' data-toggle="modal" data-target="#deleteConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete Site</button>
        @endif
    </div>

</div>

@include('admin.sites._restore_confirm')

@include('admin.sites._delete_confirm')
    
@endsection