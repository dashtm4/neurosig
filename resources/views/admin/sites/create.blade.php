<!--Create A Site-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Create Site </h1>
        <h4></h4>
    </div>
</div>

<div class="container clearfix border-bottom pb-4 mb-6"> 
  <div class="row">   
    <div class="col-md-10">
    
    {!! Form::open([
      'route' => 'sites.store', 
      'id' => 'tip-form', 
      'class' => 'form', 
      'novalidate' => 'novalidate', 
      'sites' => false]
      ) !!}

    @include('admin.sites._site_form')
    
    <div class="form-group">    
      {!! Form::submit('Create Site', array('class'=>'btn btn-primary hidden-print')) !!}
      {!! Form::close() !!}
      {!! link_to_route('sites.index', "Sites Home", null, ['class' => 'btn btn-default btn-first hidden-print'] ) !!}
        </div>    
      </div>
  </div>
</div>

@endsection