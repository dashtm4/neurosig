<!--Sites - Edit-->

@extends ('layouts.app')

@section ('content')

 <div class="page-header">
    <div class="container">
        <h1>Edit Site {{ $site->id }}</h1>
        <h4></h4>
    </div>
</div>

<div class="container clearfix border-bottom pb-4 mb-6"> 
  <div class="row">   
    <div class="col-md-10">
           
    {!! Form::model($site, [
        'route' => ['sites.update',$site->id],
        'method' => 'put',
        'id' => 'tip-form', 
        'class' => 'form', 
        'novalidate' => 'novalidate']
    ) !!}
    
    {!! Form::hidden('project_id', $site->project_id) !!}
    
    <fieldset >
    
      @include('admin.sites._site_form', ['site'=>$site])
    
    <div class="form-group">
    {!! Form::label("Users (highlighted are currently assigned to site)") !!}<br />
    {!! Form::select('users[]', 
    \App\User::where('is_admin', 0)->pluck('fullname','id')->all(), 
    $site->users->pluck('id')->all(),
    ['class' => 'form-control', 'multiple' => true]) 
    !!}
</div>
    
      {!! link_to_route('sites.index', "Sites Home", null, ['class' => 'btn btn-default btn-first'] ) !!}
        
      {!! Form::submit('Update Site', array('class'=>'btn btn-primary')) !!}  
      
      {!! Form::close() !!}

    </fieldset> 
  </div>
</div>
<br/>
@endsection