<!--Project Partial-->

<fieldset>
    
    <div class="form-group">
        {!!Form::label('identifier','Project Identifier')!!}
        {!!Form::text('identifier',null, ['id' =>'identifIer','class' =>'form-control','placeholder' =>'Enter Id']) !!}
    </div>
    
     <div class="form-group">
        {!!Form::label('client_id','Client Id')!!}
        {!! Form::select('client_id', App\Client::orderBy('identifier', 'asc')->pluck('identifier', 'id'),null, 
        ['placeholder' => 'Select Client', 'class' => 'form-control'])!!}
    </div>
    
    <div class="form-group">
        {!! Form::label('clientName','Client Name')!!}
        {!! Form::text('clientName', null, ['id' =>'clientName','class' =>'form-control','placeholder' =>'Client Name']) !!}
    </div>
    
    <div class="form-group">
        {!!Form::label('name','Project Name')!!}
        {!!Form::text('name', null, ['id' =>'name','class' =>'form-control','placeholder' => 'Enter Name']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('category','Project Category') !!}
        
        {!! Form::select('category',
            [
                'Client'   => 'Client',
                'Internal' => 'Internal',
                'Other'    => 'Other'
            ],
            isset($project) ? $project->category : null, 
            [
                'id'    => 'category',
                'class' =>'form-control',
                'placeholder' => 'Select Project Category'
            ]
            
            )!!}
    </div>
    
        @include('admin.projects._positionsInFilename_form')
    
     <div class="form-group">
        {!! Form::label('status','Project Status') !!}
        
        {!! Form::select('status',
            [
                'Open'      => 'Open',
                'Closed'    => 'Closed',
                'Other'     => 'Other'
            ],
            isset($project) ? $project->status : null, 
            [
                'id' =>'status',
                'class' =>'form-control',
                'placeholder' =>'Select Project Status'
            ]
            ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('notes','Project Notes')!!}
        {!! Form::text('notes', null, ['id' =>'notes','class' =>'form-control','placeholder' =>'Enter Notes']) !!}
    </div>   

</fieldset>
<br clear="all" />