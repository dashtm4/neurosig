<!--Projects Show-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Project {{ $project->projectId }}</h1>

    </div>
</div>

<div class="container clearfix mb-4">

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Description</th>
                <th>Value</th>
            </tr>
        </thead>
    
        <tbody>
            
            <tr>
                <td>Project Identifier</td>
                <td>{{ $project->identifier }}</td>
            </tr>
            
            <tr>
                <td>Project Name</td>
                <td>{{ $project->name }}</td>
            </tr>
            
            <tr>
                <td>Category</td>
                <td>{{ $project->category }}</td>
            </tr>
            
            <tr>
                <td>Status</td>
                <td>{{$project->status}}</td>
            </tr>
            
            <tr>
                <td>Notes</td>
                <td>{{$project->notes }}</td>
            </tr>
            
    
            <tr>
                <td>Client Name</td>
                <td>{{ $project->clientName }}</td>
            </tr>
            
            <tr>
                <td>Client Notes</td>
                <td>{{ $project->clientNotes}}</td>
            </tr>
            
    
            <tr>
                <td>File Name Position - Visit</td> 
                <td>{{ $project->fileNamePosition_VisitID }}</td> 
            </tr> 
            
            <tr>
                <td>File Name Position - Subject</td> 
                <td>{{ $project->fileNamePosition_SubjectID }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Date</td> 
                <td>{{ $project->fileNamePosition_Date }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Tester</td> 
                <td>{{ $project->fileNamePosition_TesterID }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Project</td> 
                <td>{{ $project->fileNamePosition_ProjectID }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Site</td> 
                <td>{{ $project->fileNamePosition_SiteID }}</td> 
            </tr>
            
            <tr>
                <td>File Name Position - Station</td> 
                <td>{{ $project->fileNamePosition_StationID }}</td> 
            </tr>
    
            <tr>
                <td>Client ID</td>
                <td>{{$project->client_id }}</td>
            </tr>
            
            <tr>
                <td> Created On</td>
                <td>{{ date("m/d/y", strtotime($project->created_at)) }}</td>
            </tr>
            
            <tr>
                <td>Updated On</td>
                <td>
                    @if (! is_null($project->updated_at))
                        {{ date("m/d/y", strtotime($project->updated_at)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>Deleted On</td>
                <td>
                    @if (! is_null($project->deleted_at))
                        {{ date("m/d/y", strtotime($project->deleted_at)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    <br/>

    <div class="btn-group float-right">
        {!! link_to_route('projects.index', "Projects Home", null, ['class' => 'btn btn-default'] ) !!}
    
        {!! link_to_route('projects.edit', "Edit Project", [$project->id], ['class' => 'btn btn-primary']) !!}
        
        @if($project->trashed())
        
        <button type='button' data-toggle="modal" data-target="#restoreConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Restore Project</button>
        
        @else
        
        <button type='button' data-toggle="modal" data-target="#deleteConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete Project</button>
        
        @endif
    
 <!--Delete or Restore All Files for the Project-->   
        @if($project->files()->count() > 0)
            <button type='button' data-toggle="modal" data-target="#deleteFilesConfirm" class='btn btn-warn'><i class='glyphicon glyphicon-trash'></i> Delete Project Files</button>
        @endif
        
        @if($project->trashedFiles()->count() > 0)
            <button type='button' data-toggle="modal" data-target="#restoreFilesConfirm" class='btn btn-warn'><i class='glyphicon glyphicon-trash'></i> Restore Project Files</button>
        @endif
    </div>
    
</div>
    

@include('admin.projects._restore_confirm')

@include('admin.projects._restore_files_confirm')

@include('admin.projects._delete_confirm')

@include('admin.projects._delete_files_confirm')
    
@endsection