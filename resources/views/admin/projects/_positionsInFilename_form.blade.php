
    <!--Visit-->
    <div class="form-group">
        {!! Form::label('fileNamePosition_VisitID','Visit ID Position in the File Name') !!}
        {!! Form::select('fileNamePosition_VisitID',
            [
                '0'  => 'First',
                '1'  => 'Second',
                '2'  => 'Third',
                '3'  => 'Fourth', 
                '4'  => 'Fifth',
                '5'  => 'Sixth',
                '6'  => 'Seventh',
                '7'  => 'Eighth'
            ],
            isset($project) ? $project->fileNamePosition_VisitID : '0', 
            [
                'id'    => 'fileNamePosition_VisitID',
                'class' =>'form-control',
                'placeholder' => 'Select Visit ID Position'
            ]
            
            )!!}
    </div>

    <!--Subject-->
    <div class="form-group">
        {!! Form::label('fileNamePosition_SubjectID','Subject ID Position in the File Name') !!}
        {!! Form::select('fileNamePosition_SubjectID',
            [
                '0'  => 'First',
                '1'  => 'Second',
                '2'  => 'Third',
                '3'  => 'Fourth', 
                '4'  => 'Fifth',
                '5'  => 'Sixth',
                '6'  => 'Seventh',
                '7'  => 'Eighth'
            ],
            isset($project) ? $project->fileNamePosition_SubjectID : '1', 
            [
                'id'    => 'fileNamePosition_SubjectID',
                'class' =>'form-control',
                'placeholder' => 'Select Subject ID Position'
            ]
            
            )!!}
    </div>

    <!--Date-->    
    <div class="form-group">
        {!! Form::label('fileNamePosition_Date','Date Position in the File Name') !!}
        {!! Form::select('fileNamePosition_Date',
            [
                '0'  => 'First',
                '1'  => 'Second',
                '2'  => 'Third',
                '3'  => 'Fourth', 
                '4'  => 'Fifth',
                '5'  => 'Sixth',
                '6'  => 'Seventh',
                '7'  => 'Eighth'
            ],
            isset($project) ? $project->fileNamePosition_Date : '2', 
            [
                'id'    => 'fileNamePosition_Date',
                'class' =>'form-control',
                'placeholder' => 'Select Date Position'
            ]
            
            )!!}
    </div>
    
    <!--Tester--> 
    <div class="form-group">
        {!! Form::label('fileNamePosition_TesterID','Tester Position in the File Name') !!}
        {!! Form::select('fileNamePosition_TesterID',
            [
                '0'  => 'First',
                '1'  => 'Second',
                '2'  => 'Third',
                '3'  => 'Fourth', 
                '4'  => 'Fifth',
                '5'  => 'Sixth',
                '6'  => 'Seventh',
                '7'  => 'Eighth'
            ],
            isset($project) ? $project->fileNamePosition_TesterID : '3', 
            [
                'id'    => 'fileNamePosition_TesterID',
                'class' =>'form-control',
                'placeholder' => 'Select Tester Position'
            ]
            
            )!!}
    </div>
    
    <!--Project--> 
    <div class="form-group">
        {!! Form::label('fileNamePosition_ProjectID','Project Position in the File Name') !!}
        {!! Form::select('fileNamePosition_ProjectID',
            [
                '0'  => 'First',
                '1'  => 'Second',
                '2'  => 'Third',
                '3'  => 'Fourth', 
                '4'  => 'Fifth',
                '5'  => 'Sixth',
                '6'  => 'Seventh',
                '7'  => 'Eighth'
            ],
            isset($project) ? $project->fileNamePosition_ProjectID : '4', 
            [
                'id'    => 'fileNamePosition_ProjectID',
                'class' =>'form-control',
                'placeholder' => 'Select Project Position'
            ]
            
            )!!}
    </div>

    <!--Site--> 
    <div class="form-group">
        {!! Form::label('fileNamePosition_SiteID','Site Position in the File Name') !!}
        {!! Form::select('fileNamePosition_SiteID',
            [
                '0'  => 'First',
                '1'  => 'Second',
                '2'  => 'Third',
                '3'  => 'Fourth', 
                '4'  => 'Fifth',
                '5'  => 'Sixth',
                '6'  => 'Seventh',
                '7'  => 'Eighth'
            ],
            isset($project) ? $project->fileNamePosition_SiteID : '5', 
            [
                'id'    => 'fileNamePosition_SiteID',
                'class' =>'form-control',
                'placeholder' => 'Select Site Position'
            ]
            
            )!!}
    </div>
    
    <!--Station--> 
    <div class="form-group">
        {!! Form::label('fileNamePosition_StationID','Station Position in the File Name') !!}
        {!! Form::select('fileNamePosition_StationID',
            [
                '0'  => 'First',
                '1'  => 'Second',
                '2'  => 'Third',
                '3'  => 'Fourth', 
                '4'  => 'Fifth',
                '5'  => 'Sixth',
                '6'  => 'Seventh',
                '7'  => 'Eighth'
            ],
            isset($project) ? $project->fileNamePosition_StationID : '6', 
            [
                'id'    => 'fileNamePosition_StationID',
                'class' =>'form-control',
                'placeholder' => 'Select Station Position'
            ]
            
            )!!}
    </div>