<!--Projects - Edit-->

@extends ('layouts.app')

@section ('content')

 <div class="page-header">
    <div class="container">
    <h1>Edit Project {{ $project->id }}</h1>
        <h4></h4>
    </div>
</div>

<div class="container clearfix border-bottom pb-4 mb-6"> 

    
<div class="row">   

  <div class="col-md-6">
      
    {!! Form::model($project, [
        'route' => ['projects.update',$project->id],
        'method' => 'put',
        'id' => 'tip-form', 
        'class' => 'form', 
        'novalidate' => 'novalidate']
    ) !!}
    
    <fieldset >
    
      @include('admin.projects._project_form', ['project' => $project])
    
      {!! link_to_route('projects.index', "Projects Home", null, ['class' => 'btn btn-default btn-first'] ) !!}
        
      {!! Form::submit('Update Project', array('class'=>'btn btn-primary')) !!}  
      
      {!! Form::close() !!}

    </fieldset> 
  </div>
</div>
</div>
@endsection