<!--Create A Project-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Create Project </h1>
        <h4></h4>
    </div>
</div>

<div class="container clearfix border-bottom pb-4 mb-6"> 
  <div class="row">   
    <div class="col-md-10">
    
    {!! Form::open([
      'route' => 'projects.store', 
      'id' => 'tip-form', 
      'class' => 'form', 
      'novalidate' => 'novalidate', 
      'files' => false]
    ) !!}

    @include('admin.projects._project_form')
    
      <div class="form-group">
        {!! Form::submit('Create Project', array('class'=>'btn btn-primary hidden-print')) !!}
        {!! Form::close() !!}
        {!! link_to_route('projects.index', "Projects Home", null, ['class' => 'btn btn-default btn-first hidden-print'] ) !!}
      </div>    
    </div>
  </div>
</div>

@endsection