<!-- Projects - Delete_Confirm Modal Partial -->

<div class="modal fade" id="restoreFilesConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmLabel"> <!-- id tells Bootstrap which modal to open -->
  
  <div class="modal-dialog" role="document">
    
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="restoreFilesConfirmTitle">Restore files for project Project</h4>
      </div>
      
      <div class="modal-body">
        <p>Please confirm you would like to restore files for this Project?</p> 
      </div>
      

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      
        {!! Form::open([
            'route' => ['files.restore-for-project', $project->id],
            'class' => 'form', 
            'method' => 'put',
            'id' => 'tip-form',
            'novalidate' => 'novalidate'
            ]
        ) !!}
         
         <button type="submit" class="btn btn-danger" id="restorFilesConfirmButton">Restore All Project Files</button>
         
        {!! Form::close() !!}  
      </div>
    </div>
  </div>
</div>
      
