<!--Clients Index -->

@extends ('layouts.app')

@section ('content')

    <div class="page-header">
        <div class="container clearfix">
            
            <h1>Clients</h1>
            <div class="btn-group float-right">
                
                @if(Route::currentRouteName() == 'closed-clients')
                    <a href="{{ route('clients.index') }}" class="btn btn-danger hidden-print">Closed Clients</a>
                @else
                    <a href="{{ route('closed-clients') }}" class="btn btn-danger hidden-print">Closed Clients</a>
                @endif
                
                <a href="{{ route('clients.create') }}" class="btn btn-primary hidden-print">New Client</a>
            </div>
        </div>
    </div>
    
    @if ($clients->count() > 0)
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                    <th>Identifier</th>
                    <th>Name</th>
                    <th></th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($clients as $client)
               
                <tr style="list-style: none;">
                    
                    <td>{!! link_to_route('clients.edit', $client->identifier, [ $client->id ]) !!}</td>
                    
                    <td>
                        {{ $client->name}}
                    </td>
                    
                    <td>
                        <div class="btn-group float-right">
                            {!! link_to_route('clients.edit', "Edit", [$client->id], ['class' => 'hidden-print btn btn-primary btn-sm hidden-print']) !!}
                            {!! link_to_route('clients.show', "Show", [$client->id], ['class' => 'hidden-print btn btn-secondary btn-sm hidden-print ']) !!}
                        </div>   
                    </td>
                @endforeach
                <?php echo $clients->render(); ?>
        </tbody>
      </table> 

    @else
    
        <p>
            No clients found!
        </p>
        
    @endif
 
@endsection