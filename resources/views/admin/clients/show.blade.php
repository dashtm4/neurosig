<!--Clients Show-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Client {{ $client->clientId }}</h1>

    </div>
</div>

<div class="container clearfix mb-4">

<table class="table table-striped">
    <thead>
        <tr>
            <th>Description</th>
            <th>Value</th>
        </tr>
    </thead>

    <tbody>

        <tr>
            <td>Client Identifier</td>
            <td>{{ $client->identifier }}</td>
        </tr>
        
        <tr>
            <td>Name</td>
            <td>{{ $client->name }}</td>
        </tr>
        
        <tr>
            <td> Created On</td>
            <td>{{ date("m/d/y", strtotime($client->created_at)) }}</td>
        </tr>

        <tr>
            <td>Updated On</td>
            <td>
                @if (! is_null($client->updated_at))
                    {{ date("m/d/y", strtotime($client->updated_at)) }}
                @else
                    &nbsp;
                @endif
            </td>
        </tr>
            
        <tr>
            <td>Deleted On</td>
            <td>
                @if (! is_null($client->deleted_at))
                    {{ date("m/d/y", strtotime($client->deleted_at)) }}
                @else
                    &nbsp;
                @endif
            </td>
        </tr>
    </tbody>
</table>
<br/>

    <div class="btn-group float-right">
        {!! link_to_route('clients.index', "Clients Home", null, ['class' => 'btn btn-default'] ) !!}

        {!! link_to_route('clients.edit', "Edit Client", [$client->id], ['class' => 'btn btn-primary']) !!}

        @if($client->trashed())
            <button type='button' data-toggle="modal" data-target="#restoreConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Restore Client</button>
        @else
            <button type='button' data-toggle="modal" data-target="#deleteConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete Client</button>
        @endif
    </div>
    
</div>

@include('admin.clients._restore_confirm')

@include('admin.clients._delete_confirm')
    
@endsection