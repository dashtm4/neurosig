<!--Clients - Edit-->

@extends ('layouts.app')

@section ('content')

    <h1>Edit Client {{ $client->id }}</h1>
    
<div class="row">   

  <div class="col-md-6">
      
    {!! Form::model($client, [
        'route' => ['clients.update',$client->id],
        'method' => 'put',
        'id' => 'tip-form', 
        'class' => 'form', 
        'novalidate' => 'novalidate']
    ) !!}
    
    <fieldset >
    
      @include('admin.clients._client_form')
    
      {!! link_to_route('clients.index', "Clients Home", null, ['class' => 'btn btn-default btn-first'] ) !!}
        
      {!! Form::submit('Update Client', array('class'=>'btn btn-primary')) !!}  
      
      {!! Form::close() !!}

    </fieldset> 
  </div>
</div>
<br/>
@endsection