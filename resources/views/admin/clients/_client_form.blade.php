<!--Client Partial-->

<fieldset>
    
    <div class="form-group">
        {!!Form::label('identifier','Client Identifier')!!}
        {!!Form::text('identifier',null, ['id' =>'identifIer','class' =>'form-control','placeholder' =>'Enter Identifier',]) !!}
    </div>
    
    <div class="form-group">
        {!!Form::label('name','Client Name')!!}
        {!!Form::text('name',null, ['id' =>'name','class' =>'form-control','placeholder' =>'Enter Name',]) !!}
    </div>

</fieldset>
<br clear="all" />