<!--Create A Client-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Create Client </h1>
        <h4></h4>
    </div>
</div>

<div class="container clearfix border-bottom pb-4 mb-6"> 
  <div class="row">   
    <div class="col-md-10">
    
    {!! Form::open([
      'route' => 'clients.store',
      'id' => 'tip-form', 
      'class' => 'form', 
      'novalidate' => 'novalidate', 
      'clients' => false]
      ) !!}

    @include('admin.clients._client_form')
    
      <div class="form-group">
        {!! Form::submit('Create Client', array('class'=>'btn btn-primary hidden-print')) !!}
        {!! Form::close() !!}
        {!! link_to_route('clients.index', "Clients Home", null, ['class' => 'btn btn-default btn-first hidden-print'] ) !!}
      </div>    
      </div>
  </div>
</div>

@endsection