<!--File Rename Partial-->

    @php
         $fileName = $file->name;
    @endphp

    <div class="form-group">
        {!!Form::label('name','File Name')!!}
        {!!Form::text('name',$fileName, ['id' =>'name','class' =>'form-control','placeholder' =>'Enter File Name']) !!}
    </div>
    
<br clear="all" />