<!--Create A File Record-->


@extends ('layouts.app')

@section ('content')

<div class="page-header">
      <h1>File Upload</h1>
</div>
    
    <div class="container clearfix">
      <div class="row">   

      <div class="col-md-6">
        
        {!! Form::open(['route' => 'files.store', 'id' => 'tip-form', 'class' => 'form', 'novalidate' => 'novalidate', 'files' => false]) !!}
    
        @include('admin.files._file_form')
        
        <div class="form-group">
      	    {!! Form::label('document-1', 'Upload File') !!}
      	    {!! Form::file('document-1', null, ['class'=>'form-control btn btn-lg btn-primary']) !!}
      	</div>
    
        {!! Form::submit('Upload', array('class'=>'btn btn-primary btn-lg hidden-print')) !!}
          
        {!! Form::close() !!}
    
    
            {!! link_to_route('files.index', "Files Home", null, ['class' => 'btn btn-default btn-first btn-lg hidden-print'] ) !!}
    
    
    
      </div>
    </div>
</div>
  
@endsection
