<!--Files Index -->

@extends ('layouts.app')

@section ('content')


    <div class="page-header">
        <div class="container">
            <h1>Files</h1>

        </div>
    </div>
    
    <div class="container clearfix">
        {!! Form::open(['route' => 'files.index', 'method' => 'get', 'id' => 'tip-form', 'class' => 'form', 'novalidate' => 'novalidate', 'files' => false]) !!}

    <div class="form-group hidden-print">
        {!!Form::label('project_id','Filter by Project')!!}
        {!! Form::select('project_id', App\Project::select('id', DB::raw('CONCAT("(ID: ", identifier, ") ", name) AS name'))->pluck('name', 'id'),
        null, 
        ['placeholder' => 'Select Project', 'class' => 'form-control'])!!}
    </div>

    <div class="form-group hidden-print">
        {!!Form::label('site_id','Filter by Site')!!}
        {!! Form::select('site_id', App\Site::select('id', DB::raw('CONCAT("(ID: ", identifier, ") ", name) AS name'))->pluck('name', 'id'),
        null, 
        ['placeholder' => 'Select Site', 'class' => 'form-control'])!!}
    </div>
    
        <div class="form-group hidden-print">
        {!! Form::label('file_type','File Type') !!}
        
        {!! Form::select('file_type',
            [
                'Study' => 'Study',
                'Practice'   => 'Practice',
                'Training'   => 'Training',
                'Courtesy'   => 'Courtesy',
                'Other'      => 'Other'
            ],
            null,
            [
                'id'    => 'file_type',
                'class' =>'form-control',
                'placeholder' => 'Select File Type'
            ]
            
            )!!}
    </div>
    
    <!-- Show Deleted -->
    <div class="form-group hidden-print">
        {!!Form::label('show_deleted','Show Deleted')!!}
        {!!Form::checkbox('show_deleted', 1,  $hasShowDeleted) !!}
    </div>
    
    

    <div class="btn-group float-left">
        {!! Form::submit('Filter', array('class'=>'btn btn-info hidden-print')) !!}
    
        <a href="/admin/files" class="btn btn-success hidden-print">Clear Filter</a>
    </div>
    <a href="{{ route('uploads.create') }}" class="btn btn-primary float-right hidden-print">Upload File</a>

    
    
    {!! Form::close() !!}
    </div>
    
    
    
    @if ($files->count() > 0)
    
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                    <th>Key</th>
                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'name'])}}" class="{{ $sortBy == 'name' ? 'text-info' : '' }}">File Name</a>
                    </th>
                    <th>File Type</th>
                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'site_id'])}}" class="{{ $sortBy == 'site_id' ? 'text-info' : '' }}">Site Identifier</a>
                    </th>
                    <th>Site</th>
                    <th>Rating</th>
                    <th>Uploaded By (User)</th>
                    <th>
                        <a href="{{Request::fullUrlWithQuery(['sort_by' => 'created_at'])}}" class="{{ $sortBy == 'created_at' ? 'text-info' : '' }}">Uploaded On</a>
                    </th>
                    <th></th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($files as $file)
               
                <tr style="list-style: none; {{$file->trashed() ? "background-color:#ffe0b2 !important;":''}}" class="{{$file->trashed() ? "bg-warning" : ""}}">
                    
                    <td>{!! link_to_route('files.edit', $file->id, [ $file->id ]) !!}</td>
                    
                    <td>
                        {{ $file->name}}
                    </td>
                    
                    <td>
                        @if (! is_null($file->file_type))
                            {{ $file->file_type }}
                        @else
                             &nbsp;
                        @endif
                    </td>
                    
                    <td>
                        {{ $file->site->identifier }}
                    </td>
                    
                    <td>
                        {{ $file->site->name }}
                    </td>
                    
                    <td>
                        {{ $file->rating }}
                    </td>
                    
                    <td>
                        @if (! is_null($file->user))
                        {{ $file->user->fullName}} ({{ $file->user->email }})
                        @else
                        &nbsp;
                        @endif
                    </td>
                    
                    <td>
                        {{ date("m/d/y", strtotime($file->created_at)) }}
                    </td>

                    <td>
                        <div class="btn-group">
                        {!! link_to_route('files.edit', "Edit", [$file->id], ['class' => 'btn btn-primary btn-sm hidden-print ']) !!}
                    
                        {!! link_to_route('files.show', "Show", [$file->id],['class' => 'btn btn-secondary btn-sm hidden-print ']) !!}
                    
                        {!! link_to_route('files.reviews.edit', "Review", [$file->id, $file->id],['class' => 'btn btn-success btn-sm hidden-print ']) !!}
                        </div>
                    </td>
                    
            @endforeach

        </tbody>
      </table> 


    @else
    
        <p>
            No files found!
        </p>
        
    @endif
    
    <!--{!! link_to_route('uploads.create', "Upload File", null, ['class' => 'hidden-print btn btn-default btn-first'] ) !!}-->
    
@endsection