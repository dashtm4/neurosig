<!--Files - Edit-->

@extends ('layouts.app')

@section ('content')

 <div class="page-header">
    <div class="container">
   <h1>Edit {{ $file->name }} (ID:{{ $file->id }}) </h1>
    </div>
</div>

<div class="container clearfix border-bottom pb-4 mb-6"> 
  <div class="row">  
  
      {!! Form::model($file, [
          'route' => ['files.update',$file->id],
          'method' => 'put',
          'id' => 'tip-form', 
          'class' => 'form', 
          'novalidate' => 'novalidate']
      ) !!}

        <div class="col-md-12">
      
          <fieldset >
             @include('admin.files._rename_form')
          </fieldset> 
          


      
          <fieldset >
                @include('admin.files._file_form')
          </fieldset> 
      
        </div>

      <div class="btn-group float-right">
              {!! link_to_route('files.index', "Files Home", null, ['class' => 'btn btn-default btn-first'] ) !!}
                  
              {!! Form::submit('Update File', array('class'=>'btn btn-primary')) !!}  
          
        </div>
    
      
      {!! Form::close() !!}
  
    
  </div>
</div>
@endsection