<!--Files Show-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>File {{ $file->fileId }}</h1>

    </div>
</div>

<div class="container clearfix mb-4">
    
<table class="table table-striped">
        <thead>
            <tr>
                <th>Description</th>
                <th>Value</th>
            </tr>
        </thead>
        
        <tbody>
            <tr>
                <td>Name</td>
                <td>{{ $file->name }}</td>
            </tr>
            
            <tr>
                <td>Id</td>
                <td>{{ $file->id }}</td>
            </tr>
            
            <tr>
                <td>Site</td>
                <td>{{ $file->site->name }}</td>
            </tr>
    
            <tr>
                <td> Created On</td>
                <td>{{ date("m/d/y", strtotime($file->created_at)) }}</td>
            </tr>
            
            <tr>
                <td>Updated On</td>
                <td>
                    @if (! is_null($file->updated_at))
                        {{ date("m/d/y", strtotime($file->updated_at)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
                
            <tr>
                <td> Deleted On</td>
                <td>
                    @if (! is_null($file->deleted_at))
                        {{ date("m/d/y", strtotime($file->deleted_at)) }}
                    @else
                         &nbsp;
                    @endif
                </td>
            </tr>
            
            <tr>
                <td>Deleted Reason</td>
                <td>{{ $file->deleted_reason }}</td>
            </tr>
    </tbody>
</table>
<br/>

    <div class="btn-group float-right">
        
        {!! link_to_route('files.index', "Files Home", null, ['class' => 'btn btn-default'] ) !!}
    
        {!! link_to_route('files.edit', "Edit File", [$file->id], ['class' => 'btn btn-primary']) !!}
    
         @if (is_null($file->deleted_at))
            <button type='button' data-toggle="modal" data-target="#deleteConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</button>
        @endif
    </div>
    
</div>

@include('admin.files._delete_confirm')
    
@endsection