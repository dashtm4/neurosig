<!--File Partial-->

<fieldset>
    
    <div class="form-group">
        {!!Form::label('site_id','Site Id')!!}
        {!! Form::select('site_id', App\Site::orderBy('identifier', 'asc')->pluck('identifier', 'id'),null, ['placeholder' => 'Select Site', 'class' => 'form-control'])!!}
    </div>
    
</fieldset>
<br clear="all" />