<!--Privacies Index -->

@extends ('layouts.app')

@section ('content')

    <div class="page-header">
        <div class = "container clearfix">
            
            <h1>Privacies</h1>
            <a href="{{ route('privacies.create') }}" class="btn btn-primary pull-right hidden-print">New Privacy</a>

        </div>
        
    </div>
    
    @if ($privacies->count() > 0)
    
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                    <th>Key</th>
                    <th>Privacy Date</th>
                    <th>Notes</th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($privacies as $privacy)
               
                <tr style="list-style: none;">
                    
                                    
                    <td>
                        {{ $privacy->id}}
                    </td>

                    <td>
                        {{ $privacy->privacyDate}}
                    </td>
                    
                                        
                    <td>
                        {{ $privacy->notes}}
                    </td>
                    
                    
                    <td>
                        <div class="btn-group float-right">
                             {!! link_to_route('privacies.edit', "Edit", [$privacy->id], ['class' => 'btn btn-primary btn-sm hidden-print ']) !!}
                    
                            {!! link_to_route('privacies.show', "Show", [$privacy->id],['class' => 'btn btn-secondary btn-sm hidden-print ']) !!}
                        </div>
                       
                    </td>
            @endforeach

        </tbody>
      </table> 

    @else
    
        <p>
            No privacy found!
        </p>
        
    @endif

@endsection