<!--Edit A Privacy Policy Date with Notes-->

@extends ('layouts.app')

@section ('content')

    <h1>Edit Privacy Policy Date {{ $privacy->id }}</h1>
    
<div class="row">   

  <div class="col-md-6">
      
    {!! Form::model($privacy, [
        'route' => ['privacies.update',$privacy->id],
        'method' => 'put',
        'id' => 'tip-form', 
        'class' => 'form', 
        'novalidate' => 'novalidate']
    ) !!}
    
    <fieldset >
    
      @include('admin.privacies._privacy_form')
    
      {!! link_to_route('privacies.index', "Privacies Home", null, ['class' => 'btn btn-default btn-first'] ) !!}
        
      {!! Form::submit('Update Privacy', array('class'=>'btn btn-primary')) !!}  
      
      {!! Form::close() !!}

    </fieldset> 
  </div>
</div>
<br/>
@endsection