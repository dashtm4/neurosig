<!--Privacies Show-->

@extends ('layouts.app')

@section ('content')

<div class="page-header">
    <div class="container">
        <h1>Privacy #{{ $privacy->id }}</h1>

    </div>
</div>

<div class="container clearfix mb-4">

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Description</th>
                <th>Value</th>
            </tr>
        </thead>
    
        <tbody>
            
            <tr>
                <td>Key</td>
                <td>{{ $privacy->id }}</td>
            </tr>
                
            <tr>
                <td>Privacy Policy Date</td>
                <td>
                    @if (! is_null($privacy->privacyDate))
                        {{ date("m/d/y", strtotime($privacy->privacyDate)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
     
            <tr>
                <td>Privacy Policy Notes</td>
                <td>{{ $privacy->privacyNotes}}</td>
            </tr>
                
            <tr>
                <td>Updated On</td>
                <td>
                    @if (! is_null($privacy->updated_at))
                        {{ date("m/d/y", strtotime($privacy->updated_at)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
                
            <tr>
                <td>Deleted On</td>
                <td>
                    @if (! is_null($privacy->deleted_at))
                        {{ date("m/d/y", strtotime($privacy->deleted_at)) }}
                    @else
                        &nbsp;
                    @endif
                </td>
            </tr>
            
        </tbody>
    </table>
    <br/>
    
<fieldset >
        {!! link_to_route('privacies.index', "Privacies Home", null, ['class' => 'btn btn-default'] ) !!}
    
        {!! link_to_route('privacies.edit', "Edit Privacy", [$privacy->id], ['class' => 'btn btn-primary']) !!}
</fieldset> 
<br/>

<p>
<button type='button' data-toggle="modal" data-target="#deleteConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</button>
</p>

@include('admin.privacies._delete_confirm')

@endsection
