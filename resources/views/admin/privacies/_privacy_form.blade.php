<!--Privacy Partial-->

<fieldset>

    <div class="form-group">
        {!!Form::label('privacyDate','Privacy Policy Date')!!}
        {!!Form::date('privacyDate',null, ['id' =>'privacyDate','class' =>'form-control','placeholder' =>'Enter Privacy Policy Date']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('notes','Privacy Policy Notes')!!}
        {!! Form::text('notes', null, ['id' =>'notes','class' =>'form-control','placeholder' =>'Enter Privacy Policy Notes']) !!}
    </div>   

</fieldset>
<br clear="all" />