<!--Snippets - Edit-->

@extends ('layouts.app')

@section ('content')

    <h1>Edit Snippet (ID: {{ $snippet->id }})</h1>
    
<div class="row">   

  <div class="col-md-6">
      
    {!! Form::model($snippet, [
        'route' => ['snippets.update',$snippet->id],
        'method' => 'put',
        'id' => 'tip-form', 
        'class' => 'form', 
        'novalidate' => 'novalidate']
    ) !!}
    
    <fieldset >
    
      @include('admin.snippets._snippet_form')
    
      {!! link_to_route('snippets.index', "Snippets Home", null, ['class' => 'btn btn-default btn-first'] ) !!}
        
      {!! Form::submit('Update Snippet', array('class'=>'btn btn-primary')) !!}  
      
      {!! Form::close() !!}

    </fieldset> 
  </div>
</div>
<br/>
@endsection