<!--Snippets Show -->

@extends ('layouts.app')

@section ('content')

<h1>Snippet {{ $snippet->snippetId }}</h1>

<table class="table table-responsive table-condensed">
    <thead>
        <tr>
            <th>Description</th>
            <th>Value</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>ID</td>
            <td>{{ $snippet->id }}</td>
        </tr>
        
        <tr>
            <td>Name</td>
            <td>{{ $snippet->name }}</td>
        </tr>
        
        <tr>
            <td>Snippet</td>
            <td>{!! nl2br($snippet->snippet) !!}</td>
        </tr>
        
        <tr>
            <td>Category</td>
            <td>{{ $snippet->category }}</td>
        </tr>
        
        <tr>
            <td>Type of Quality Issue</td>
            <td>{{ $snippet->type }}</td>
        </tr>

        <tr>
            <td> Created On</td>
            <td>{{ date("m/d/y", strtotime($snippet->created_at)) }}</td>
        </tr>
    </tbody>
</table>
<br/>

<fieldset>
    {!! link_to_route('snippets.index', "Snippets Home", null, ['class' => 'btn btn-default hidden-print'] ) !!}

    {!! link_to_route('snippets.edit', "Edit Snippet", [$snippet->id], ['class' => 'btn btn-primary hidden-print']) !!}
</fieldset> 
<br/>

<p>
<button type='button' data-toggle="modal" data-target="#deleteConfirm" class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</button>
</p>

@include('admin.snippets._delete_confirm')
    
@endsection


