<!--Create A Snippet-->

@extends ('layouts.app')

@section ('content')

    <div class="page-header">
        <div class="container">
            <h1>Create Snippet</h1>
        </div>
    </div>
  
    <div class="container clearfix border-bottom pb-4 mb-6"> <!--Border at Bottom of Container      -->  

    {!! Form::open(['route' => 'snippets.store', 'id' => 'tip-form', 'class' => 'form', 'novalidate' => 'novalidate', 'snippets' => false]) !!}

    @include('admin.snippets._snippet_form')
    
    {!! Form::submit('Create Snippet', array('class'=>'btn btn-primary hidden-print')) !!}
      
    {!! Form::close() !!}

<br />
<p>
        {!! link_to_route('snippets.index', "Snippets Home", null, ['class' => 'btn btn-default btn-first hidden-print'] ) !!}

</p>

  </div>
</div>
<br/>
@endsection