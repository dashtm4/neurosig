<!--Snippets Index  -->
         
@extends ('layouts.app')

@section ('content')

    <div class="page-header">
        <div class="container clearfix">

            <h1>Snippets</h1>
            <a href="{{ route('snippets.create') }}" class="btn btn-primary pull-right hidden-print">New Snippet</a>

        </div>
    </div>
    @if ($snippets->count() > 0)
    
        <table class="table table-striped table-hover">
            <thead>
              <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Snippet</th>
                    <th>Category</th>
                    <th>Type of Quality Issue</th>
              </tr>
            </thead>
            
            <tbody>
    
                @foreach ($snippets as $snippet)
               
                <tr style="list-style: none;">
    
                    <td>
                        {!! link_to_route('snippets.edit', $snippet->id, [ $snippet->id ]) !!}
                    </td>
                    
                    <td>
                        @if (! is_null($snippet->name))
                            {{ $snippet->name }}
                        @else
                            &nbsp;
                        @endif
                    </td>
                    
                    <td>
                        @if (! is_null($snippet->snippet))
                        
                            {!! nl2br($snippet->snippet) !!}
                            
                        @else
                            &nbsp;
                        @endif
                    </td>
                    
                    <td>
                        @if (! is_null($snippet->category))
                            {{ $snippet->category }}
                        @else
                            &nbsp;
                        @endif
                    </td>
                    
                    <td>
                        @if (! is_null($snippet->type))
                            {{ $snippet->type }}
                        @else
                            &nbsp;
                        @endif
                    </td>

                    <td>
                        <div class="btn-group">
                            {!! link_to_route('snippets.edit', "Edit", [$snippet->id], ['class' => 'hidden-print btn btn-primary btn-sm hidden-print ']) !!}
                        
                            {!! link_to_route('snippets.show', "Show", [$snippet->id],['class' => 'btn btn-default btn-sm hidden-print ']) !!}
                        </div>
                    </td>
            @endforeach
           
        </tbody>
      </table> 

    @else
    
        <p>
            No snippets found!
        </p>
        
    @endif

@endsection