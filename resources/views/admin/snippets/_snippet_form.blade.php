<!--Snippet Partial-->

<fieldset>

<br>
    <div class="form-group row border-bottom "> 
        <legend class="col-form-label col-sm-2 pt-0">Snippet Name</legend>
        <div class="col-sm-10">
            
            {!!Form::text('name',null, ['id' =>'name','class' =>'form-control','placeholder' =>'Enter Snippet Name']) !!}
        
        </div>
        <br><br><br>
    </div>

<!-- Category -->

    <div class="form-group row border-bottom "> 
        <legend class="col-form-label col-sm-2 pt-0">Category</legend>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-2">
                    {{ Form::radio('category', 'None', true, ['class'=>'form-radio-input', 'id'=>'none-checked', 'checked'=>'checked']) }}
                    <label class="form-check-label" for="none-checked">
                    None
                    </label>
                </div>
 
                <div class="col-sm-2">
                    {{ Form::radio('category', 'Quality', false, ['class'=>'form-radio-input', 'id'=>'quality-checked']) }}
                    <label class="form-check-label" for="none-checked">
                    Quality
                    </label>
                </div>
            
                <div class="col-sm-2">
                    {{ Form::radio('category', 'Quality', false, ['class'=>'form-radio-input', 'id'=>'support-checked']) }}
                    <label class="form-check-label" for="none-checked">
                    Support
                    </label>
                </div>
                
                <div class="col-sm-2">
                    {{ Form::radio('category', 'Niceness', false, ['class'=>'form-radio-input', 'id'=>'niceness-checked']) }}
                    <label class="form-check-label" for="niceness-checked">
                    Niceness
                    </label>
                </div>            
            
                 <div class="col-sm-2">
                    {{ Form::radio('category', 'Other', false, ['class'=>'form-radio-input', 'id'=>'other-checked']) }}
                    <label class="form-check-label" for="other-checked">
                    Other
                    </label>
                </div> 
            
            </div>
        </div>
        <br><br>
    </div>  
        
<!-- Enter The Snippet-->         
     <div class="row border-bottom">
        <legend class="col-form-label col-sm-2 pt-0">Snippet</legend>
        <div class=" form-group col-sm-10 row">
        {!! Form::textarea('snippet', null, ['class' => 'form-control', 'rows' => 8, 'cols' => 50 ,'placeholder'=> 'Enter the Snippet']) !!}
        </div>
    </div>    

    

     <!-- Type of Quality Issue -->
    <br>
    <div class="row border-bottom">
        <legend class="col-form-label col-sm-2 pt-0">Type of Quality Issue</legend>
        <div class="form-group col-sm-10 row">
            {!! Form::select('type',
            [
                'None'               => 'None',
                'Other Artifacts'    => 'Other Artifacts',
                'Electrical Noise'   => 'Electrical Noise',
                'Bad Electrode(s)'   => 'Bad Electrode(s)',
                'Eyes'               => 'Eyes',
                'Filename'           => 'Filename',           
                'Gel Bridging'       => 'Gel Bridging',
                'Missing Session(s)' => 'Missing Session(s)',
                'Not Usable'         => 'Not Usable',
                'Response Button'    => 'Response Button',
                'Other'              => 'Other'
            ],
            'None', 
            [
                'id'    => 'is_draft',
                'class' =>'form-control'
            ]
            
            )!!}
        </div>
    </div>

        
</fieldset>
<br clear="all" />