<fieldset>
    
    <!--Rating Section     -->
    <div class="form-group row border-bottom"> 
        <legend class="col-form-label col-sm-2 pt-0">Quality Rating</legend>
        
        <div class="col-sm-10 row pb-2">
            <div class="col-sm-2">
                {{ Form::radio('rating', '0', ['class'=>'form-radio-input', 'id'=>'zero-check']) }}
                <label class="form-check-label" for="zero-check">
                  0 - No Rating
                </label>
            </div>
            <div class="col-sm-2">
                {{ Form::radio('rating', '1', ['class'=>'form-radio-input', 'id'=>'one-check']) }}
                <label class="form-check-label" for="one-check">
                  1 - Not Usable
                </label>
            </div>
            <div class="form-check col-md-2">
                {{ Form::radio('rating', '2', ['class'=>'form-radio-input', 'id'=>'two-check']) }}
                <label class="form-check-label" for="two-check">
                  2 - Some
                </label>
            </div>
            <div class="form-check col-sm-2">
                {{ Form::radio('rating', '3', ['class'=>'form-radio-input', 'id'=>'three-check']) }}
                <label class="form-check-label" for="three-check">
                  3 - All w/Tips
                </label>
            </div>
            
            <div class="form-check col-sm-2">
                {{ Form::radio('rating', '4', ['class'=>'form-radio-input', 'id'=>'four-check']) }}
                <label class="form-radio-label" for="four-check">
                  4 - All Good
                </label>
            </div>
        </div>
    </div>
    
    <!--Snippets-->
    <div class="form-group row border-bottom"> 
    <legend class="col-form-label col-sm-2 mt-0 pt-0">Select Snippets</legend>
        <div class="col-sm-10 row pb-3">
            
            <div id="snippets" class="container">
                
                @foreach ($snippets as $snippet)
                    <span class="badge badge-pill badge-primary snippet" data-snippet-id="{{$snippet['id']}}" data-snippet-text="{!!$snippet['snippet']!!}" v-on:click="snippetTool">{{$snippet['name']}}</span>
                @endforeach
                
                <input name="snippet-ids" type="hidden" id="snippet-ids" />
                
            </div>
        </div>
    </div>

    <!-- Feedback Text Area -->
    <div class="row border-bottom">
        <legend class="col-form-label col-sm-2 pt-0">Feedback</legend>
        <div class=" form-group col-sm-10 row">
            {!! Form::textarea('reviewer_notes', null, ['id' => 'snippitField', 'class' => 'form-control', 'rows' => 14, 'cols' => 50 ,'placeholder'=> 'Enter Feedback Here']) !!}
        </div>
    </div>

    <!-- Email Attachment Section   --> 

    <div class="form-group row border-bottom"> 
        <legend class="col-form-label col-sm-2 pt-3">Email Attachment</legend>
        <div class="col-sm-10 row pb-3 pt-3">
            <div class="col-sm-2">   
                {!! Form::file('email_attachment', null )!!}
            </div>   
        </div>     
    </div>
 
    <!-- Follow Up Section   --> 

    <div class="form-group row border-bottom"> 
        <legend class="col-form-label col-sm-2 pt-0">Follow Up</legend>
        <div class="col-sm-10 row pb-3">

            <div class="col-sm-2">
                {{ Form::radio('status', 'None', true, ['class'=>'form-radio-input', 'id'=>'none-checked', 'checked'=>'checked']) }}
                <label class="form-check-label" for="none-checked">
                None
                </label>
            </div>
            
            <div class="col-sm-2">
                {{ Form::radio('status', 'Follow Up', false, ['class'=>'form-radio-input', 'id'=>'followUp-checked']) }}
                <label class="form-check-label" for="followUp-checked">
                Follow Up
                </label>
            </div>
            
            <div class="col-sm-2">
                {{ Form::radio('status', 'Urgent', false, ['class'=>'form-radio-input', 'id'=>'urgent-checked']) }}
                <label class="form-check-label" for="urgent-checked">
                Urgent
                </label>
            </div>
        
            <div class="col-sm-2">
                {{ Form::radio('status', 'Other', false, ['class'=>'form-radio-input', 'id'=>'other-checked']) }}
                <label class="form-check-label" for="other-checked">
                Other
                </label>
            </div>
        </div>
    </div>
      
     <!-- Notes Section-->
    <div class="row border-bottom">
        <legend class="col-form-label col-sm-2 pt-0">Internal Notes</legend>
        <div class=" form-group col-sm-10 row">
            {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 50 ,'placeholder'=> 'Enter Follow Up or Other Notes Here']) !!}
        </div>
    </div>
    
    <!--Save as Draft  -->
    <div class="form-group row border-bottom">
        <legend class="col-form-label col-sm-2 pt-2">Save as Draft</legend>
       <div class="col-sm-2 pt-2 pb-2">

                <input type="checkbox" name="is_draft" value="true" class="form-check-input" id="is_draft" v-on:click="fixEmailChecks"/>
                <label class="form-check-label" for="is_draft">Draft</label>
            </div>
    </div>
    
     <!-- Email to Uploader-->
    <div class="row">
        <legend class="col-form-label col-sm-2 pt-2">Email the Uploader</legend>
        <div class=" form-group col-sm-10 row pt-3" >   

            <input type="checkbox" name="send_email" id="send_email" value="1" checked="checked"/>
            
            <p>
                &nbsp;
                &nbsp;
                By checking this checkbox, you will send the feedback email to <span style="color:red;"><strong>{{ $file->user->email }}</strong></span>.
            </p
            >
        </div>
    </div>
    
    <div class="row border-bottom">
        <legend class="col-form-label col-sm-2 pt-2">Email All Site Members</legend>
            <div class=" form-group col-sm-10 row pt-3" >

                <input type="checkbox" name="send_email_all_users" id="send_email_all_users" value="1" checked="checked" />
                <p>
                &nbsp;
                &nbsp;
                    By checking this checkbox, you will send the feedback email
                    to <span style="color:red;"><strong>everyone</strong> </span> associated with this file's site.
                </p>
            </div>
    </div>
    
</fieldset>

