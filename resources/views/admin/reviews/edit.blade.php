<!--Reviews - Edit-->

@extends ('layouts.app')

@section ('content')
      
<div class="page-header">
    <div class="container">
        <h1>Review</h1>
        <h4><span style="color:blue;">{{ $file->name }}</span> (ID: {{ $file->id }})</h4>
    </div>
</div>
    

<div class="container clearfix border-bottom pb-4 mb-6"> <!--Border at Bottom of Container      -->
    {!! Form::model($file, [
        'route'=> [
            'files.reviews.update', 
            $file->id, 
            $file->id
            ],
      'method' => 'put',
      'id' => 'review-form', 
      'class' => 'form', 
      'novalidate' => 'novalidate',
      'files' => true
      ]) 
    !!}
<br>
    <!--Parent ID -->
    <div class="form-group row">               
        <legend class="col-form-label col-sm-2 pt-0">Parent ID</legend>
        <div class="col-sm-3">
            <div class="row">
                {!! Form::text('parent_id', $file->parent_id == 0 ? 0 : $file->parent_id, 
                    ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>

     <!--Visit -->    
    <div class="form-group row"> 
        <legend class="col-form-label col-sm-2 pt-0">Visit</legend>
        <div class="col-sm-3">
            <div class="row">
                {!! Form::text('subject_visit', is_null($file->subject_visit) ? $fileComponents[0] : $file->subject_visit, ['class' => 'form-control']) !!}
            </div> 
        </div>
    </div>
        
    <!--Subject -->                
    <div class="form-group row">
        <legend class="col-form-label col-sm-2 pt-0">Subject</legend>
        <div class="col-sm-3">
            <div class="row">
                {!! Form::text('subject_id', is_null($file->subject_id) ? $fileComponents[2] : $file->subject_id, 
                    ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
        
    <!--EEG Date    --> 
    <div class="form-group row">               
         <legend class="col-form-label col-sm-2 pt-0">EEG Date</legend>

        <div class="col-sm-3">
            <div class="row">
                
            {!! Form::date('eeg_date', is_null($file->eeg_date) ? $fileComponents[3] : $file->eeg_date, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>

    <!--Type of File -->
    <div class="form-group row border-bottom pt-0 pb-1"> 
        <legend class="col-form-label col-sm-2 pt-0">File Type</legend>
        <div class="col-sm-10">
            <div class="row">
                
                <div class="col-sm-2">
                    {{ Form::radio('file_type', 'Study', ['class'=>'form-radio-input', 'id'=>'study-check']) }}
                    <label class="form-check-label" for="study-check">Study</label>
                </div>
                
                <div class="col-sm-2">
                    {{ Form::radio('file_type', 'Practice', ['class'=>'form-radio-input', 'id'=>'practice-check']) }}
                    <label class="form-check-label" for="practice-check">Practice</label>
                </div>
                
                <div class="form-check col-md-2">
                    {{ Form::radio('file_type', 'Training', ['class'=>'form-radio-input', 'id'=>'training-check']) }}
                    <label class="form-check-label" for="training-check">Training</label>
                </div>
                
                <div class="form-check col-sm-2">
                    {{ Form::radio('file_type', 'Other', ['class'=>'form-radio-input', 'id'=>'other-check']) }}
                    <label class="form-check-label" for="training-check">Other</label>
                </div>
                
                <div class="form-check col-sm-2">
                    {{ Form::radio('file_type', 'Testing', ['class'=>'form-radio-input', 'id'=>'testing-check']) }}
                    <label class="form-radio-label" for="testing-check">Testing</label>
                </div>
            </div>
        </div>
    </div>  

    @include('admin.reviews._form')
        
    <div class="form-group row pt-3">
        {!! Form::submit('Submit Review', [
            'class'=>'btn btn-primary float-left hidden-print',
            'onClick' => 'document.getElementById("file-parsing-form").submit();'
            ]) !!}  
    </div>
    
    {!! Form::close() !!}
        
</div>

@endsection