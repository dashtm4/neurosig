<style>
    tr{
        padding-bottom: 12px;
    }
    tr.logo{
        padding-bottom: 24px;
    }
    .info tr{
        padding: 8px;
    }
    .info th{
        padding-left: 10px;
    }
</style>
<br/>
<table>

        <tr class="logo">

           <td align="left">
               <img src="https://app.neurosig.com/img/NeuroSig-Star-In-Brain-Logo-320x80.png" width="160" height="40" alt="NeuroSig Portal">
               <br/>
            </td>

        </tr>

        <tr>
            <td>
                <p style="color:black;">Hello {{ $user->fullName }}, </p>

                <p style="color:black;">We have created an account on your behalf to access the NeuroSig Portal.  </p> <br />
            </td>
        </tr>
        <tr>
            <td>
                <table class="info" style="padding-bottom:18px;">
                    <tr>
                        <td width="20px"></td><th align="right" style="padding:8px 10px">URL:</th><td align="left"><a href={{url('/')}}>{{url('/')}}</a></td>
                    </tr>
                    <tr>
                        <td width="20px"></td><th align="right" style="padding:8px 10px">Username:</th><td align="left">{{$user->email}}</td>
                    </tr>
                    <tr>
                        <td width="20px"></td><th align="right" style="padding:8px 10px">Password:</th><td align="left">{{$password}}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <br/>
                <p style="color:black;">Kind regards,</p>
                <p style="color:black;">The NeuroSig Team</p>
            </td>
            
        </tr>
        
        <tr></tr>
        <tr></tr>
        <tr style="height:1px; background-color:#0c4da2;"><td></td></tr>

        <tr>

            <td align="left">
               <p style="color:black;">Questions?</p>
               <p>Please contact <a href="https://support.neurosig.com/contact/">NeuroSig Support</a>.</p>
            </td>

        </tr>

</table>
