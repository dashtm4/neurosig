<style>

    th, td {
        vertical-align:top;
    }
    
    tr{
        padding-bottom: 12px;
    }
    tr.logo{
        padding-bottom: 24px;
    }
    .info tr{
        padding: 8px;
    }
    .info th{
        padding-left: 10px;
    }
</style>
<br/>
<table>

        <tr class="logo">
           <td align="left">
               <img src="https://app.neurosig.com/img/NeuroSig-Star-In-Brain-Logo-320x80.png" width="160" height="40" alt="NeuroSig Portal">
               <br/>
            </td>
        </tr>

        <tr>
            <td>
                <p style="color:black;">NeuroSig has reviewed your uploaded EEG file.  </p> <br />
            </td>
        </tr>
        
        <tr>
            <td>
                <table class="info" style="padding-bottom:18px;">
                    <tr style="padding:8px 0">
                        <td width="20px"></td><th align="right" valign="top" style="padding:8px 10px" >
                        
                        File:<br /></th><td align="left" valign="top" style="padding:8px 10px">{{ $file->name }}</td>
                    </tr>
                    
                    <tr style="padding:8px 0">
                        <td width="20px"></td><th align="right"  valign="top" style="padding:8px 10px">Rating:<br /></th><td align="left" valign="top" style="padding:8px 10px">
                        @if ($file->rating == 1) 
                            1 - Not Usable
                        @elseif ($file->rating == 2)
                            2 - Some Data Useable
                        @elseif ($file->rating == 3)
                            3 - Useable with Tips for Improvement
                        @elseif ($file->rating == 4)
                            4 - Good Data
                        @else
                            &nbsp;
                        @endif
                            
                        </td>
                    </tr>
                    
                    <tr style="padding:8px 0">
                        <td width="20px"></td><th align="right"  valign="top" style="padding:8px 10px">Review Notes:<br /></th><td align="left" valign="top" style="padding:8px 10px">{!! nl2br($file->reviewer_notes) !!}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr></tr>
        <tr></tr>
        <tr style="height:1px; background-color:#0c4da2;"><td></td></tr>

        <tr>

            <td align="left">
               <p style="color:black;">Questions?</p>
               <p>Please contact <a href="https://support.neurosig.com/contact/">NeuroSig Support</a>.</p>
            </td>

        </tr>

</table>
