Alert! A new file has been uploaded.

{{ $file->sites->identifier }} tester {{ $file->user->name }} ({{ $file->user->email }}) uploaded a file named {{ $file->name }}.