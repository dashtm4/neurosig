<!-- Preparing the Subject Article -->

@extends ('layouts.app')

@section ('content')

<header class="masthead prepare">
    <div class="container"><img class="img-fluid" src="img/step0-cap.png" alt="Step-0-Cap-Is-On-With-Red-Lights"
                                height="400" width="300">
        <div class="intro-text">
            <h1>Preparing The Subject</h1>
        </div>
    </div>
</header>
<section class="odd centered">
    <div class="container">
        <h3>Press down on black ground FPz electrode and insert gel only.</h3>
        <p><img class="img-fluid" src="img/step1-press.png" alt="Step-1-Press-Down-On-Black-Electrode"/></p>
        <iframe allowFullScreen frameborder="0" height="564" mozallowfullscreen
                src="https://player2.vimeo.com/video/240781010" webkitAllowFullScreen width="640"></iframe>
    </div>
</section>
<section class="even centered">
    <div class="container">
        <h3>Insert gel in forehead FP1 electrode.</h3>
        <p><img class="img-fluid" src="img/step2-fill.png" alt="Step-2-Fill-Black-Electrode-With-Gel"/></p>
        <iframe allowFullScreen frameborder="0" height="564" mozallowfullscreen
                src="https://player2.vimeo.com/video/240781001" webkitAllowFullScreen width="640"></iframe>
    </div>
</section>
<section class="odd centered">
    <div class="container">
        <h3>Return to black electrode and rock the syringe as shown in video and refill electrode with gel.</h3>
        <p><img class="img-fluid" src="img/step3-press.png" alt="Step-3-Press-Down-On-FP1"/></p>
        <iframe allowFullScreen frameborder="0" height="564" mozallowfullscreen
                src="https://player2.vimeo.com/video/240781018" webkitAllowFullScreen width="640"></iframe>
    </div>
</section>
<section class="even centered">
    <div class="container">
        <h3>Return to forehead electrode FP1 and rock syringe until light turns green.</h3>
        <p><img class="img-fluid" src="img/step4-addgel.png" alt="Step-4-Add-Gel-To-FP1"/></p>
        <iframe allowFullScreen frameborder="0" height="564" mozallowfullscreen
                src="https://player2.vimeo.com/video/240781036" webkitAllowFullScreen width="640"></iframe>
    </div>
</section>
<section class="odd centered">
    <div class="container">
        <h3>Press down on FP1 electrode and fill with gel.</h3>
        <p><img class="img-fluid" src="img/step5-prepblack.png"
                alt="Step-5-Prep-Black-By-Rocking-Syring-And-Refill-With-Gel"/></p>
        <iframe allowFullScreen frameborder="0" height="564" mozallowfullscreen
                src="https://player2.vimeo.com/video/240781044" webkitAllowFullScreen width="640"></iframe>
    </div>
</section>
<section class="even centered">
    <div class="container">
        <h3>Prep all remaining electrodes.</h3>
        <iframe allowFullScreen frameborder="0" height="564" mozallowfullscreen
                src="https://player2.vimeo.com/video/240781058" webkitAllowFullScreen width="640"></iframe>
    </div>
</section>
<!-- Footer -->
<!--<footer class="text-center">-->
<!--    <div class="footer-above">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <h2>Contact NeuroSig Support</h2>-->
<!--                <p>Business Hours: 9:00 am to 6:00 pm PST</p>-->
<!--                <p>&nbsp;</p>-->
<!--                <div class="footer-col col-md-4">-->
<!--                    <h4>Email</h4>-->
<!--                    <p><a href="mailto:support@neurosig.com">support@neurosig.com</a></p>-->
<!--                </div>-->
<!--                <div class="footer-col col-md-4">-->
<!--                    <h4>Telephone Support - Business Hours</h4>-->
<!--                    <p>International: 001-619-737-2518 ext. 0<br/>-->
<!--                        United States: 1-619-737-2518 ext. 0</p>-->
<!--                </div>-->
<!--                <div class="footer-col col-md-4">-->
<!--                    <h4>Telephone Support – Emergency</h4>-->
<!--                    <p>International: 001-619-737-2518 ext. 1<br/>-->
<!--                        United States: 1-619-737-2518 ext. 1</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    
@endsection
