<!--Support Page -->
@extends ('layouts.app')

@section ('content')

    <div class="page-header">
    <span><h1>Support</h1></span>
    </div>

        <h2>Email - Preferred Way to Contact Us</h2>
            
        <span>
            <a href="/contact" class="btn btn-default btn-first hidden-print">Contact Us</a>
        </span>

        <h2>Telephone</h2>

            <h5>International: 001-619-737-2518 <span>Extension 0</span></h5>
            <h5>United States: 1-619-737-2518 <span>Extension 0</span></h5>
            <br />
            
            <h5>Business Hours: 9:00 AM to 6:00 PM Pacific Time</h5> 
        <h2>Emergency</h2>
            <h5>An emergency is when you are about to perform a test and cannot or if something occurs during a test that does not seem normal.</h5> 
            <br />
            <h5>International: 001-619-737-2518 <span>Extension 1</span></h5>
            <h5>United States: 1-619-737-2518 <span>Extension 1</span></h5>
@endsection