import vueResource from 'vue-resource';
import Vue from 'vue';
import axios from 'axios';
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'


Vue.prototype.$http = axios

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');




window.Vue = require('vue');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

Vue.component(
    'uploader',
    require('./components/Uploader.vue')
)


const app = new Vue({
    el: '#app',
    self: this,
    // define methods under the `methods` object
    methods: {
        snippetTool: function (event) {
            var ids = document.getElementById('snippet-ids').value;
            if(ids != ''){
                document.getElementById('snippet-ids').value += ',' + event.currentTarget.getAttribute('data-snippet-id');
            }
            else{
                document.getElementById('snippet-ids').value += event.currentTarget.getAttribute('data-snippet-id');

            }
            document.getElementById('snippitField').value += event.currentTarget.getAttribute('data-snippet-text') + '\n\n';
        },
        fixEmailChecks: function(event) {
            let draftChecked = (event.currentTarget.checked);
            if(draftChecked){
                document.getElementById("send_email").checked = false;
                document.getElementById("send_email_all_users").checked = false;

            }
            else{
                document.getElementById("send_email").checked = true;
                document.getElementById("send_email_all_users").checked = true;
            }
            
        },
        sitesForPojects: function(event) {
            
            const selected = document.querySelectorAll('#project_id option:checked');
            const siteIds = Array.from(selected).map((el) => el.value);
            document.getElementById('site_id').innerHTML = '';

            siteIds.forEach(siteId => {
                console.log(`Selected Site ID: ${siteId}`);
                let optionsHTML = '';

                Vue.prototype.$http.get(`/admin/projects/${siteId}/getsites`).then(data => {
                    data.data.forEach( option => {
                        optionsHTML += `<option value="${option.id}">${option.name}</option>`;
    
                    });
                    document.getElementById('site_id').innerHTML += optionsHTML;
    
                });
            })
        },
        updatePrivacy: function(event) {
            let userId = event.currentTarget.getAttribute('data-user-id');
            Vue.prototype.$http.put(`/users/${userId}/privacy`).then(data => {
                document.getElementById('privacy-update').remove();
            });
        },
        // A minimal polyfill for `navigator.clipboard.writeText()` that works most of the time in most modern browsers.
        // Note that on Edge this may call `resolve()` even if copying failed.
        // See https://github.com/lgarron/clipboard-polyfill for a more robust solution.
        // License: public domain
        writeText: function(str) {
          return new Promise(function(resolve, reject) {
            var success = false;
            function listener(e) {
              e.clipboardData.setData("text/plain", str);
              e.preventDefault();
              success = true;
            }
            document.addEventListener("copy", listener);
            document.execCommand("copy");
            document.removeEventListener("copy", listener);
            success ? resolve(): reject();
          });
        }
    }
});
