<?php

namespace App\Mail;

use App\File;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReviewCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $file;
    public $upload;
    public $filename;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(File $file, $upload, $filename)
    {
        $this->file = $file;
        $this->upload = $upload;
        $this->filename = $filename;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->upload != '') {
            return $this->view('emails.review')->attach($this->upload, ['as' => $this->filename]);
        } else {
            return $this->view('emails.review');
        }
    }
}
