<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SoftDeletes;

    protected $fillable = [

    ];

    // Relationships

    public function site()
    {
        return $this->belongsTo(\App\Site::class)->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class)->withTrashed();
    }

    public function project()
    {
        return $this->belongsTo(\App\Project::class)->withTrashed();
    }

    public function snippets()
    {
        return $this->belongsToMany(\App\Snippet::class)->withTimestamps();
    }

    // Scopes

    public function scopeByProject($query, $projectID)
    {
        return $query->where('project_id', $projectID);
    }

    public function scopeParentIsZero($query)
    {
        return $query->where('parent_id', 0);
    }

    public function scopeNoRating($query)
    {
        return $query->where('rating', 0);
    }

    // Scopes

    /**
     * Utility to get the file name with out the extension.
     *
     * @returns a {@code String} containing just the filename
     */
    public function fileExtensionStrip()
    {
        return preg_replace('/.[^.]*$/', '', $this->name);
    }
}
