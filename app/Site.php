<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model
{
    use SoftDeletes;

    protected $fillable = [

    ];

    // Relationships

    public function project()
    {
        return $this->belongsTo(\App\Project::class);
    }

    public function files()
    {
        return $this->hasMany(\App\File::class);
    }

    public function users()
    {
        return $this->belongsToMany(\App\User::class)->withTimestamps();
    }
}
