<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteUser extends Model
{
    // Do this to override the model table name. Otherwise, Laravel will think
    // this table's name is sites_users.
    protected $table = 'site_user';

    // Relationships

    public function site()
    {
        return $this->belongsTo(\App\Site::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
