<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileSnippet extends Model
{
    // Do this to override the model table name. Otherwise, Laravel will think
    // this table's name is sites_users.
    protected $table = 'file_snippet';

    // Relationships

    public function file()
    {
        return $this->belongsTo(\App\File::class);
    }

    public function snippet()
    {
        return $this->belongsTo(\App\Snippet::class);
    }
}
