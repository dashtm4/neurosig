<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Snippet extends Model
{
    use SoftDeletes;

    protected $fillable = [

    ];

    public function files()
    {
        return $this->belongsToMany(\App\File::class)->withTimestamps();
    }
}
