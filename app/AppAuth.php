<?php

namespace App;

use App\Privacy;
use Illuminate\Support\Facades\Auth;

class AppAuth extends Auth
{
    public static function needPrivacy()
    {
        if (Auth::user()->privacy == null) {
            return true;
        }
        $rows = (Privacy::orderBy('privacyDate', 'desc')->first()->where('privacyDate', '>', Auth::user()->privacy)->first());

        return $rows !== null && $rows->count() > 0;
    }
}
