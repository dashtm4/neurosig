<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $fillable = [

    ];

    // Relationships

    public function client()
    {
        return $this->belongsTo(\App\Client::class);
    }

    public function sites()
    {
        return $this->hasMany(\App\Site::class);
    }

    public function files()
    {
        return $this->hasMany(\App\File::class);
    }

    public function trashedFiles()
    {
        return $this->hasMany(\App\File::class)->onlyTrashed();
    }

    public function users()
    {
        return $this->belongsToMany(\App\User::class)->withTimestamps();
    }
}
