<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUser extends Model
{
    // Do this to override the model table name. Otherwise, Laravel will think
    // this table's name is sites_users.
    protected $table = 'project_user';

    // Relationships

    public function project()
    {
        return $this->belongsTo(\App\Project::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
