<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectRequest;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProjectsController extends Controller
{
    //Index Display
    public function index(Request $request)
    {
        $hasSortBy = $request->has('sort_by');
        $sortBy = $hasSortBy ? $request->get('sort_by') : 'name';

        $query = Project::orderBy($sortBy, $sortBy == 'created_at' ? 'name' : 'asc')
            ->orderBy('created_at', 'name');

        $projects = $query->get();

        return view('admin.projects.index')->with(['projects' => $projects, 'sortBy' => $sortBy]);
    }

    //Closed Display Button
    public function closed()
    {
        $sortBy = 'name';

        $query = Project::onlyTrashed()->orderBy($sortBy, $sortBy == 'created_at' ? 'name' : 'asc')
            ->orderBy('created_at', 'name');

        $projects = $query->get();

        return view('admin.projects.index')->with(['projects' => $projects, 'sortBy' => $sortBy]);
    }

    //Create and Store
    public function create()
    {
        return view('admin.projects.create');
    }

    public function store(Request $request)
    {
        $project = new Project;

        $project->identifier = $request->get('identifier');
        $project->name = $request->get('name');
        $project->category = $request->get('category');
        $project->status = $request->get('status');
        $project->notes = $request->get('notes');
        $project->clientName = $request->get('clientName');
        $project->clientNotes = $request->get('clientNotes');

        //File Name Postions
        $project->fileNamePosition_VisitID = $request->get('fileNamePosition_VisitID');
        $project->fileNamePosition_SubjectID = $request->get('fileNamePosition_SubjectID');
        $project->fileNamePosition_Date = $request->get('fileNamePosition_Date');
        $project->fileNamePosition_TesterID = $request->get('fileNamePosition_TesterID');
        $project->fileNamePosition_ProjectID = $request->get('fileNamePosition_ProjectID');
        $project->fileNamePosition_SiteID = $request->get('fileNamePosition_SiteID');
        $project->fileNamePosition_StationID = $request->get('fileNamePosition_StationID');

        $project->client_id = $request->get('client_id');

        $project->save();

        return Redirect::route('projects.index')->with('message', 'Project saved!');
    }

    //Show
    public function show($id)
    {
        $project = Project::withTrashed()->findOrFail($id);

        return view('admin.projects.show')->with('project', $project);
    }

    //Edit
    public function edit($id)
    {
        $project = Project::withTrashed()->findOrFail($id);

        return view('admin.projects.edit')->with('project', $project);
    }

    //Update
    public function update(ProjectRequest $request, $id)
    {
        $project = Project::findOrFail($id);

        $project->identifier = $request->get('identifier');
        $project->name = $request->get('name');
        $project->category = $request->get('category');
        $project->status = $request->get('status');
        $project->notes = $request->get('notes');

        $project->clientName = $request->get('clientName');
        $project->clientNotes = $request->get('clientNotes');

        //File Name Postions
        $project->fileNamePosition_VisitID = $request->get('fileNamePosition_VisitID');
        $project->fileNamePosition_SubjectID = $request->get('fileNamePosition_SubjectID');
        $project->fileNamePosition_Date = $request->get('fileNamePosition_Date');
        $project->fileNamePosition_TesterID = $request->get('fileNamePosition_TesterID');
        $project->fileNamePosition_ProjectID = $request->get('fileNamePosition_ProjectID');
        $project->fileNamePosition_SiteID = $request->get('fileNamePosition_SiteID');
        $project->fileNamePosition_StationID = $request->get('fileNamePosition_StationID');

        $project->client_id = $request->get('client_id');

        $project->save();

        return Redirect::route('projects.edit', [$project->id])->with('message', 'Your Project has been updated!');
    }

    //Get Sites
    public function getSites($id)
    {
        $project = Project::findOrFail($id);

        return response()->json($project->sites);
    }

    //Restore
    public function restore($projectId)
    {
        Project::withTrashed()->find($projectId)->restore();

        return back()->with('message', 'Project Restored!');
    }

    //Delete
    public function destroy($id)
    {
        $project = Project::findOrFail($id);

        $project->delete();

        return Redirect::route('projects.index')->with('message', 'Project deleted!');
    }
}
