<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PrivacyRequest;
use App\Privacy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PrivaciesController extends Controller
{
    //Index Display
    public function index(Request $request)
    {
        $hasSortBy = $request->has('sort_by');
        $sortBy = $hasSortBy ? $request->get('sort_by') : 'privacyDate';

        $query = Privacy::orderBy($sortBy, $sortBy == 'created_at' ? 'privacyDate' : 'asc')
        ->orderBy('created_at', 'privacyDate');

        $privacies = $query->get();

        return view('admin.privacies.index')->with(['privacies' => $privacies, 'sortBy' => $sortBy]);
    }

    //Create and Store
    public function create()
    {
        return view('admin.privacies.create');
    }

    public function store(Request $request)
    {
        $privacy = new Privacy;

        $privacy->privacyDate = $request->get('privacyDate');
        $privacy->notes = $request->get('notes');

        $privacy->save();

        return Redirect::route('privacies.index')->with('message', 'Privacy saved!');
    }

    //Show
    public function show($id)
    {
        $privacy = Privacy::findOrFail($id);

        return view('admin.privacies.show')->with('privacy', $privacy);
    }

    //Edit
    public function edit($id)
    {
        $privacy = Privacy::findOrFail($id);

        return view('admin.privacies.edit')->with('privacy', $privacy);
    }

    //Update
    public function update(PrivacyRequest $request, $id)
    {
        $privacy = Privacy::findOrFail($id);

        $privacy->privacyDate = $request->get('privacyDate');
        $privacy->notes = $request->get('notes');

        $privacy->save();

        return Redirect::route('privacies.edit', [$privacy->id])->with('message', 'Your Privacy has been updated!');
    }

    //Delete
    public function destroy($id)
    {
        if (Privacy::all()->count() <= 1) {
            return Redirect::route('privacies.index')->with('message', 'Unable to delete without first adding a new Privacy Statement!');
        }
        $privacy = Privacy::findOrFail($id);

        $privacy->delete();

        return Redirect::route('privacies.index')->with('message', 'Privacy deleted!');
    }
}
