<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SiteRequest;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SitesController extends Controller
{
    //Index Display
    public function index()
    {
        $sites = Site::orderBy('identifier', 'name')->paginate(15);

        return view('admin.sites.index')->with('sites', $sites);
    }

    //Closed Display
    public function closed()
    {
        $sortBy = 'name';

        $query = Site::onlyTrashed()->orderBy($sortBy, $sortBy == 'created_at' ? 'name' : 'asc')
            ->orderBy('created_at', 'name');

        $sites = $query->get();

        return view('admin.sites.index')->with(['sites' => $sites, 'sortBy' => $sortBy]);
    }

    //Create and Store
    public function create()
    {
        return view('admin.sites.create');
    }

    public function store(SiteRequest $request)
    {
        $site = new Site;

        $site->project_id = $request->get('project_id');
        $site->identifier = $request->get('identifier');

        $site->name = $request->get('name');
        $site->street1 = $request->get('street1');
        $site->street2 = $request->get('street2');
        $site->city = $request->get('city');
        $site->state = $request->get('state');
        $site->country = $request->get('country');
        $site->zip = $request->get('street2');
        $site->shipAttention = $request->get('shipAttention');

        $site->siteTelephone = $request->get('siteTelephone');
        $site->status = $request->get('status');
        $site->notes = $request->get('notes');

        $site->equipmentStatus = $request->get('equipmentStatus');
        $site->shippingNotes = $request->get('shippingNotes');
        $site->importExportNotes = $request->get('importExportNotes');
        $site->returnShippingNotes = $request->get('returnShippingNotes');
        $site->returnInspectionNotes = $request->get('returnInspectionNotes');

        $site->save();

        return Redirect::route('sites.index')->with('message', 'Site saved!');
    }

    //Show
    public function show($id)
    {
        $site = Site::withTrashed()->findOrFail($id);

        return view('admin.sites.show')->with('site', $site);
    }

    //Edit
    public function edit($id)
    {
        $site = Site::withTrashed()->findOrFail($id);

        return view('admin.sites.edit')->with('site', $site);
    }

    //Update
    public function update(SiteRequest $request, $id)
    {
        $site = Site::findOrFail($id);

        $site->project_id = $request->get('project_id');
        $site->identifier = $request->get('identifier');

        $site->name = $request->get('name');
        $site->street1 = $request->get('street1');
        $site->street2 = $request->get('street2');
        $site->city = $request->get('city');
        $site->state = $request->get('state');
        $site->country = $request->get('country');
        $site->zip = $request->get('street2');
        $site->shipAttention = $request->get('shipAttention');

        $site->siteTelephone = $request->get('siteTelephone');
        $site->status = $request->get('status');
        $site->notes = $request->get('notes');

        $site->equipmentStatus = $request->get('equipmentStatus');
        $site->shippingNotes = $request->get('shippingNotes');
        $site->importExportNotes = $request->get('importExportNotes');
        $site->returnShippingNotes = $request->get('returnShippingNotes');
        $site->returnInspectionNotes = $request->get('returnInspectionNotes');

        $site->save();

        $site->users()->sync($request->get('users'));

        return Redirect::route('sites.edit', [$site->id])->with('message', 'Your Site has been updated!');
    }

    //Restore
    public function restore($siteId)
    {
        Site::withTrashed()->find($siteId)->restore();

        return back()->with('message', 'Site Restored!');
    }

    //Delete
    public function destroy($id)
    {
        $site = Site::findOrFail($id);

        $site->delete();

        return Redirect::route('sites.index')->with('message', 'Site deleted!');
    }
}
