<?php

namespace App\Http\Controllers\Admin;

use App\File;
use App\Http\Controllers\Controller;
use App\Mail\ReviewCreated;
use App\Project;
use App\Snippet;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class ReviewsController extends Controller
{
    public function edit(File $file)
    {
        $errors = [];
        $project = Project::find($file->project_id);

        if ($project == null) {
            $errors[] = 'Unable to find associated project for file';
        }
        $fileComponents = explode('_', $file->name);
        $snippets = Snippet::orderBy('name', 'asc')
               ->get();

        if (count($fileComponents) <= 3) {
            $errors[] = 'Unable to break up file name. Is the parts seperated by the correct delimeter? Do you have all parts;';

            $fileParts[0] = '';
            $fileParts[1] = '';
            $fileParts[2] = '';
            $fileParts[3] = '';
        } else {

            //Get the positions for the Visit, Subject and EEG from the Projects Table for the Project Id associated with this Record
            //Figure out how to pass them to the view
            try {
                if ($project->fileNamePosition_VisitID === null or
                   $project->fileNamePosition_SubjectID === null or
                   $project->fileNamePosition_Date === null) {
                    $fileParts[0] = $fileParts[2] = $fileParts[3] = '';
                    throw new Exception('The field is undefined.');
                }

                // Subject visit
                $fileParts[0] = $project != null ? $fileComponents[$project->fileNamePosition_VisitID] : '';

                // Subject ID
                $fileParts[2] = $project != null ? $fileComponents[$project->fileNamePosition_SubjectID] : '';

                // EEG Date
                $fileParts[3] = $project != null ? $fileComponents[$project->fileNamePosition_Date] : '';

                $date = new Carbon($fileParts[3]);
                $fileParts[3] = $date->toDateString();
            } catch (\Exception $e) {
                $errors[] = ('Error in filename. Could not parse date into desired format.');
            }
        }

        $fileComponents = $fileParts;

        return view('admin.reviews.edit')
          ->with('file', $file)
          ->with('snippets', $snippets)
          ->with('fileComponents', $fileComponents)

          ->withErrors($errors);
    }

    public function update(Request $request, $fileID, $reviewID)
    {
        $upload = '';
        $filename = '';

        $file = File::findOrFail($fileID);

        if ($request->has('snippet-ids') && ! empty($request->get('snippet-ids'))) {
            $snippetIds = explode(',', $request->get('snippet-ids'));
            if (count($snippetIds) > 0) {
                $file->snippets()->sync($snippetIds);
            }
        }

        if ($request->has('parent_id')) {
            $file->parent_id = $request->get('parent_id');
        }

        if ($request->has('subject_visit')) {
            $file->subject_visit = $request->get('subject_visit');
        }

        if ($request->has('subject_id')) {
            $file->subject_id = $request->get('subject_id');
        }

        if ($request->has('eeg_date')) {
            $file->eeg_date = $request->get('eeg_date');
        }

        if ($request->has('file_type')) {
            $file->file_type = $request->get('file_type');
        }

        if ($request->has('rating')) {
            $file->rating = $request->get('rating');
        }

        if ($request->has('reviewer_notes')) {
            $file->reviewer_notes = $request->get('reviewer_notes');
        }

        if ($request->has('is_draft')) {
            $file->is_draft = $request->get('is_draft');
        }

        if ($request->has('status')) {
            $file->status = $request->get('status');
        }

        if ($request->has('comment')) {
            $file->comment = $request->get('comment');
        }

        $file->reviewer_id = Auth::user()->id;
        $file->rated_at = date('Y-m-d H:i:s');

        $file->save();

        if ($request->hasFile('email_attachment')) {
            $attachment = $request->file('email_attachment');

            // Store locally so we can do things with it otherwise it is just a
            // temp file.
            $upload = $attachment->store('email_attachments', 'local');
            $egg_file_name = explode('.', $file->name)[0];
            $filename = $egg_file_name.'-attachment.'.$attachment->getClientOriginalExtension();
            $projectID = $file->project_id;

            // Store a copy in the S3 bucket in an organized fashion
            Storage::makeDirectory('storage/files/'.$projectID.'/emails');
            $attachment->storeAs(
                'storage/files/'.$projectID.'/emails',
                $filename
            );

            $upload = storage_path().'/app/'.$upload;
        }

        if (! $request->has('is_draft') || $request->get('is_draft') == 'no') {
            if ($request->has('send_email_all_users')) {
                Mail::to('support@neurosig.com')
                    ->cc($file->site->users->pluck('email')->toArray())
                    ->send(new ReviewCreated($file, $upload, $filename));
            } else {
                if ($request->has('send_email')) {
                    Mail::to($file->user->email)
                            ->cc('support@neurosig.com')
                            ->send(new ReviewCreated($file, $upload, $filename));
                }
            }
        }

        if ($request->hasFile('email_attachment')) {
            unlink($upload);
        }

        return Redirect::route('downloads.index')->with('message', 'Review updated!');
    }

    public function parent(Request $request, $fileID)
    {
        $parentID = $request->has('parent_id') ? $request->parent_id : 0;

        $file = File::findOrFail($fileID);
        $file->parent_id = $parentID;
        $file->save();

        return Redirect::route('downloads.index')->with('message', 'File parent ID updated!');
    }
}
