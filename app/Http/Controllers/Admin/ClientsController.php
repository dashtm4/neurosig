<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ClientsController extends Controller
{
    //Index
    public function index(Request $request)
    {
        $clients = Client::orderBy('identifier', 'name')->paginate(15);

        return view('admin.clients.index')->with('clients', $clients);
    }

    //Closed Display
    public function closed()
    {
        $sortBy = 'name';

        $query = Client::onlyTrashed()->orderBy($sortBy, $sortBy == 'created_at' ? 'name' : 'asc')
            ->orderBy('created_at', 'name');

        $projects = $query->get();

        return view('admin.clients.index')->with(['clients' => $projects, 'sortBy' => $sortBy]);
    }

    //Create and Store
    public function create()
    {
        return view('admin.clients.create');
    }

    public function store(ClientRequest $request)
    {
        $client = new Client;

        $client->identifier = $request->get('identifier');
        $client->name = $request->get('name');

        $client->save();

        return Redirect::route('clients.index')->with('message', 'Client saved!');
    }

    //Show
    public function show($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.show')->with('client', $client);
    }

    //Edit
    public function edit($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.edit')->with('client', $client);
    }

    //Update
    public function update(ClientRequest $request, $id)
    {
        $client = Client::findOrFail($id);

        $client->identifier = $request->get('identifier');
        $client->name = $request->get('name');

        $client->save();

        return Redirect::route('clients.edit', [$client->id])->with('message', 'Your Client has been updated!');
    }

    //Restore
    public function restore($clientId)
    {
        Project::withTrashed()->find($clientId)->restore();

        return back()->with('message', 'Client Restored!');
    }

    //Delete
    public function destroy($id)
    {
        $client = Client::findOrFail($id);

        $client->delete();

        return Redirect::route('clients.index')->with('message', 'Client deleted!');
    }
}
