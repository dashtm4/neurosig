<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SnippetRequest;
use App\Snippet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class SnippetsController extends Controller
{
    //Index

    public function index(Request $request)
    {
        $query = Snippet::orderBy('name', 'asc');

        $snippets = $query->get();

        return view('admin.snippets.index')->withSnippets($snippets);
    }

    //Create and Store

    public function create()
    {
        return view('admin.snippets.create');
    }

    public function store(SnippetRequest $request)
    {
        $snippet = new Snippet;

        $snippet->name = $request->get('name');
        $snippet->snippet = $request->get('snippet');
        $snippet->type = $request->get('type');
        $snippet->category = $request->get('category');

        $snippet->save();

        return Redirect::route('snippets.index')->with('message', 'Snippet saved!');
    }

    //Show

    public function show($id)
    {
        $snippet = Snippet::find($id);

        return view('admin.snippets.show')->with('snippet', $snippet);
    }

    //Edit

    public function edit($id)
    {
        $snippet = Snippet::findOrFail($id);

        return view('admin.snippets.edit')->with('snippet', $snippet);
    }

    //Update

    public function update(SnippetRequest $request, $id)
    {
        $snippet = Snippet::findOrFail($id);

        $snippet->name = $request->get('name');
        $snippet->snippet = $request->get('snippet');
        $snippet->type = $request->get('type');
        $snippet->category = $request->get('category');

        $snippet->save();

        return Redirect::route('snippets.edit', [$snippet->id])->with('message', 'Your Snippet has been updated!');
    }

    //Delete

    public function destroy($id)
    {
        $snippet = Snippet::findOrFail($id);

        $snippet->delete();

        return Redirect::route('snippets.index')->with('message', 'Snippet deleted!');
    }
}
