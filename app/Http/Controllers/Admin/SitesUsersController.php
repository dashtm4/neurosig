<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SiteRequest;
use Illuminate\Http\Request;

class SitesUsersController extends Controller
{
    /**
     * Index Display.
     */
    public function index()
    {
    }

    /**
     * Create and Store.
     */
    public function create()
    {
    }

    public function store(SiteRequest $request)
    {
    }

    /**
     * Show.
     */
    public function show($id)
    {
    }

    /**
     * Edit.
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiteRequest $request, $id)
    {
    }

    /**
     * Delete.
     */
    public function destroy($id)
    {
    }
}
