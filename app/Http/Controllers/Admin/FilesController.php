<?php

namespace App\Http\Controllers\Admin;

use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    //Index Display
    public function index(Request $request)
    {
        $hasSortBy = $request->has('sort_by');
        $sortBy = $hasSortBy ? $request->get('sort_by') : 'name';
        $hasShowDeleted = $request->has('show_deleted') && $request->get('show_deleted') != null;

        $query = File::orderBy($sortBy, $sortBy == 'created_at' ? 'desc' : 'asc')
            ->orderBy('created_at', 'desc');

        if ($request->has('project_id') && $request->get('project_id') != null) {
            $query->where('project_id', $request->get('project_id'));
        }

        if ($request->has('site_id') && $request->get('site_id') != null) {
            $query->where('site_id', $request->get('site_id'));
        }

        if ($request->has('file_type') && $request->get('file_type') != null) {
            $query->where('file_type', $request->get('file_type'));
        }

        //if show deleted is checked
        if ($hasShowDeleted) {
            $query->withTrashed();
        }

        $files = $query->get();

        return view('admin.files.index')->with([
            'files' => $files,
            'sortBy' => $sortBy,
            'hasShowDeleted' => $hasShowDeleted,
        ]);
    }

    //Create and Store
    public function create()
    {
        return view('admin.files.create');
    }

    public function store(Request $request)
    {
        $file = new File();

        $file->name = $request->get('name');
        $file->site_id = $request->get('site_id');

        $file->save();

        return Redirect::route('files.index')->with('message', 'File saved!');
    }

    //Show
    public function show($id)
    {
        $file = File::withTrashed()->findOrFail($id);

        return view('admin.files.show')->with('file', $file);
    }

    //Edit
    public function edit($id)
    {
        $file = File::findOrFail($id);

        return view('admin.files.edit')->with('file', $file);
    }

    //Update
    public function update(Request $request, $id)
    {
        $file = File::findOrFail($id);

        if ($request->has('site_id') && $request->get('site_id') != null) {
            $file->site_id = $request->get('site_id');
        }

        if ($request->has('name') && $request->get('name') != null && $request->get('name') != $file->name) {
            $oldFileName = $file->name;
            $newFileName = $request->get('name');
            $file->name = $newFileName;

            $doesFileExist = Storage::disk('s3')->exists('storage/files/'.$oldFileName);
            if ($doesFileExist) {
                Storage::move('storage/files/'.$oldFileName, 'storage/files/'.$newFileName);
            }
        }

        $file->save();

        return Redirect::route('files.edit', [$file->id])->with('message', 'Your File has been updated!');
    }

    public function destroyForProject($projectId)
    {
        /*
         * TODO:
         * Comment out line 137
         **/
        File::where('project_id', $projectId)->delete();

        /*
         * TODO:
         * Add an int column to the files table with the key "deleted_reason"
         * Uncomment lines 144-146
         */
        // $timestamp = Carbon\Carbon::now()->toDateTimeString();
        // $data = ['deleted_at' => $timestamp, 'deleted_reason' => 500];
        // File::where('id', $id)->update($data);

        return back()->with('message', 'Files Deleted!');
    }

    //Restore For Project - all files
    public function restoreForProject($projectId)
    {

        /*
         * TODO: Comment out line 161
         */
        File::where('project_id', $projectId)->onlyTrashed()->restore();

        /*
         * TODO Uncomment Lines 168
         */
        // File::where('project_id', $projectId)->where('deleted_reason', 500)->onlyTrashed()->restore();

        return back()->with('message', 'Files Restored!');
    }

    //Restore - this is not possible if we are actually deleting them
    // public function restore($fileId)
    // {
    //     Project::withTrashed()->find($fileId)->restore();
    //     return back()->with('message', 'File Restored!');
    // }

    //Delete
    public function destroy($id)
    {
        $file = File::findOrFail($id);

        // Storage::delete('storage/files/' . $file->name);  Use soft deletes instead.

        $file->delete();

        return Redirect::route('files.index')->with('message', 'File deleted!');
    }
}
