<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Mail\UserCreated;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    //Index Display
    public function index(Request $request)
    {
        $hasSortBy = $request->has('sort_by') && $request->get('sort_by') != null;
        $hasClear = $request->has('clear') && $request->get('clear') != null;
        $hasShowDeleted = $request->has('show_deleted') && $request->get('show_deleted') != null;

        $isSite = ! $request->has('site_user') || ($request->has('site_user') && $request->get('site_user'));
        // dump($request->has('isSite'));
        // dump($isSite);
        $isMonitor = ! $request->has('monitor_user') || ($request->has('monitor_user') && $request->get('monitor_user'));
        $isProject = ! $request->has('project_user') || ($request->has('project_user') && $request->get('project_user'));
        $allUsers = ! $request->has('all_users') || ($request->has('all_users') && $request->get('all_users'));

        $sortBy = $hasSortBy ? $request->get('sort_by') : 'name';

        $query = User::orderBy($sortBy, $sortBy == 'created_at' ? 'desc' : 'asc')
            ->orderBy('fullName', 'asc');

        if (! $allUsers) {
            if ($isSite) {
                $query->orWhere('is_site', 1);
            }
            if ($isMonitor) {
                $query->where('is_monitor', 1);
            }
            if ($isProject) {
                $query->where('is_project', 1);
            }
        }

        if ($request->has('project_id') && $request->get('project_id') != null) {
            $query->select('users.*')->join('project_user', 'users.id', '=', 'project_user.user_id')
                ->where('project_user.project_id', $request->get('project_id'));
        }

        if ($request->has('site_id') && $request->get('site_id') != null) {
            $query->select('users.*')->join('site_user', 'users.id', '=', 'site_user.user_id')
                ->where('site_user.site_id', $request->get('site_id'));
        }

        if ($hasShowDeleted) {
            $query->withTrashed();
        }

        $users = $query->get();

        $emails = $users->implode('email', ',');

        return view('admin.users.index')->with([
                'users' => $users,
                'hasShowDeleted' => $hasShowDeleted,
                'sortBy' => $sortBy,
                'emails' => $emails,
                'isSite' => $isSite,
                'isProject' => $isProject,
                'isMonitor' => $isMonitor,
                'allUsers' => $allUsers,
            ]);
    }

    //Create and Store
    public function create()
    {
        return view('admin.users.create');
    }

    public function store(UserRequest $request)
    {
        $user = new User();
        $user->name = $request->get('name');
        $user->lastName = $request->get('lastName');
        $user->email = $request->get('email');
        $user->telephone = $request->get('telephone');
        $user->fullName = $request->get('fullName');
        $user->userId = $request->get('userId');
        $user->password = bcrypt($request->get('password'));
        $user->role = $request->get('role');
        $user->status = $request->get('status');
        $user->notes = $request->get('notes');
        $user->privacy = $request->get('privacy');
        $user->is_system = $request->has('is_system') ? 1 : 0;
        $user->is_admin = $request->has('is_admin') ? 1 : 0;
        $user->is_expert = $request->has('is_expert') ? 1 : 0;
        $user->is_trainer = $request->has('is_trainer') ? 1 : 0;
        $user->is_site = $request->has('is_site') ? 1 : 0;
        $user->is_other = $request->has('is_other') ? 1 : 0;
        $user->is_monitor = $request->has('is_monitor') ? 1 : 0;
        $user->is_project = $request->has('is_project') ? 1 : 0;

        $user->save();

        // Assign user to projects and sites
        $user->projects()->sync($request->get('projects'));
        $user->sites()->sync($request->get('sites'));

        // Send the user an email with thier password
        if ($request->has('send_email')) {
            Mail::to($user->email)
                ->bcc('admin@neurosig.com')
                ->send(new UserCreated($user, $request->get('password')));
        }

        return Redirect::route('users.edit', [$user->id])->with('message', 'User created!');
    }

    //Show
    public function show($id)
    {
        $user = User::withTrashed()->find($id);

        return view('admin.users.show')->with('user', $user);
    }

    //Edit
    public function edit($id)
    {
        $user = User::withTrashed()->findOrFail($id);

        $sitesSelection = collect();
        foreach ($user->projects as $project) {
            $projectSites = $project->sites->mapWithKeys(function ($site, $key) {
                return [$site->id => $site->name];
            });

            $sitesSelection = $sitesSelection->union(
                $projectSites
            );
        }

        return view('admin.users.edit')->with([
            'user' => $user,
            'sitesSelection' => $sitesSelection,
        ]);
    }

    //Update
    public function update(UserRequest $request, $id)
    {
        $hasSystem = $request->has('is_system') ? true : false;
        $hasAdmin = $request->has('is_admin') ? true : false;
        $hasExpert = $request->has('is_expert') ? true : false;
        $hasTrainer = $request->has('is_trainer') ? true : false;
        $hasSite = $request->has('is_site') ? true : false;
        $hasOther = $request->has('is_other') ? true : false;
        $hasMonitor = $request->has('is_monitor') ? true : false;
        $hasProject = $request->has('is_project') ? true : false;
        $hasShowDeleted = $request->has('show_deleted') && $request->get('show_deleted') != null;

        $user = User::find($id);

        $user->name = $request->get('name');
        $user->lastName = $request->get('lastName');
        $user->email = $request->get('email');
        $user->telephone = $request->get('telephone');
        $user->fullName = $request->get('fullName');
        $user->userId = $request->get('userId');

        if ($request->has('password') && $request->get('password') != null) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->role = $request->get('role');
        $user->status = $request->get('status');
        $user->notes = $request->get('notes');
        // Intentionally leaving privacy out
        $user->is_system = $hasSystem && $request->get('is_system') != null ? $request->get('is_system') : 0;
        $user->is_admin = $hasAdmin && $request->get('is_admin') != null ? $request->get('is_admin') : 0;
        $user->is_expert = $hasExpert && $request->get('is_expert') != null ? $request->get('is_expert') : 0;
        $user->is_trainer = $hasTrainer && $request->get('is_trainer') != null ? $request->get('is_trainer') : 0;
        $user->is_site = $hasSite && $request->get('is_site') != null ? $request->get('is_site') : 0;
        $user->is_other = $hasOther && $request->get('is_other') != null ? $request->get('is_other') : 0;
        $user->is_monitor = $hasMonitor && $request->get('is_monitor') != null ? $request->get('is_monitor') : 0;
        $user->is_project = $hasProject && $request->get('is_project') != null ? $request->get('is_project') : 0;

        $user->update();

        // Assign user to projects and sites
        $user->projects()->sync($request->get('projects'));
        $user->sites()->sync($request->get('sites'));

        // Send the user an email with their password
        if ($request->has('send_email')) {
            Mail::to($user->email)
                ->bcc('admin@neurosig.com')
                ->send(new UserCreated($user, $request->get('password')));
        }

        return Redirect::route('users.edit', [$user->id])->with('message', 'User has been updated!');
    }

    public function updatePrivacy($id)
    {
        $user = User::findOrFail($id);
        $user->privacy = now();
        $user->save();
    }

    //Restore
    public function restore($userId)
    {
        User::withTrashed()->find($userId)->restore();

        return back()->with('message', 'User Restored!');
    }

    //Delete
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return Redirect::route('users.index')->with('message', 'User deleted!');
    }
}
