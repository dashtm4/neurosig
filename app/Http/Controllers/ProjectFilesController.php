<?php

namespace App\Http\Controllers;

use App\File;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectFilesController extends Controller
{
    public function index(Request $request)
    {
        $sites = Site::whereIn('project_id', Auth::user()->projects->pluck('id'))->pluck('id');

        $hasSortBy = $request->has('sort_by');
        $sortBy = $hasSortBy ? $request->get('sort_by') : 'created_at';

        //  Display files with a parent of 0 and a file type of Study

        $query = File::whereIn('site_id', $sites)->where('parent_id', 0)->where('file_type', 'Study');

        if ($hasSortBy) {
            $query->orderBy($sortBy, 'asc');
        } else {
            $query->orderBy('created_at', 'desc');
        }

        if ($request->has('project_id') && $request->get('project_id') != null) {
            $query->where('project_id', $request->get('project_id'));
        }

        $files = $query->get();

        $userProjects = Auth::user()->projects;

        return view('projectfiles.index')->with([
            'files'=> $files,
            'project_id'=> $request->has('project_id') ? $request->get('project_id') : '',
            'userProjects'=>$userProjects,
            'sortBy' => $sortBy,
            ]);
    }

    public function show($id)
    {
        $file = File::findOrFail($id);

        return view('projectfiles.show')->with('file', $file);
    }
}
