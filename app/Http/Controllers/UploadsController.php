<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Requests\UploadRequest;
use App\Mail\UploadAlert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Response;

class UploadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('uploads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadRequest $request)
    {

        //$fileExists = File::where('name', $request->get('name'))->exists();

        //if ($fileExists) {

        //$ids = File::where('name', $request->get('name'))->get()->pluck('id')->toArray();
        //    Log::info("here");
        //    File::where('name', $request->get('name'))->delete();

        //}

        $siteID = $request->get('site_id');
        $projectID = $request->get('project_id');
        $file = $request->file('eeg');

        //$existedBeforeUpload = Storage::disk('s3')->exists('storage/files/' . $request->eeg->getClientOriginalName());

        // Does the file name already exist in the database?
        $fileExists = File::where('name', $request->eeg->getClientOriginalName())->first();

        // We're going to overwrite the previous conflicting file regardless of whether the name
        // is already found in the database.

        Storage::makeDirectory('storage/files/'.$projectID);
        $request->file('eeg')->storeAs('storage/files/'.$projectID, $request->eeg->getClientOriginalName());

        $uploadedFile = new File();
        if ($fileExists) {
            $uploadedFile->rating = $fileExists->rating;
            $uploadedFile->rated_at = $fileExists->rated_at;
            $uploadedFile->reviewer_notes = $fileExists->reviewer_notes;
            $uploadedFile->reviewer_id = $fileExists->reviewer_id;
            $uploadedFile->status = $fileExists->status;
            $uploadedFile->comment .= "Duplicate of $fileExists->id";
        }
        $uploadedFile->name = $request->eeg->getClientOriginalName();
        $uploadedFile->site_id = $request->get('site_id');
        $uploadedFile->project_id = $request->get('project_id');
        $uploadedFile->user_id = Auth::user()->id;
        $uploadedFile->size = $request->file('eeg')->getClientSize();
        $uploadedFile->save();

        if ($fileExists) {
            $fileExists->comment .= "Overwritten by $uploadedFile->id \n";
            $fileExists->rating = 0;
            $fileExists->status = 'Soft Deleted';
            $fileExists->save();
            $fileExists->delete();
        }

        Mail::to('data@neurosig.com')->send(new UploadAlert($uploadedFile));

        return Redirect::route('uploads.create')->with('message', 'File uploaded!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function uploaderStore(Request $request)
    {
        $files = $request->file('file');
        $siteID = $request->get('site_id');
        $projectID = $request->get('project_id');

        if (! is_array($files)) {
            $files = [$files];
        }

        foreach ($files as $file) {
            // Does the file name already exist in the database?
            $fileExists = File::where('name', $file->getClientOriginalName())->first();

            // We're going to overwrite the previous conflicting file regardless of whether the name
            // is already found in the database.

            Storage::makeDirectory('storage/files/'.$projectID);
            $file->storeAs('storage/files/'.$projectID, $file->getClientOriginalName());

            $uploadedFile = new File();
            if ($fileExists) {
                $uploadedFile->rating = $fileExists->rating;
                $uploadedFile->rated_at = $fileExists->rated_at;
                $uploadedFile->reviewer_notes = $fileExists->reviewer_notes;
                $uploadedFile->reviewer_id = $fileExists->reviewer_id;
                $uploadedFile->status = $fileExists->status;
                $uploadedFile->comment .= "Duplicate of $fileExists->id";
            }
            $uploadedFile->name = $file->getClientOriginalName();
            $uploadedFile->site_id = $siteID;
            $uploadedFile->project_id = $projectID;
            $uploadedFile->user_id = Auth::user()->id;
            $uploadedFile->size = $file->getClientSize();
            $uploadedFile->save();
            Mail::to('data@neurosig.com')->send(new UploadAlert($uploadedFile));

            if ($fileExists) {
                $fileExists->comment .= "Overwritten by $uploadedFile->id \n";
                $fileExists->rating = 0;
                $fileExists->status = 'Soft Deleted';
                $fileExists->save();
                $fileExists->delete();
            }
        }

        return Response::json([
            'message' => 'Files saved Successfully',
        ], 200);
    }

    public function uploader()
    {
        return view('uploads.uploader');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
