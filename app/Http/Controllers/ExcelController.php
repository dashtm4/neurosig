<?php

namespace App\Http\Controllers;

use App\File;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function index(Request $request)
    {
        $sites = Site::whereIn('project_id', Auth::user()->projects->pluck('id'))->pluck('id');

        //  Display files with a parent of 0 and a file type of Study

        // to limit columns selected, use ->select('id', 'name', 'rating')

        $query = File::whereIn('site_id', $sites)->
        select(
            'files.id',
            'file_type',
            'files.name',
            'subject_id',
            'subject_visit',
            'eeg_date',
            'files.created_at',
            'rating',
            'qualityCategory',
            'sites.identifier',
            'sites.country',
            'reviewer_notes',
            'files.status',
            'sites.status',
            'comment'
        )
            ->join('sites', 'files.site_id', '=', 'sites.id')
            ->where('parent_id', 0)->where('file_type', 'Study');

        if ($request->get('project_id') != null) {
            $query->where('files.project_id', $request->get('project_id'));
        }

        $files = $query->get();

        $files->transform(function ($file) {
            $file->name = $file->fileExtensionStrip();

            return $file;
        });

        // Initialize the array which will be passed into the Excel
        // generator.
        $filesArray = [];

        // Define the Excel spreadsheet headers

        $filesArray[] = [
        'Id',
        'File Type',
        'File Name',
        'Subject ID',
        'Visit ID',
        'EEG Date',
        'Upload Date',
        'Quality Rating',
        'Quality Category',
        'Site ID',
        'Country',
        'Reviewer Feedback',
        'Status',
        'Comment',
        ];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($files as $file) {
            $filesArray[] = $file->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('NeuroSigPortal', function ($excel) use ($filesArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Projects');
            $excel->setCreator('NeuroSig')->setCompany('NeuroSig');
            $excel->setDescription('project files');

            // Build the spreadsheet, passing in the files array
            $excel->sheet('sheet1', function ($sheet) use ($filesArray) {
                $sheet->fromArray($filesArray, null, 'A1', false, false);
            });
        })->download('xlsx');
    }
}
