<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DownloadsController extends Controller
{
    public function index(Request $request)
    {
        // for a Sort to order by Create Date
        $hasSortBy = $request->has('sort_by') && $request->get('sort_by') != null;
        $hasSiteId = $request->has('site_id') && $request->get('site_id') != null;
        $hasProjectId = $request->has('project_id') && $request->get('project_id') != null;
        $hasFileType = $request->has('file_type') && $request->get('file_type') != null;
        $hasParentId = $request->has('parent') && is_numeric($request->get('parent'));
        $hasRating = $request->has('rating') && $request->get('rating') != null;
        $hasNoStatus = $request->has('no_status') && $request->get('no_status') != null;
        $hasShowDeleted = $request->has('show_deleted') && $request->get('show_deleted') != null;
        $hasClear = $request->has('clear') && $request->get('clear') != null;

        $sortBy = $hasSortBy ? $request->get('sort_by') : 'name';

        $query = File::orderBy($sortBy, $sortBy == 'created_at' ? 'desc' : 'asc')
            ->orderBy('created_at', 'desc');

        if ($hasProjectId) {
            $query->where('project_id', $request->get('project_id'));
        }

        if ($hasSiteId) {
            $query->where('site_id', $request->get('site_id'));
        }

        if ($hasFileType) {
            $query->where('file_type', $request->get('file_type'));
        }

        if ($hasParentId) {
            $query->where('parent_id', $request->get('parent'));
        }

        if ($hasShowDeleted) {
            $query->withTrashed();
        }

        if ($hasRating) {
            $query->where('rating', 0);
            $query->where('parent_id', 0);
            $query->where('file_type', 'Study');
        }

        if ($hasNoStatus) {
            $query->where('status', null);
        }

        // Set a defuault Query
        $shouldDefault = ! $hasProjectId && ! $hasSiteId && ! $hasClear && ! $hasFileType && ! $hasParentId && ! $hasRating && ! $hasNoStatus && ! $hasShowDeleted;
        if ($shouldDefault) {
            $query->where('file_type', 'study');
            $query->where('parent_id', 0);
            $query->where('rating', 0);
            $query->where('status', null);
            $hasRating = true;
            $hasParentId = true;
            $hasNoStatus = true;
        }

        $files = $query->get();

        return view('downloads.index')
            ->with([
                'files' => $files,
                'hasParentId' => $hasParentId,
                'hasRating' => $hasRating,
                'hasNoStatus' => $hasNoStatus,
                'hasShowDeleted' => $hasShowDeleted,
                'sortBy' => $sortBy,
            ]);
    }

    public function download($id)
    {
        $systemFile = File::find($id);

        $systemFile->downloaded_at = \Carbon\Carbon::now()->toDateTimeString();

        $systemFile->save();

        $file = Storage::disk(config('filesystems.default'))->get('storage/files/'.$systemFile->project_id.'/'.$systemFile->name);

        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => "attachment; filename={$systemFile->name}",
            'filename' => $systemFile->name,
        ];

        return response($file, 200, $headers);
    }

    public function rename($id)
    {
    }
}
