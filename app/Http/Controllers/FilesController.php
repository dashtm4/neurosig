<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

         // for a Sort to order by Create Date
        $hasSortBy = $request->has('sort_by') && $request->get('sort_by') != null;
        $hasSiteId = $request->has('site_id') && $request->get('site_id') != null;
        $hasFileType = $request->has('file_type') && $request->get('file_type') != null;
        $hasParentId = $request->has('parent') && is_numeric($request->get('parent'));
        $hasClear = $request->has('clear') && $request->get('clear') != null;

        $sortBy = $hasSortBy ? $request->get('sort_by') : 'name';

        $userSites = Auth::user()->sites;

        $hasSortBy = $request->has('sort_by');
        $sortBy = $hasSortBy ? $request->get('sort_by') : 'created_at';

        $query = File::whereIn('site_id', $userSites->pluck('id'))
            ->orderBy($sortBy, 'desc')
            ->orderBy('name', 'asc');

        if ($hasSiteId) {
            $query->where('site_id', $request->get('site_id'));
        }

        if ($hasFileType) {
            $query->where('file_type', $request->get('file_type'));
        }

        if ($hasParentId) {
            $query->where('parent_id', $request->get('parent'));
        }

        // Set a defuault Query
        $shouldDefault = ! $hasSiteId && ! $hasClear && ! $hasFileType && ! $hasParentId;
        if ($shouldDefault) {
            $query->where('file_type', 'study');
            $query->where('parent_id', 0);
            $hasParentId = true;
        }

        $files = $query->paginate(5000);

        return view('files.index')
            ->with([
                'files' => $files,
                'userSites' => $userSites,
                'hasParentId' => $hasParentId,
                'sortBy' => $sortBy,
            ]);
    }
}
