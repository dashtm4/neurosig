<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class TrainerAuthentication
{
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            if ($this->auth->user()->is_trainer == true) {
                return $next($request);
            }
        }

        return redirect(url('/'));
    }
}
