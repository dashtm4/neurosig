<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_id' => 'required',
            'eeg'     => 'required',
        ];
    }

    // Paste below in after 'required' on line 28
    // |mimetypes:text/plain,application/zip,application/x-rar-compressed, application/x-brainvision-data, text/x-brainvision-marker,text/x-brainvision-header,application/pdf

    public function messages()
    {
        return [
            'site_id.required' => 'Please select a site',
            'eeg.required'     => 'Please choose a file to upload',
            //'eeg.mimetypes'    => 'The following file formats are supported: zip, text'
        ];
    }
}
