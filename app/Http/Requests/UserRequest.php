<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'email'         => 'required|unique:users',
                    'sites'         => 'required|array|min:1',
                    'projects'      => 'required|array|min:1',
                    'password'      => 'required',
                    'userId'        => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'email'         => 'required',
                    'sites'         => 'required|array|min:1',
                    'projects'      => 'required|array|min:1',
                    'userId'        => 'required',
                ];
            }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'email.required' => 'A unique email address is required.',
        ];
    }
}
