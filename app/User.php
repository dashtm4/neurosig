<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'role', 'lastName', 'email', 'password',
     ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'is_system' => 'boolean',
        'is_admin' => 'boolean',
        'is_expert' => 'boolean',
        'is_trainer' => 'boolean',
        'is_site' => 'boolean',
        'is_other' => 'boolean',
        'is_monitor' => 'boolean',
        'is_project' => 'boolean',
    ];

    public function sites()
    {
        return $this->belongsToMany(\App\Site::class)->withTimestamps();
    }

    public function files()
    {
        return $this->hasMany(\App\File::class);
    }

    public function projects()
    {
        return $this->belongsToMany(\App\Project::class)->withTimestamps();
    }
}
