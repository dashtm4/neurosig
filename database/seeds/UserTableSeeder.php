<?php

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $TestPassword = '01NeuroSigSuperTester!';
        // 1
        User::create([
            'name' => 'Teresa',
            'fullName' => 'Teresa Light',
            'userId' => 'TML',
            'email' =>'teresa@teresalight.com',
            'password' => bcrypt('1234'),
            'is_system' => '1',
            'is_admin' => '1',
            'is_expert' => '1',
            'is_trainer' => '1',
            'is_site' => '1',
            'is_other' => '1',
        ]);

        // 2
        User::create([
            'name' => 'Jason',
            'fullName' => 'Jason Gilmore',
            'userId' => 'JG',
            'email' =>'wj@wjgilmore.com',
            'password' => bcrypt('1234'),
            'is_system' => '1',
            'is_admin' => '1',
            'is_expert' => '1',
            'is_trainer' => '1',
            'is_site' => '1',
            'is_other' => '1',
        ]);

        // 3
        User::create([
            'name' => 'Admin Test User',
            'fullName' => 'Admin Test User',
            'userId' => 'TEST',
            'email' =>'teresa@01hearts.com',
            'password' => bcrypt($TestPassword),
            'is_system' => '0',
            'is_admin' => '1',
            'is_expert' => '0',
            'is_trainer' => '0',
            'is_site' => '0',
            'is_other' => '0',
        ]);

        // 4
        User::create([
            'name' => 'Expert Test User',
            'fullName' => 'Expert Test User',
            'userId' => 'TEST',
            'email' =>'teresa@neurosig.com',
            'password' => bcrypt($TestPassword),
            'is_system' => '0',
            'is_admin' => '0',
            'is_expert' => '1',
            'is_trainer' => '0',
            'is_site' => '0',
            'is_other' => '0',
          ]);

        // 5
        User::create([
            'name' => 'Trainer Test User',
            'fullName' => 'Trainer Test User',
            'userId' => 'TEST',
            'email' =>'support@neurosig.com',
            'password' => bcrypt($TestPassword),
            'is_system' => '0',
            'is_admin' => '0',
            'is_expert' => '0',
            'is_trainer' => '1',
            'is_site' => '0',
            'is_other' => '0',
          ]);

        // 6
        User::create([
            'name' => 'Site Test User',
            'fullName' => 'Site Test User',
            'userId' => 'TEST',
            'email' =>'admin@neurosig.com',
            'password' => bcrypt($TestPassword),
            'is_system' => '0',
            'is_admin' => '0',
            'is_expert' => '0',
            'is_trainer' => '0',
            'is_site' => '1',
            'is_other' => '0',
          ]);

        // 7
        User::create([
            'name' => 'Greg',
            'fullName' => 'Greg Light',
            'userId' => 'GL',
            'email' =>'greg@neurosig.com',
            'password' => bcrypt('1SuperScientist!'),
            'is_system' => '0',
            'is_admin' => '0',
            'is_expert' => '1',
            'is_trainer' => '0',
            'is_site' => '0',
            'is_other' => '0',
        ]);

        // 8
        User::create([
            'name' => 'Trainee',
            'fullName' => 'Site Trainee',
            'userId' => 'TRAIN',
            'email' =>'training@neurosig.com',
            'password' => bcrypt('Heptares202'),
            'is_system' => '0',
            'is_admin' => '0',
            'is_expert' => '0',
            'is_trainer' => '0',
            'is_site' => '1',
            'is_other' => '0',
        ]);

        // 9
        User::create([
            'name' => 'Marlena',
            'fullName' => 'Marlena Pela',
            'userId' => 'MP',
            'email' =>'marlenapela@gmail.com',
            'password' => bcrypt('1SuperTrainer!'),
            'is_system' => '0',
            'is_admin' => '0',
            'is_expert' => '0',
            'is_trainer' => '1',
            'is_site' => '0',
            'is_other' => '0',
        ]);
    }
}
