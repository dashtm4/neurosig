<?php

use App\Project;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//  NeuroSig
        //  'project_id'=>'1'

        Project::create([
            'identifier'=>'NeuroSig Internal',
            'name'=>'NeuroSig',
            'category'=>'Internal',
            'status'=>'Open',
            'notes'=>'Internal Project for Testing',
            'client_id'=>'1',
        ]);

        //  US Navy
        //  'project_id'=>'2'

        Project::create([
            'identifier'=>'US Navy',
            'name'=>'US Navy Study',
            'category'=>'Client',
            'status'=>'Open',
            'notes'=>'Notes for US Navy Project',
            'client_id'=>'2',
        ]);

        //  Europe
        //  'project_id'=>'3'

        Project::create([
            'identifier'=>'Hep202 Project',
            'name'=>'Hep202 Study',
            'category'=>'Client',
            'status'=>'Open',
            'notes'=>'Notes for Hep202 Project',
            'client_id'=>'3',
        ]);
    }
}
