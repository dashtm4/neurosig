<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $this->call(UserTableSeeder::class);

        DB::table('clients')->delete();
        $this->call(ClientTableSeeder::class);

        DB::table('projects')->delete();
        $this->call(ProjectTableSeeder::class);

        DB::table('sites')->delete();
        $this->call(SiteTableSeeder::class);

        // DB::table('files')->delete();
        // $this->call(FileTableSeeder::class);

        DB::table('site_user')->delete();
        $this->call(SiteUserTableSeeder::class);
    }
}
