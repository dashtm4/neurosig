<?php

use App\SiteUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class SiteUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteUser::create([
            'user_id'=>'1',
            'site_id'=>'1',
        ]);

        SiteUser::create([
            'user_id'=>'2',
            'site_id'=>'1',
        ]);

        SiteUser::create([
            'user_id'=>'3',
            'site_id'=>'1',
        ]);

        SiteUser::create([
            'user_id'=>'4',
            'site_id'=>'1',
        ]);

        SiteUser::create([
            'user_id'=>'5',
            'site_id'=>'1',
        ]);

        SiteUser::create([
            'user_id'=>'6',
            'site_id'=>'1',
        ]);

        SiteUser::create([
            'user_id'=>'7',
            'site_id'=>'1',
        ]);

        SiteUser::create([
            'user_id'=>'8',
            'site_id'=>'1',
        ]);

        SiteUser::create([
            'user_id'=>'9',
            'site_id'=>'1',
        ]);
    }
}
