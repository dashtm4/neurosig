<?php

use App\Site;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class SiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Site::create([
            'identifier'=>'001',
            'name'=>'NeuroSig',
            'country'=>'USA',
            'project_id'=>'1',
        ]);

        Site::create([
            'identifier'=>'002',
            'name'=>'US Navy',
            'country'=>'USA',
            'project_id'=>'2',
        ]);

        Site::create([
            'identifier'=>'205',
            'name'=>'CLINTRIAL s.r.o.',
            'country'=>'Czech Republic',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'101',
            'name'=>'NZOZ Syntonia Przychodnia Specjalistyczna w Pruszczu Gdańskim',
            'country'=>'Poland',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'102',
            'name'=>'Przychodnia Lekarsko-Psychologiczna PERSONA',
            'country'=>'Poland',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'104',
            'name'=>'Euromedis',
            'country'=>'Poland',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'105',
            'name'=>'NZOZ Neuro-Kard',
            'country'=>'Poland',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'107',
            'name'=>'Malopolskie Centrum Kliniczne',
            'country'=>'Poland',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'108',
            'name'=>'Podlaskie Centrum Psychogeriatrii',
            'country'=>'Poland',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'201',
            'name'=>'Clinline Services s.r.o.',
            'country'=>'Czech Republic',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'202',
            'name'=>'Neuropsychiatriehk, s.r.o.',
            'country'=>'Czech Republic',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'203',
            'name'=>'Vestra Clinics s.r.o.',
            'country'=>'Czech Republic',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'205',
            'name'=>'CLINTRIAL s.r.o.',
            'country'=>'Czech Republic',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'302',
            'name'=>'Hospital General de Catalunya',
            'country'=>'Spain',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'303',
            'name'=>'Hospital Universitario Virgen Macarena',
            'country'=>'Spain',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'305',
            'name'=>'Clinica Rehasalud',
            'country'=>'Spain',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'306',
            'name'=>'Centro de Salud la Alamedilla',
            'country'=>'Spain',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'308',
            'name'=>'Instituto Internacional de Neurociencias Aplicadas (IINA)',
            'country'=>'Spain',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'401',
            'name'=>'Konzilium, s.r.o.',
            'country'=>'Slovakia',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'403',
            'name'=>'Mentum s.r.o, Psychiatricka ambulancia',
            'country'=>'Slovakia',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'404',
            'name'=>'I. neurologická klinika LFUK a UNB',
            'country'=>'Slovakia',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'406',
            'name'=>'Psychiatrické oddelenie Nemocnica s poliklinikou sv. Barbory Rožňava, a.s.',
            'country'=>'Slovakia',
            'project_id'=>'3',
        ]);

        Site::create([
            'identifier'=>'407',
            'name'=>'MUDr. Beáta Dupejová, neurologická ambulancia, s.r.o',
            'country'=>'Slovakia',
            'project_id'=>'3',
        ]);
    }
}
