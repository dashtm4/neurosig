<?php

use App\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create([
            'identifier'=>'Hep',
            'name'=>'Heptares Therapeutics',
        ]);

        Client::create([
            'identifier'=>'Navy',
            'name'=>'US Navy',
        ]);

        Client::create([
            'identifier'=>'Test Client',
            'name'=>'Test Client',
        ]);
    }
}
