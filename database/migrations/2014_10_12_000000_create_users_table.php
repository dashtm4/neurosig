<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('name');
            $table->string('lastName')->nullable();
            $table->string('email')->unique();
            $table->string('telephone', 150)->nullable()->default(null);
            $table->string('fullName')->nullable();

            $table->string('userId')->nullable();
            $table->string('userCode')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('role')->nullable();
            $table->string('status', 150)->nullable();
            $table->text('notes')->nullable();

            $table->datetime('privacy')->nullable()->default(null);

            $table->boolean('is_system')->default(false);
            $table->boolean('is_admin')->default(false);
            $table->boolean('is_expert')->default(false);
            $table->boolean('is_trainer')->default(false);
            $table->boolean('is_site')->default(false);
            $table->boolean('is_other')->default(false);
            $table->boolean('is_monitor')->default(false);
            $table->boolean('is_project')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
