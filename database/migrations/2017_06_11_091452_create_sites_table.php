<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            //Relationships
            $table->integer('project_id')->unsigned()->nullable();

            //Other Fields
            $table->string('identifier', 50);

            $table->string('name', 150);
            $table->string('street1', 150)->nullable()->default('NULL');
            $table->string('street2', 150)->nullable()->default('NULL');
            $table->string('city', 150)->nullable()->default('NULL');
            $table->string('state', 150)->nullable()->default('NULL');
            $table->string('zip', 150)->nullable()->default('NULL');
            $table->string('country', 150)->nullable()->default('NULL');
            $table->string('shipAttention', 150)->nullable()->default('NULL');

            $table->string('siteTelephone', 150)->nullable()->default('NULL');

            $table->string('status', 150)->nullable()->default('NULL');
            $table->text('notes')->nullable();

            $table->string('equipmentStatus', 150)->nullable()->default('NULL');

            $table->text('shippingNotes')->nullable();
            $table->text('importExportNotes')->nullable();
            $table->text('returnShippingNotes')->nullable();
            $table->text('returnInspectionNotes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sites');
    }
}
