<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->integer('project_id')->unsigned()->nullable();
            $table->foreign('project_id')->references('id')->on('projects');

            $table->string('fileIdentifier', 50)->nullable();
            $table->string('filePath', 191)->nullable()->default(null);

            $table->timestamps();
            $table->softDeletes();

            $table->string('name', 150);

            $table->timestamp('downloaded_at')->nullable();
            $table->integer('size')->nullable();

            //Relationships
            $table->integer('site_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            //More Fields
            $table->integer('rating')->default(0);
            $table->integer('reviewer_id')->nullable();
            $table->datetime('rated_at')->nullable();
            $table->text('reviewer_notes')->nullable();
            $table->string('emailAttachment', 191)->nullable();

            $table->string('file_type')->nullable()->default('Study');

            $table->string('is_draft')->default('no');

            $table->string('status')->nullable();
            $table->string('comment')->nullable();
            $table->string('subject_status')->nullable();
            $table->string('subject_id')->nullable();
            $table->string('subject_visit')->nullable();
            $table->date('eeg_date')->nullable();
            $table->string('qualityCategory')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
